import './App.css';
import { useEffect, useState } from "react";
import { NotLoginRoute, AlreadyLoginRoute } from './navigation/RouteApp';
import ReactSelectExample from './ui/sample/ReactSelectExample';
import ExampleReactSelect from './ui/sample/ExampleReactSelect';
import SidebarBarNavigationAdmin from './ui/admin/SideBarNavigationAdmin';
import { Context, Provider } from './provider/provider';

function App() {
  // const [isLogin, setIsLogin] = useState(window.localStorage.getItem("isLogin"));
  // const [token, setToken] = useState(window.localStorage.getItem("token"));
  // useEffect(() => {
  //   console.log(isLogin);
  // }, []);

  let isLogin = localStorage.getItem("isLogin");


  // if (!isLogin || isLogin == null) {
  //   return (
  //     <NotLoginRoute/>
  //   );
  // }
  return (
    <Provider>
      <AlreadyLoginRoute/>
    </Provider>
    // <SidebarBarNavigationAdmin/>
  );
}

export default App;
