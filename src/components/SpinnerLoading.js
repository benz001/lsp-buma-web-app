import React from 'react';
import { Spinner } from "reactstrap";

const SpinnerLoading = () => {
    return (
        <div style={{ width: '100%', height: '95%', background: 'white', display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
            <Spinner color="primary" style={{ width: '3rem', height: '3rem' }} />
            <div>Loading...</div>
        </div>
    );
}

export default SpinnerLoading;