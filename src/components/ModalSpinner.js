import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Spinner } from 'reactstrap';
import React from 'react';

const ModalSpinner = (props) => {
    return (
        <Modal isOpen={props.statusOpen} toggle={props.toggle} size="lg" style={{maxWidth: '300px', width: '60%', justifyContent: 'center', marginTop: '20%'}} backdrop="static">
            <ModalBody style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                <Spinner size="lg" color="dark" />
                <div>Loading...</div>
            </ModalBody>
        </Modal>
    );
}

export default ModalSpinner;