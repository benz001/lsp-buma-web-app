import { Modal, ModalHeader, ModalBody, ModalFooter, Button} from 'reactstrap';
import React from 'react';

 const ModalAlert = (props) => {
    return (
        <Modal isOpen={props.statusOpen} toggle={props.toggle} size="lg" style={{ maxWidth: '500px', width: '60%', marginTop: '20%' }} backdrop="static">
            <ModalHeader toggle={props.toggle}>{props.title}</ModalHeader>
            <ModalBody >
                {props.content}
            </ModalBody>
            <ModalFooter>
                <Button color="danger" onClick={props.close}>Ok</Button>{' '}
            </ModalFooter>
        </Modal>
    );
}

export default ModalAlert;