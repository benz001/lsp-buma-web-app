import React, { useEffect, useState } from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useHistory,
    useRouteMatch
} from "react-router-dom";
import logo from '../../assets/image/logo.PNG';
import ListAsessor from '../../assets/image/ic_list_assesor.png';
import ListAsesi from '../../assets/image/ic_list_asesi.png';
import ProposeNewAsesment from '../../assets/image/ic_propose_new_asesment.png';
import CreateSchedule from '../../assets/image/ic_create_schedule.png';
import AsesmentDocument from '../../assets/image/ic_asesment_document.png';
import { AiOutlineImport, AiFillAccountBook, AiFillFile } from "react-icons/ai";
import ModalAlert from "../../components/ModalAlert";
import AdminProfilePage from "./AdminProfilePage";
import ListAssesorPage from "./ListAsessorPage";
import ListAccessionPage from "./ListAccessionPage";
import { FaUserLock } from "react-icons/fa";
import ListComptencyTestPlace from "./ListCompetencyTestPlace";
import ListScheme from "./ListScheme";
import ProposeNewAsesmentPage from "./ProposeNewAsesmentPage";
import CreateSchedulePage from "./CreateSchedulePage";
import AssesmentDocumentPage from "./AssesmentDocumentPage";




export default function SidebarBarNavigationAdmin() {
    useEffect(() => {
        checkRolesAuth();
    }, []);

    let { path, url } = useRouteMatch();
    const routes = [
        {
            path: `${url}`,
            exact: true,
            sidebar: () => <div style={{ width: '100%', height: '100%', display: 'flex', justifyContent: 'center' }}> <h3 style={{marginTop: '25%'}}>Selamat Datang, Silahkan Pilih Menu yang Tersedia</h3></div>,
            main: () => <div style={{ width: '100%', height: '100%', display: 'flex', justifyContent: 'center'}}> <h3 style={{marginTop: '25%'}}>Selamat Datang, Silahkan Pilih Menu yang Tersedia</h3></div>
        },
        {
            path: `${url}/profil-admin`,
            sidebar: () => <AdminProfilePage />,
            main: () => <AdminProfilePage />
        },
        {
            path: `${url}/daftar-asesor`,
            sidebar: () => <ListAssesorPage />,
            main: () => <ListAssesorPage />
        },
        {
            path: `${url}/daftar-asesi`,
            sidebar: () => <ListAccessionPage />,
            main: () => <ListAccessionPage />
        },
        {
            path: `${url}/daftar-tuk`,
            sidebar: () => <ListComptencyTestPlace />,
            main: () => <ListComptencyTestPlace />
        },
        {
            path: `${url}/daftar-skema`,
            sidebar: () => <ListScheme />,
            main: () => <ListScheme />
        },
        {
            path: `${url}/pengajuan-asesmen-baru`,
            sidebar: () => <ProposeNewAsesmentPage />,
            main: () => <ProposeNewAsesmentPage />
        },
        {
            path: `${url}/create-schedule`,
            sidebar: () => <CreateSchedulePage />,
            main: () => <CreateSchedulePage />
        },
        {
            path: `${url}/dokumen-asesmen`,
            sidebar: () => <AssesmentDocumentPage />,
            main: () => <AssesmentDocumentPage />
        }
    ];
    let history = useHistory();
    const [activeIndicator, setActiveIndicator] = useState(0);
    const [showModal, setShowModal] = useState(false);
    const [hideWelcome, setHideWelcome] = useState(false);
    const changeShowModal = () => {
        history.push("/");
    }

    const checkRolesAuth = () => {
        let roles = localStorage.getItem("roles");
        if (roles != null) {
            if (roles === "admin") {
                setShowModal(false);
            } else {
                setShowModal(true);
            }
        } else {
            setShowModal(true);
        }
    }





    const chooseMenu = (indicator, status) => {
        if (indicator != 9) {
            setActiveIndicator(indicator);
            setHideWelcome(status);
        } else {
            if (window.confirm('Apa Anda yakin ingin keluar ?')) {
                // Save it!
                localStorage.clear();
                history.push("/");
                console.log('Thing was saved to the database.');
            } else {
                // Do nothing!
                console.log('Thing was not saved to the database.');
            }
        }
    }

    return (
        <Router>
            <div style={{ width: '100%'}}>
                <div style={{ width: '100%', height: '15%', background: '#2F5597', display: 'flex', flexDirection: 'row' }}>
                    <img src={logo} style={{ paddingLeft: '1%', paddingTop: '1%', paddingBottom: '2%' }} height='85px' />
                    <div style={{ borderLeft: '3px solid white', height: '50px', marginTop: '12px', marginLeft: '5px' }} />
                    <div style={{ display: 'flex', flexDirection: 'column', paddingLeft: '1%', paddingTop: '1%', color: 'white' }}>
                        <h6>LSP BUMA</h6>
                        <h6>Lembaga Sertifikasi Profesi PT Bukit Makmur Mandiri Utama</h6>
                    </div>
                </div>
                <div style={{ width: '100%', height: activeIndicator == 7?'1650px':'1350px', display: 'flex', flexDirection: 'row' }}>
                    <div style={{ width: '20%', background: '#E2EBCF' }}>
                        <div style={{ height: '5px' }} />
                        <ul style={{ listStyleType: "none", padding: 0 }}>
                            <li style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                                <FaUserLock color="#B0131E" size={30} style={{ marginTop: '5px', marginLeft: '5px' }} />
                                <Link onClick={() => chooseMenu(1, true)} style={{ color: activeIndicator === 1 ? 'blue' : 'black', textDecoration: 'none' }} to={`${url}/profil-admin`}>Profile</Link>
                                {/* <Link onClick={() => alert("Under Development")} style={{ color: activeIndicator === 1 ? 'blue' : 'black', textDecoration: 'none' }}>Profile<label style={{color: 'blue'}}>(Coming Soon)</label></Link> */}
                            </li>
                            <hr style={{ borderTop: activeIndicator === 1 ? '1px solid red' : '1px solid white' }} />

                            <li style={{ display: 'flex', flexDirection: 'row', paddingTop: 'px' }}>
                                <img src={ListAsessor} height="30px" />
                                {/* <Link onClick={() => alert("Under Development")} style={{ color: activeIndicator === 1 ? 'blue' : 'black', textDecoration: 'none' }}>Daftar Asesor<label style={{color: 'blue'}}>(Coming Soon)</label></Link> */}
                                <Link onClick={() => chooseMenu(2, true)} style={{ color: activeIndicator === 2 ? 'blue' : 'black', textDecoration: 'none' }} to={`${url}/daftar-asesor`}>Daftar Asesor</Link>
                            </li>
                            <hr style={{ borderTop: activeIndicator === 2 ? '1px solid red' : '1px solid white' }} />

                            <li style={{ display: 'flex', flexDirection: 'row' }}>
                                <img src={ListAsesi} height="30px" />
                                {/* <Link onClick={() => alert("Under Development")} style={{ color: activeIndicator === 1 ? 'blue' : 'black', textDecoration: 'none' }}>Daftar Asesi<label style={{color: 'blue'}}>(Coming Soon)</label></Link> */}
                                <Link onClick={() => chooseMenu(3, true)} style={{ color: activeIndicator === 3 ? 'blue' : 'black', textDecoration: 'none' }} to={`${url}/daftar-asesi`}>Daftar Asesi</Link>
                            </li>
                            <hr style={{ borderTop: activeIndicator === 3 ? '1px solid red' : '1px solid white' }} />

                            <li style={{ display: 'flex', flexDirection: 'row' }}>
                                <AiFillAccountBook color="#B0131E" size={30} style={{ marginLeft: '5px' }} />
                                {/* <Link onClick={() => alert("Under Development")} style={{ color: activeIndicator === 1 ? 'blue' : 'black', textDecoration: 'none' }}>Daftar TUK<label style={{color: 'blue'}}>(Coming Soon)</label></Link> */}
                                <Link onClick={() => chooseMenu(4, true)} style={{ color: activeIndicator === 4 ? 'blue' : 'black', textDecoration: 'none' }} to={`${url}/daftar-tuk`}>Daftar TUK</Link>
                            </li>
                            <hr style={{ borderTop: activeIndicator === 4 ? '1px solid red' : '1px solid white' }} />

                            <li style={{ display: 'flex', flexDirection: 'row' }}>
                                <AiFillFile color="#B0131E" size={30} style={{ marginLeft: '5px' }} />
                                {/* <Link onClick={() => alert("Under Development")} style={{ color: activeIndicator === 1 ? 'blue' : 'black', textDecoration: 'none' }}>Daftar Skema<label style={{color: 'blue'}}>(Coming Soon)</label></Link> */}
                                <Link onClick={() => chooseMenu(5, true)} style={{ color: activeIndicator === 5 ? 'blue' : 'black', textDecoration: 'none' }} to={`${url}/daftar-skema`}>Daftar Skema</Link>
                            </li>
                            <hr style={{ borderTop: activeIndicator === 5 ? '1px solid red' : '1px solid white' }} />

                            <li style={{ display: 'flex', flexDirection: 'row' }}>
                                <img src={ProposeNewAsesment} height="30px" />
                                <Link onClick={() => chooseMenu(6, true)} style={{ color: activeIndicator === 6 ? 'blue' : 'black', textDecoration: 'none' }} to={`${url}/pengajuan-asesmen-baru`}>Pengajuan Asesmen Baru</Link>
                            </li>
                            <hr style={{ borderTop: activeIndicator === 6 ? '1px solid red' : '1px solid white' }} />

                            <li style={{ display: 'flex', flexDirection: 'row' }}>
                                <img src={CreateSchedule} height="30px" />
                                <Link onClick={() => chooseMenu(7, true)} style={{ color: activeIndicator === 7 ? 'blue' : 'black', textDecoration: 'none' }} to={`${url}/create-schedule`}>Create Schedule Pra-Asesmen</Link>
                            </li>
                            <hr style={{ borderTop: activeIndicator === 7 ? '1px solid red' : '1px solid white' }} />

                            <li style={{ display: 'flex', flexDirection: 'row' }}>
                                <img src={AsesmentDocument} height="30px" />
                                <Link onClick={() => chooseMenu(8, true)} style={{ color: activeIndicator === 8 ? 'blue' : 'black', textDecoration: 'none' }} to={`${url}/dokumen-asesmen`}>Dokumen Asesmen</Link>
                            </li>
                            <hr style={{ borderTop: activeIndicator === 8 ? '1px solid red' : '1px solid white' }} />

                            <li style={{ display: 'flex', flexDirection: 'row' }}>
                                <AiOutlineImport color="#B0131E" size={25} />
                                <Link onClick={() => chooseMenu(9, true)} style={{ color: activeIndicator === 9 ? 'blue' : 'black', textDecoration: 'none' }}>Logout</Link>
                            </li>
                        </ul>
                    </div>
                    <div style={{ width: '80%', height: '100%' }}>
                        {/* {hideWelcome ? null : <div><h3>Selamat Datang, Silahkan Pilih Menu yang Tersedia</h3></div>} */}
                        <Switch>
                            {routes.map((route, index) => (
                                <Route
                                    key={index}
                                    path={route.path}
                                    exact={route.exact}
                                    children={<route.main />}
                                />
                            ))}
                        </Switch>
                    </div>
                </div>
                <ModalAlert statusOpen={showModal} toggle={changeShowModal} title={"Pesan"} content={"Maaf Anda tidak memiliki akses kehalaman ini"} close={changeShowModal} />
            </div>
        </Router>
    );
}

