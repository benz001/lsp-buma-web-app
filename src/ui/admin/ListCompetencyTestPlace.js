import React, { useEffect, useState } from 'react';
import SpinnerLoading from '../../components/SpinnerLoading';
import axios from 'axios';
import { Button, Col, FormGroup, Input, InputGroup, InputGroupText, Label, Modal, ModalBody, ModalFooter, ModalHeader, Pagination, PaginationItem, PaginationLink, Row, Spinner, Table } from 'reactstrap';
import { AiFillEdit, AiFillDelete, AiFillFolderOpen, AiOutlineSearch, AiOutlineUserAdd } from "react-icons/ai";
import { APIURL } from '../../helper/LinkAPI';
import { checkThisEmpty } from '../../helper/CheckEmpty';
import AsyncSelect from 'react-select/async';
import Select from 'react-select';
import { useHistory } from 'react-router';

export default function ListCompetencyTestPlace() {
    let history = useHistory();
    const [next, setNext] = useState(1);
    const [isLoading, setIsLoading] = useState(true);
    const [listPlaceCompetence, setListPlaceCompetence] = useState([]);
    const [modalCreate, setModalCreate] = useState(false);
    const [loadingModalCreate, setLoadingModalCreating] = useState(true);
    const [savingModalCreate, setSavingModalCreate] = useState(false);

    const [modalUpdate, setModalUpdate] = useState(false);
    const [loadingModalUpdate, setLoadingModalUpdate] = useState(true);
    const [savingModalUpdate, setSavingModalUpdate] = useState(false);

    const [modalDelete, setModalDelete] = useState(false);
    const [loadingModalDelete, setLoadingModalDelete] = useState(true);
    const [savingModalDelete, setSavingModalDelete] = useState(false);

    const [modalView, setModalView] = useState(false);
    const [loadingModalView, setLoadingModalView] = useState(true);
    const [savingModalView, setSavingModalView] = useState(false);
    const [name, setName] = useState("");
    const [address, setAddress] = useState("");
    const [code, setCode] = useState("");

    var pages = 1;
    const [page, setPage] = useState(pages);
    const [id, setId] = useState(null);
    const [totalPage, setTotalPage] = useState(null);

    useEffect(() => {
        getListPlaceCompetence();
    }, []);


    const getListPlaceCompetence = async () => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + `/place-competence/get/all`, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                const total = result.data.data;
                if (status === "success") {
                    console.log("data success: " + JSON.stringify(data));
                    setListPlaceCompetence(data.data);
                    setTotalPage(total.last_page);
                    setTimeout(() => {
                        setIsLoading(false)
                    }, 1000);
                }
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                history.push('/admin');
            } else {
                alert("Error: " + JSON.stringify(error.response.data?.data?.error));
                history.push('/admin');
            }
        }
    }

    const createPlaceCompetence = async () => {

        if (checkThisEmpty(name)) {
            alert("Field input tidak boleh kosong");
        } else {
            const token = localStorage.getItem("token");
            console.log("token: " + token);
            setSavingModalCreate(true);
            try {
                const result = await axios.post(APIURL + "/place-competence", {
                    "name": name,
                    "address":address,
                    "code": code
                }, {
                    headers: {
                        "Authorization": `Bearer ${token}`
                    }
                });

                if (result.status === 200) {
                    const meta = result.data.meta;
                    const status = meta.status;
                    const data = result.data.data;
                    if (status === "success") {
                        alert("Create Success");
                        setSavingModalCreate(false);
                        refreshListPlaceCompetence().then((value) => {
                            if (value) {
                                setModalCreate(false);
                            }
                        });
                        // console.log("role " + JSON.stringify(role));
                    } else {
                        alert("Create Failed");
                        setSavingModalCreate(false);
                    }
                } else {
                    // setIsLoading(false);
                    alert("Create Failed");
                    console.log("Create Error: " + JSON.stringify(result.data));
                    setSavingModalCreate(false);
                }
            } catch (error) {
                console.log("error " + error);
                alert("Create Error");
                setSavingModalCreate(false);
            }
        }
    }

    const refreshListPlaceCompetence = async () => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + `/place-competence/get/all`, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                if (status === "success") {
                    console.log("data success: " + JSON.stringify(data));
                    setListPlaceCompetence(data.data);
                } else {
                    console.log("data unsuccess: " + JSON.stringify(data));
                }
                return true;
            } else {
                console.log("bad request");
                return false;
            }
        } catch (error) {
            console.log("error request");
            return false;
        }
    }

    const clearField = () => {
        setName("");
        setAddress("");
        setCode("");
    }

    const getDetailUpdate = async (id) => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + `/place-competence/${id}`, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                if (status === "success") {
                    console.log("data success: " + JSON.stringify(data.name));
                    setName(data.name);
                    setAddress(data.address);
                    setCode(data.code);
                } else {
                    console.log("data unsuccess: " + JSON.stringify(data));
                }
                return true;
            } else {
                console.log("bad request");
                return false;
            }
        } catch (error) {
            console.log("error request: " + error);
            return false;
        }
    }

    const updatePlaceCompetence = async () => {
        if (checkThisEmpty(name)) {
            alert("Field input tidak boleh kosong");
        } else {
            const token = localStorage.getItem("token");
            console.log("token: " + token);
            setSavingModalUpdate(true);
            try {
                const result = await axios.put(APIURL + `/place-competence/${id}/update`, {
                    "name": name,
                    "address": address,
                    "code": code

                }, {
                    headers: {
                        "Authorization": `Bearer ${token}`,
                        "Content-Type": "application/json"
                    }
                });

                if (result.status === 200) {
                    const meta = result.data.meta;
                    const status = meta.status;
                    const data = result.data.data;
                    if (status === "success") {
                        alert("Update Success");
                        setSavingModalUpdate(false);
                        refreshListPlaceCompetence().then((value) => {
                            if (value) {
                                setModalCreate(false);
                            }
                        });
                    } else {
                        alert("Update Failed");
                        setSavingModalCreate(false);
                    }
                } else {
                    alert("Update Failed");
                    console.log("Create Error: " + JSON.stringify(result.data));
                    setSavingModalUpdate(false);
                }
            } catch (error) {
                console.log("error " + error);
                alert("Update Error");
                setSavingModalUpdate(false);
            }
        }
    }


    const deletePlaceCompetence = async () => {
        console.log("id: " + id);
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        setSavingModalDelete(true);
        try {
            const result = await axios.delete(APIURL + `/place-competence/${id}/delete`, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                if (status === "success") {
                    alert("Delete Success");
                    setSavingModalDelete(false);
                    refreshListPlaceCompetence().then((value) => {
                        if (value) {
                            setModalDelete(false);
                        }
                    });
                } else {
                    alert("Delete Failed");
                    setSavingModalDelete(false);
                }
            } else {
                alert("Delete Failed");
                console.log("Delete Error: " + JSON.stringify(result.data));
                setSavingModalDelete(false);
            }
        } catch (error) {
            console.log("error " + error);
            alert("Delete Error");
            setSavingModalDelete(false);
        }
    }

    const getViewPlaceCompetence = async (id) => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + `/place-competence/${id}`, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                if (status === "success") {
                    console.log("data success: " + JSON.stringify(data));
                    setName(data.name);
                    setAddress(data.address);
                    setCode(data.code);
                } else {
                    console.log("data unsuccess: " + JSON.stringify(data));
                }
                return true;
            } else {
                console.log("bad request");
                return false;
            }
        } catch (error) {
            console.log("error request: " + error);
            return false;
        }
    }

    const toggleCreate = () => {
        clearField();
        setModalCreate(!modalCreate);
        if (loadingModalCreate) {
            setTimeout(() => {
                setLoadingModalCreating(false);
            }, 1000);
        }
    }

    const toggleUpdateOpen = (id) => {
        clearField();
        getDetailUpdate(id).then((value) => {
            if (value) {
                console.log("value: " + value);
                setId(id);
                setModalUpdate(!modalUpdate);
                if (loadingModalUpdate) {
                    setTimeout(() => {
                        setLoadingModalUpdate(false);
                    }, 1000);
                }
            } else {
                alert("Get data failed");
            }
        }
        );
    }

    const toggleDeleteOpen = (id) => {
        if (id != "") {
            setId(id);
            setModalDelete(!modalDelete);
            if (loadingModalDelete) {
                setTimeout(() => {
                    setLoadingModalDelete(false);
                }, 1000);
            }
        } else {
            alert("Get data failed");
        }
    }

    const toggleUpdateClose = () => {
        clearField();
        setId(null);
        setModalUpdate(!modalUpdate);
        if (loadingModalUpdate) {
            setTimeout(() => {
                setLoadingModalUpdate(false);
            }, 1000);
        }
    }

    const toggleDeleteClose = () => {
        setId(null);
        setModalDelete(!modalDelete);
        if (loadingModalDelete) {
            setTimeout(() => {
                setLoadingModalDelete(false);
            }, 1000);
        }
    }

    const toggleViewOpen = (id) => {
        clearField();
        getViewPlaceCompetence(id).then((value) => {
            if (value) {
                setId(id);
                setModalView(!modalView);
                if (loadingModalView) {
                    setTimeout(() => {
                        setLoadingModalView(false);
                    }, 1000);
                }
            } else {
                alert("Get data failed");
            }
        });
    }

    const toggleViewClose = () => {
        setId(null);
        setModalView(!modalView);
        if (loadingModalView) {
            setTimeout(() => {
                setLoadingModalView(false);
            }, 1000);
        }
    }



    const showModalCreate = () => {
        return (
            <Modal isOpen={modalCreate} toggle={toggleCreate} size="lg" style={{ maxWidth: '700px', width: '100%' }} backdrop="static">
                <ModalHeader toggle={toggleCreate}>Tambah TUK</ModalHeader>
                {
                    loadingModalCreate ?
                        <ModalBody style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                            <Spinner size="lg" color="blue" />
                            <div>Loading...</div>
                        </ModalBody>
                        :
                        savingModalCreate ?
                            <ModalBody style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <Spinner size="lg" color="blue" />
                                <div>Saving...</div>
                            </ModalBody>
                            :
                            <ModalBody>
                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="name"
                                    >Nama TUK</Label>
                                    <Input type="text" name="name" id="name" onChange={(e) => setName(e.target.value)} style={{ width: '85%' }} />
                                </FormGroup>
                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="address"
                                    >Alamat</Label>
                                    <Input type="text" name="address" id="address" onChange={(e) => setAddress(e.target.value)} style={{ width: '85%' }} />
                                </FormGroup>
                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="code"
                                    >Kode</Label>
                                    <Input type="text" name="code" id="code" onChange={(e) => setCode(e.target.value)} style={{ width: '85%' }} />
                                </FormGroup>
                            </ModalBody>

                }
                <ModalFooter>
                    <Button color="danger" onClick={createPlaceCompetence}>Simpan</Button>{' '}
                    <Button color="warning" onClick={toggleCreate}>Batal</Button>
                </ModalFooter>
            </Modal>

        );
    }

    const showModalUpdate = () => {
        return (
            <Modal isOpen={modalUpdate} toggle={toggleUpdateClose} size="lg" style={{ maxWidth: '700px', width: '100%' }} backdrop="static">
                <ModalHeader toggle={toggleUpdateClose}>Update TUK</ModalHeader>
                {
                    loadingModalUpdate ?
                        <ModalBody style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                            <Spinner size="lg" color="blue" />
                            <div>Loading...</div>
                        </ModalBody>
                        :
                        savingModalUpdate ?
                            <ModalBody style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <Spinner size="lg" color="blue" />
                                <div>Saving...</div>
                            </ModalBody>
                            :
                            <ModalBody>
                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="name"
                                    >Nama TUK</Label>
                                    <Input type="text" name="name" id="name" value={name} onChange={(e) => setName(e.target.value)} style={{ width: '85%' }} />
                                </FormGroup>
                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="address"
                                    >Alamat</Label>
                                    <Input type="text" name="address" id="address" value={address} onChange={(e) => setAddress(e.target.value)} style={{ width: '85%' }} />
                                </FormGroup>
                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="code"
                                    >Kode</Label>
                                    <Input type="text" name="code" id="code" value={code} onChange={(e) => setCode(e.target.value)} style={{ width: '85%' }} />
                                </FormGroup>
                            </ModalBody>

                }
                <ModalFooter>
                    <Button color="danger" onClick={updatePlaceCompetence}>Simpan</Button>{' '}
                    <Button color="warning" onClick={toggleUpdateClose}>Batal</Button>
                </ModalFooter>
            </Modal>

        );
    }

    const showModalDelete = () => {
        return (
            <Modal isOpen={modalDelete} toggle={toggleDeleteClose} size="lg" style={{ maxWidth: '700px', width: '100%' }} backdrop="static">
                <ModalHeader toggle={toggleDeleteClose}>Hapus TUK</ModalHeader>
                {
                    loadingModalDelete ?
                        <ModalBody style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                            <Spinner size="lg" color="blue" />
                            <div>Loading...</div>
                        </ModalBody>
                        :
                        savingModalDelete ?
                            <ModalBody style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <Spinner size="lg" color="blue" />
                                <div>Saving...</div>
                            </ModalBody>
                            :
                            <ModalBody>
                                Apakah Anda yakin ingin menghapus data ini ?
                            </ModalBody>

                }
                <ModalFooter>
                    <Button color="danger" onClick={deletePlaceCompetence}>Ya</Button>{' '}
                    <Button color="warning" onClick={toggleDeleteClose}>Tidak</Button>
                </ModalFooter>
            </Modal>

        );
    }

    const showModalView = () => {
        return (
            <Modal isOpen={modalView} toggle={toggleViewClose} size="lg" style={{ maxWidth: '700px', width: '100%' }} backdrop="static">
                <ModalHeader toggle={toggleViewClose}>Detail TUK</ModalHeader>
                {
                    loadingModalView ?
                        <ModalBody style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                            <Spinner size="lg" color="blue" />
                            <div>Loading...</div>
                        </ModalBody>
                        :
                        savingModalView ?
                            <ModalBody style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <Spinner size="lg" color="blue" />
                                <div>Saving...</div>
                            </ModalBody>
                            :
                            <ModalBody>
                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="name"
                                    >Nama TUK</Label>
                                    <Input type="text" name="name" id="name" value={name} onChange={(e) => setName(e.target.value)} style={{ width: '85%' }} disabled />
                                </FormGroup>
                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="address"
                                    >Alamat</Label>
                                    <Input type="text" name="address" id="address" value={address} style={{ width: '85%' }} disabled/>
                                </FormGroup>
                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="code"
                                    >Kode</Label>
                                    <Input type="text" name="code" id="code" value={code} style={{ width: '85%' }} disabled/>
                                </FormGroup>
                            </ModalBody>

                }
                <ModalFooter>
                    {/* <Button color="danger" onClick={toggleViewOpen}>Simpan</Button>{' '} */}
                    <Button color="warning" onClick={toggleViewClose}>Tutup</Button>
                </ModalFooter>
            </Modal>

        );
    }



    if (isLoading) {
        return (
            <SpinnerLoading />
        );
    }

    return (
        <div style={styles.body}>
            <div style={styles.searchAndSort}>
                <div style={{ width: '80%' }}>
                    <InputGroup>
                        <Input placeholder=" Masukkan Keyword" />
                        <InputGroupText>
                            <AiOutlineSearch />
                        </InputGroupText>
                    </InputGroup>
                </div>
                <div style={{ marginLeft: '5px', marginRight: '5px' }} />
                <div style={{ width: '20%' }}>
                    <Input
                        id="exampleSelect"
                        name="select"
                        type="select"
                    >
                        <option>Sortir Berdasarkan...</option>
                    </Input>
                </div>
            </div>
            <div>
            </div>
            <div style={styles.table}>
                <Table
                    bordered
                    // responsive
                    striped
                >
                    <thead>
                        <tr style={{ background: 'black', color: 'white' }}>
                            <th style={{ width: '5%' }}>No</th>
                            <th>Nama TUK</th>
                            <th style={{ width: '20%', textAlign: 'center' }}>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            listPlaceCompetence.map((item, index) => (
                                <tr>
                                    <td>{index + 1}</td>
                                    <td>
                                        {item.name}
                                    </td>
                                    <td>
                                        <Row>
                                            <Col xs="3">
                                                <Button color="primary"
                                                    onClick={() => toggleUpdateOpen(item.id)}>
                                                    <AiFillEdit />
                                                </Button></Col>
                                            <Col xs="3"><Button color="danger" onClick={() => toggleDeleteOpen(item.id)}><AiFillDelete /></Button></Col>
                                            <Col xs="3"><Button color="success" onClick={() => toggleViewOpen(item.id)}><AiFillFolderOpen /></Button></Col>
                                        </Row>
                                    </td>
                                </tr>
                            ))
                        }
                    </tbody>
                </Table>
            </div>
            <div style={styles.addAndPagination}>
                <div style={{ width: '50%', display: 'flex', justifyContent: 'flex-start' }}>
                    <Button onClick={toggleCreate}>
                        <AiOutlineUserAdd />   Tambah TUK
                    </Button>
                </div>
                <div style={{ width: '50%', display: 'flex', justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                    <Pagination aria-label="Page navigation example">
                        <PaginationItem disabled>
                            <PaginationLink
                                href="#"
                                previous
                            />
                        </PaginationItem>
                        <div style={{ width: '100px', display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <div style={{ width: '60px' }}>
                                <Input type="number" value={page} style={{ textAlign: 'center' }} />
                            </div>
                            <div style={{ width: '40px' }}>
                                /{totalPage}
                            </div>
                        </div>
                        <PaginationItem>
                            <PaginationLink
                                onClick={() => setPage(pages++)}
                                next
                            />
                        </PaginationItem>
                    </Pagination>
                </div>
            </div>
            {showModalCreate()}
            {showModalUpdate()}
            {showModalDelete()}
            {showModalView()}
        </div>
    );
}

const styles = {
    body: {
        width: '100%',
        height: '95%',
        paddingRight: '2%',
        paddingLeft: '2%'
    },
    searchAndSort: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        marginTop: '15px'
    },
    table: {
        width: '100%',
        marginTop: '15px'
    },
    addAndPagination: {
        width: '100%',
        marginTop: '15px',
        display: 'flex',
        flexDirection: 'row',
    },
}