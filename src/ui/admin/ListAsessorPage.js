import React, { useEffect, useState } from 'react';
import SpinnerLoading from '../../components/SpinnerLoading';
import axios from 'axios';
import { Button, Col, FormGroup, Input, InputGroup, InputGroupText, Label, Modal, ModalBody, ModalFooter, ModalHeader, Pagination, PaginationItem, PaginationLink, Row, Spinner, Table } from 'reactstrap';
import { AiFillEdit, AiFillDelete, AiFillFolderOpen, AiOutlineSearch, AiOutlineUserAdd } from "react-icons/ai";
import { APIURL } from '../../helper/LinkAPI';
import { checkThisEmpty } from '../../helper/CheckEmpty';
import AsyncSelect from 'react-select/async';
import Select from 'react-select';
import { useHistory } from 'react-router';

export default function ListAsessorPage() {
    let history = useHistory();
    const [next, setNext] = useState(1);
    const [isLoading, setIsLoading] = useState(true);
    const [listAsesor, setListAsesor] = useState([]);
    const [modalCreate, setModalCreate] = useState(false);
    const [loadingModalCreate, setLoadingModalCreating] = useState(true);
    const [savingModalCreate, setSavingModalCreate] = useState(false);

    const [modalUpdate, setModalUpdate] = useState(false);
    const [loadingModalUpdate, setLoadingModalUpdate] = useState(true);
    const [savingModalUpdate, setSavingModalUpdate] = useState(false);

    const [modalDelete, setModalDelete] = useState(false);
    const [loadingModalDelete, setLoadingModalDelete] = useState(true);
    const [savingModalDelete, setSavingModalDelete] = useState(false);

    const [modalView, setModalView] = useState(false);
    const [loadingModalView, setLoadingModalView] = useState(true);
    const [savingModalView, setSavingModalView] = useState(false);
    const [loadingSearch, setLoadingSearch] = useState(false);
    const [loadingSort, setLoadingSort] = useState(false);
    const [loadingPage, setLoadingPage] = useState(false);

    const [nik, setNIK] = useState("");
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");

    const [phone, setPhone] = useState("");
    const [gender, setGender] = useState("");
    const [pob, setPob] = useState("");
    const [dob, setDob] = useState("");
    const [address, setAddress] = useState("");
    const [province, setProvince] = useState("");
    const [city, setCity] = useState("");
    const [postalCode, setPostalCode] = useState("");
    const [education, setEducation] = useState("");
    const [profession, setProfession] = useState("");

    const [provinceFocus, setProvinceFocus] = useState(false);
    const [districtFocus, setDistrictFocus] = useState(false);
    const [listDistrict, setListDistrict] = useState([]);

    var pages = 1;
    const [page, setPage] = useState(1);
    const [keywordSearch, setKeywordSearch] = useState("");
    const [keywordSort, setKeywordSort] = useState("");

    const [id, setId] = useState(null);
    const [totalPage, setTotalPage] = useState(null);
    const [indexPage, setIndexPage] = useState(1);

    useEffect(() => {
        getListAsesor();
    }, []);

    useEffect(() => {
        console.log("result page: " + page);
        refreshListAsesor().then((value) => {
            if (value) {
                setLoadingPage(false);
            }
        });
    }, [loadingPage])



    const getListAsesor = async () => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + `/user/asesor?page=${page}&search=${keywordSearch}&sortby=${keywordSort}`, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                const total = result.data.data;
                if (status === "success") {
                    console.log("data success: " + JSON.stringify(data));
                    setListAsesor(data.data);
                    setTotalPage(total.last_page);
                    setTimeout(() => {
                        setIsLoading(false)
                    }, 1000);
                }
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                history.push('/admin');
            } else {
                alert("Error: " +JSON.stringify(error.response.data?.data?.error));
                history.push('/admin');
            }
        }
    }

    const getProvince = async () => {
        try {
            const result = await axios.get("https://ibnux.github.io/data-indonesia/provinsi.json");
            const data = result.data;
            const province = data;
            const tempArrayProvince = [];
            console.log("get Province");
            if (province.length) {
                province.forEach((element) => {
                    tempArrayProvince.push({
                        label: element.nama,
                        value: `${element.id}`
                    });
                });
            } else {
                tempArrayProvince.push({
                    label: province.nama,
                    value: `${province.id}`
                });
            }
            return tempArrayProvince;
        } catch (error) {
            // console.log("error: " + error);
            return [];
        }
    }

    const getDistrict = async (value) => {
        // console.log(selectDistrict !== "");
        try {
            const result = await axios.get(`https://ibnux.github.io/data-indonesia/kabupaten/${value}.json`);
            const data = result.data;
            const district = data;
            const tempArrayDistrict = [];

            console.log("getDistrict");

            if (district.length) {
                district.forEach((element) => {
                    tempArrayDistrict.push({
                        label: element.nama,
                        value: `${element.id}`
                    });
                });
            } else {
                tempArrayDistrict.push({
                    label: district.nama,
                    value: `${district.id}`
                });
            }

            setListDistrict(tempArrayDistrict);
        } catch (error) {
            // console.log("error: " + error);
            setListDistrict([]);
        }
    }

    const setterProvinceAndDistrict = (value, label) => {
        getDistrict(value);
        console.log(label);
        setProvince(label);

    }

    const setterDistrict = (value, label) => {
        console.log(label);
        setCity(label);

    }

    const createAsesor = async () => {
        if (checkThisEmpty(name) ||
            checkThisEmpty(email) ||
            checkThisEmpty(password) ||
            checkThisEmpty(confirmPassword)) {
            alert("Field input tidak boleh kosong");
        } else {
            setSavingModalCreate(true);
            try {
                const result = await axios.post(APIURL + "/user/register", {
                    "name": name,
                    "email": email,
                    "password": password,
                    "role": "asesor"
                });

                if (result.status === 200) {
                    const meta = result.data.meta;
                    const status = meta.status;
                    const data = result.data.data;
                    if (status === "success") {
                        alert("Create Success");
                        setSavingModalCreate(false);
                        refreshListAsesor().then((value) => {
                            if (value) {
                                setModalCreate(false);
                            }
                        });
                    }
                }
            } catch (error) {
                if (error.message === "Network Error") {
                    alert("Error: " + error.message);
                    setSavingModalCreate(false);
                } else {
                    alert("Error: " + error.response.data.data.error);
                    setSavingModalCreate(false);
                }
            }
        }
    }

    const refreshListAsesor = async () => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + `/user/asesor?page=${page}&search=${keywordSearch}&sortby=${keywordSort}`, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                if (status === "success") {
                    console.log("data success: ");
                    setListAsesor(data.data);
                }
                return true;
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                setSavingModalCreate(false);
            } else {
                alert("Error: " + error.response.data.data.error);
                setSavingModalCreate(false);
            }
        }
    }

    const clearField = () => {
        setName("");
        setEmail("");
        setPassword("");
        setConfirmPassword("");
        setNIK("");
        setPhone("");
        setGender("");
        setPob("");
        setDob("");
        setAddress("");
        setProvince("");
        setCity("");
        setPostalCode("");
        setEducation("");
        setProfession("");
    }

    const getDetailUpdate = async (id) => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + `/user/asesor/${id}`, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                if (status === "success") {
                    console.log("data success: " + JSON.stringify(data.profile));
                    // setListAsesi(data.data);
                    data.user.name != null ? setName(data.user.name) : setName("");
                    data.user.email != null ? setEmail(data.user.email) : setEmail("");
                    data.profile.nik != null ? setNIK(data.profile.nik) : setNIK("");
                    data.profile.phone_number != null ? setPhone(data.profile.phone_number) : setPhone("");
                    data.profile.gender != null ? setGender(data.profile.gender) : setGender("");
                    data.profile.place_of_birth != null ? setPob(data.profile.place_of_birth) : setPob("");
                    data.profile.date_of_birth != null ? setDob(data.profile.date_of_birth) : setDob("");
                    data.profile.address != null ? setAddress(data.profile.address) : setAddress("");
                    data.profile.province != null ? setProvince(data.profile.province) : setProvince("");
                    data.profile.city != null ? setCity(data.profile.city) : setCity("");
                    data.profile.postal_code != null ? setPostalCode(data.profile.postal_code) : setPostalCode("");
                    data.profile.education != null ? setEducation(data.profile.education) : setEducation("");
                    data.profile.profession != null ? setProfession(data.profile.profession) : setProfession("");
                }
                return true;
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                setSavingModalCreate(false);
            } else {
                alert("Error: " + error.response.data?.data?.error);
                setSavingModalCreate(false);
            }
            return false;
        }
    }

    const updateAsesor = async () => {
        if (checkThisEmpty(name) ||
            checkThisEmpty(email) ||
            checkThisEmpty(nik) ||
            checkThisEmpty(phone) ||
            checkThisEmpty(gender) ||
            checkThisEmpty(pob) ||
            checkThisEmpty(dob) ||
            checkThisEmpty(address) ||
            checkThisEmpty(province) ||
            checkThisEmpty(city) ||
            checkThisEmpty(postalCode) ||
            checkThisEmpty(education) ||
            checkThisEmpty(profession)) {
            alert("Field input tidak boleh kosong");
        } else {
            const token = localStorage.getItem("token");
            console.log("token: " + token);
            setSavingModalUpdate(true);
            try {
                const result = await axios.post(APIURL + `/user/asesor/${id}/update?_method=put`, {
                    "nik": nik,
                    "phone_number": phone,
                    "gender": gender,
                    "place_of_birth": pob,
                    "date_of_birth": dob,
                    "address": address,
                    "province": province,
                    "city": city,
                    "postal_code": postalCode,
                    "education": education,
                    "profession": profession

                }, {
                    headers: {
                        "Authorization": `Bearer ${token}`,
                        "Content-Type": "application/json"
                    }
                });

                if (result.status === 200) {
                    const meta = result.data.meta;
                    const status = meta.status;
                    const data = result.data.data;
                    if (status === "success") {
                        alert("Update Success");
                        setSavingModalUpdate(false);
                        refreshListAsesor().then((value) => {
                            if (value) {
                                setModalCreate(false);
                            }
                        });
                    }
                }
            } catch (error) {
                if (error.message === "Network Error") {
                    alert("Error: " + error.message);
                    setSavingModalUpdate(false);
                } else {
                    alert("Error: " + error.response.data.data.error);
                    setSavingModalUpdate(false);
                }
            }
        }
    }


    const deleteAsesor = async () => {
        console.log("id: " + id);
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        setSavingModalDelete(true);
        try {
            const result = await axios.delete(APIURL + `/user/asesor/${id}`, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                if (status === "success") {
                    alert("Delete Success");
                    setSavingModalDelete(false);
                    refreshListAsesor().then((value) => {
                        if (value) {
                            setModalDelete(false);
                        }
                    });
                }
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                setSavingModalDelete(false);
            } else {
                alert("Error: " + error.response.data.data.error);
                setSavingModalDelete(false);
            }

            return false;
        }
    }

    const getViewAsesor = async (id) => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + `/user/asesor/${id}`, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                if (status === "success") {
                    console.log("data success: " + JSON.stringify(data));
                    data.user.name != null ? setName(data.user.name) : setName("");
                    data.user.email != null ? setEmail(data.user.email) : setEmail("");
                    data.profile.nik != null ? setNIK(data.profile.nik) : setNIK("");
                    data.profile.phone_number != null ? setPhone(data.profile.phone_number) : setPhone("");
                    data.profile.gender != null ? setGender(data.profile.gender) : setGender("");
                    data.profile.place_of_birth != null ? setPob(data.profile.place_of_birth) : setPob("");
                    data.profile.date_of_birth != null ? setDob(data.profile.date_of_birth) : setDob("");
                    data.profile.address != null ? setAddress(data.profile.address) : setAddress("");
                    data.profile.province != null ? setProvince(data.profile.province) : setProvince("");
                    data.profile.city != null ? setCity(data.profile.city) : setCity("");
                    data.profile.postal_code != null ? setPostalCode(data.profile.postal_code) : setPostalCode("");
                    data.profile.education != null ? setEducation(data.profile.education) : setEducation("");
                    data.profile.profession != null ? setProfession(data.profile.profession) : setProfession("");
                }
                return true;
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                setSavingModalView(false);
            } else {
                alert("Error: " + error.response.data.data.error);
                setSavingModalView(false);
            }
            return false;
        }
    }

    const toggleCreate = () => {
        clearField();
        setModalCreate(!modalCreate);
        if (loadingModalCreate) {
            setTimeout(() => {
                setLoadingModalCreating(false);
            }, 1000);
        }
    }

    const toggleUpdateOpen = (id) => {
        clearField();
        getDetailUpdate(id).then((value) => {
            if (value) {
                console.log("value: " + value);
                setId(id);
                setModalUpdate(!modalUpdate);
                if (loadingModalUpdate) {
                    setTimeout(() => {
                        setLoadingModalUpdate(false);
                    }, 1000);
                }
            } else {
                alert("Get data failed");
            }
        });
    }

    const toggleDeleteOpen = (id) => {
        if (id != "") {
            setId(id);
            setModalDelete(!modalDelete);
            if (loadingModalDelete) {
                setTimeout(() => {
                    setLoadingModalDelete(false);
                }, 1000);
            }
        } else {
            alert("Get data failed");
        }
    }

    const toggleUpdateClose = () => {
        clearField();
        setId(null);
        setModalUpdate(!modalUpdate);
        if (loadingModalUpdate) {
            setTimeout(() => {
                setLoadingModalUpdate(false);
            }, 1000);
        }
    }

    const toggleDeleteClose = () => {
        setId(null);
        setModalDelete(!modalDelete);
        if (loadingModalDelete) {
            setTimeout(() => {
                setLoadingModalDelete(false);
            }, 1000);
        }
    }

    const toggleViewOpen = (id) => {
        clearField();
        getViewAsesor(id).then((value) => {
            if (value) {
                setId(id);
                setModalView(!modalView);
                if (loadingModalView) {
                    setTimeout(() => {
                        setLoadingModalView(false);
                    }, 1000);
                }
            } else {
                alert("Get data failed");
            }
        });
    }

    const toggleViewClose = () => {
        setId(null);
        setModalView(!modalView);
        if (loadingModalView) {
            setTimeout(() => {
                setLoadingModalView(false);
            }, 1000);
        }
    }

    const searchAsesor = () => {
        setKeywordSearch(keywordSearch);
        setLoadingSearch(true);
        refreshListAsesor().then((value) => {
            if (value) {
                setLoadingSearch(false);
            }
        });

    }

    const sortAsesor = (keywordSort) => {
        console.log("sort: " + keywordSort);
        setKeywordSort(keywordSort);
        setLoadingSort(true);
        refreshListAsesor().then((value) => {
            if (value) {
                setLoadingSort(false);
            }
        });
    }

    const nextPageAsesiEnter = () => {
        if (page > totalPage) {
            alert("Melebihi page terakhir")
        } else {
            console.log("page: " + page);
            setPage(page);
            // setIndexPage(11);
            setLoadingPage(true);

        }
    }

    const nextPageAsesorInc = () => {
        if (page >= totalPage) {
            alert("Sudah mencapai page terakhir")
        } else {
            console.log("page: " + page);
            setPage(page => page + 1);
            setIndexPage(indexPage => indexPage + 10);
            setLoadingPage(true);
        }

    }

    const nextPageAsesorDec = () => {
        console.log("page: " + typeof page);
        if (page < totalPage) {
            alert("Sudah mencapai page terendah")
        } else {
            setPage(page => page - 1);
            setIndexPage(indexPage => indexPage - 10);
            setLoadingPage(true);
        }
    }



    const showModalCreate = () => {
        return (
            <Modal isOpen={modalCreate} toggle={toggleCreate} size="lg" style={{ maxWidth: '700px', width: '100%' }} backdrop="static">
                <ModalHeader toggle={toggleCreate}>Tambah Asesor</ModalHeader>
                {
                    loadingModalCreate ?
                        <ModalBody style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                            <Spinner size="lg" color="blue" />
                            <div>Loading...</div>
                        </ModalBody>
                        :
                        savingModalCreate ?
                            <ModalBody style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <Spinner size="lg" color="blue" />
                                <div>Saving...</div>
                            </ModalBody>
                            :
                            <ModalBody>
                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="fullname"
                                    >Nama Lengkap</Label>
                                    <Input type="text" name="fullname" id="fullname" onChange={(e) => setName(e.target.value)} style={{ width: '85%' }} />
                                </FormGroup>


                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="email"
                                        // disabled
                                    >Email</Label>
                                    <Input type="email" name="email" id="email" onChange={(e) => setEmail(e.target.value)} style={{ width: '85%' }} />
                                </FormGroup>
                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="fullname"
                                    >Password</Label>
                                    <Input type="password" name="password" id="password" onChange={(e) => setPassword(e.target.value)} style={{ width: '85%' }} />
                                </FormGroup>


                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="confirm-password"
                                        disabled
                                    >Konfirmasi Password</Label>
                                    <Input type="password" name="confirm-password" id="confirm-password" onChange={(e) => setConfirmPassword(e.target.value)} style={{ width: '85%' }} />
                                </FormGroup>
                            </ModalBody>

                }
                <ModalFooter>
                    <Button color="danger" onClick={createAsesor}>Simpan</Button>{' '}
                    <Button color="warning" onClick={toggleCreate}>Batal</Button>
                </ModalFooter>
            </Modal>

        );
    }

    const showModalUpdate = () => {
        return (
            <Modal isOpen={modalUpdate} toggle={toggleUpdateClose} size="lg" style={{ maxWidth: '700px', width: '100%' }} backdrop="static">
                <ModalHeader toggle={toggleUpdateClose}>Update Asesor</ModalHeader>
                {
                    loadingModalUpdate ?
                        <ModalBody style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                            <Spinner size="lg" color="blue" />
                            <div>Loading...</div>
                        </ModalBody>
                        :
                        savingModalUpdate ?
                            <ModalBody style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <Spinner size="lg" color="blue" />
                                <div>Saving...</div>
                            </ModalBody>
                            :
                            <ModalBody>
                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="fullname"
                                    >Nama Lengkap</Label>
                                    <Input type="text" name="fullname" id="fullname" value={name} onChange={(e) => setName(e.target.value)} style={{ width: '85%' }} disabled />
                                </FormGroup>


                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="email"
                                        disabled
                                    >Email</Label>
                                    <Input type="email" name="email" id="email" value={email} onChange={(e) => setEmail(e.target.value)} style={{ width: '85%' }} disabled />
                                </FormGroup>

                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="nik"
                                    >NIK(KTP)</Label>
                                    <Input type="number" name="nik" id="nik" value={nik} onChange={(e) => setNIK(e.target.value)} style={{ width: '85%' }} />
                                </FormGroup>

                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="telephone"
                                    >No.Handphone</Label>
                                    <Input type="number" name="telephone" id="telephone" value={phone} onChange={(e) => setPhone(e.target.value)} style={{ width: '85%' }} />
                                </FormGroup>

                                <FormGroup for="gender" style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label style={{ width: '23%', textAlign: 'end', paddingRight: '10px' }}>Jenis Kelamin</Label>
                                    <div style={{ width: '77%' }}>
                                        <Input
                                            type="select"
                                            name="gender"
                                            id="gender"
                                            value={gender}
                                            onChange={(e) => setGender(e.target.value)}>
                                            <option value="">Pilih Jenis Kelamin</option>
                                            <option value="male">Pria</option>
                                            <option value="female">Wanita</option>
                                        </Input>
                                    </div>
                                </FormGroup>

                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label for="pob" style={{ width: '23%', textAlign: 'end', paddingRight: '10px' }}>Tempat Lahir</Label>
                                    <div style={{ width: '77%' }}>
                                        <Input type="text" id="pob" value={pob} onChange={(e) => setPob(e.target.value)} />
                                    </div>
                                </FormGroup>

                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label for="dob" style={{ width: '23%', textAlign: 'end', paddingRight: '10px' }}>Tanggal Lahir</Label>
                                    <div style={{ width: '77%' }}>
                                        <Input type="date" name="dob" id="dob" value={dob} onChange={(e) => setDob(e.target.value)} />
                                    </div>
                                </FormGroup>

                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="address"
                                    >Alamat</Label>
                                    <Input type="text" name="address" id="address" value={address} onChange={(e) => setAddress(e.target.value)} style={{ width: '85%' }} />
                                </FormGroup>

                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '22%', textAlign: 'end', paddingRight: '10px' }}
                                        for="provinsi"
                                    >Provinsi</Label>
                                    <div style={{ width: '78%' }}>
                                        <AsyncSelect
                                            styles={{ width: '50%' }}
                                            loadOptions={getProvince}
                                            placeholder={provinceFocus ? "Pilih Provinsi" : province != "" ? province : "Ketik Provinsi"}
                                            onFocus={() => setProvinceFocus(true)}
                                            onBlur={() => setProvinceFocus(false)}
                                            defaultOptions={true}
                                            onChange={(e) => setterProvinceAndDistrict(e.value, e.label)}
                                        />
                                    </div>

                                </FormGroup>

                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '22%', textAlign: 'end', paddingRight: '10px' }}
                                        for="kabupaten"
                                    >Kabupaten</Label>
                                    <div style={{ width: '78%' }}>
                                        <Select
                                            options={listDistrict}
                                            placeholder={districtFocus ? "Pilih Kabupaten" : city != "" ? city : "Ketik Kabupaten"}
                                            onFocus={() => setDistrictFocus(true)}
                                            onBlur={() => setDistrictFocus(false)}
                                            defaultOptions={true}
                                            onChange={(e) => setterDistrict(e.value, e.label)}
                                        />
                                    </div>
                                </FormGroup>

                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label for="postal-code" style={{ width: '22%', textAlign: 'end', paddingRight: '10px' }}>Kode Pos</Label>
                                    <div style={{ width: '78%' }}>
                                        <Input type="text" id="postal-code" value={postalCode} onChange={(e) => setPostalCode(e.target.value)} />
                                    </div>
                                </FormGroup>

                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label for="education" style={{ width: '22%', textAlign: 'end', paddingRight: '10px' }}>Pendidikan</Label>
                                    <div style={{ width: '78%' }}>
                                        <Input type="select" name="education" id="education" value={education} onChange={education} onChange={(e) => setEducation(e.target.value)}>
                                            <option value="">Pilih Pendidikan</option>
                                            <option value="elementary-school">SD/MI/Sederajat</option>
                                            <option value="junior-high-school">SMP/Mts/Sederajat</option>
                                            <option value="senior-high-school">SMA/SMK/Sederajat</option>
                                            <option value="strata-1">S1</option>
                                            <option value="strata-2">S2</option>
                                            <option value="strata-3">S3</option>
                                        </Input>
                                    </div>
                                </FormGroup>

                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label for="work" style={{ width: '22%', textAlign: 'end', paddingRight: '10px' }}>Pekerjaan</Label>
                                    <div style={{ width: '78%' }}>
                                        <Input type="select" name="work" id="work" value={profession} onChange={(e) => setProfession(e.target.value)}>
                                            <option value="">Pilih Pekerjaan</option>
                                            <option value="private-employees">Pegawai Swasta</option>
                                            <option value="goverment-employees">Pegawai Negeri</option>
                                            <option value="entrepreneur">Wiraswasta</option>
                                            <option value="student">Mahasiswa/Pelajar</option>
                                        </Input>
                                    </div>
                                </FormGroup>
                            </ModalBody>

                }
                <ModalFooter>
                    <Button color="danger" onClick={updateAsesor}>Simpan</Button>{' '}
                    <Button color="warning" onClick={toggleUpdateClose}>Batal</Button>
                </ModalFooter>
            </Modal>

        );
    }

    const showModalDelete = () => {
        return (
            <Modal isOpen={modalDelete} toggle={toggleDeleteClose} size="lg" style={{ maxWidth: '700px', width: '100%' }} backdrop="static">
                <ModalHeader toggle={toggleDeleteClose}>Hapus Asesor</ModalHeader>
                {
                    loadingModalDelete ?
                        <ModalBody style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                            <Spinner size="lg" color="blue" />
                            <div>Loading...</div>
                        </ModalBody>
                        :
                        savingModalDelete ?
                            <ModalBody style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <Spinner size="lg" color="blue" />
                                <div>Saving...</div>
                            </ModalBody>
                            :
                            <ModalBody>
                                Apakah Anda yakin ingin menghapus data ini ?
                            </ModalBody>

                }
                <ModalFooter>
                    <Button color="danger" onClick={deleteAsesor}>Ya</Button>{' '}
                    <Button color="warning" onClick={toggleDeleteClose}>Tidak</Button>
                </ModalFooter>
            </Modal>

        );
    }

    const showModalView = () => {
        return (
            <Modal isOpen={modalView} toggle={toggleViewClose} size="lg" style={{ maxWidth: '700px', width: '100%' }} backdrop="static">
                <ModalHeader toggle={toggleViewClose}>Detail Asesor</ModalHeader>
                {
                    loadingModalView ?
                        <ModalBody style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                            <Spinner size="lg" color="blue" />
                            <div>Loading...</div>
                        </ModalBody>
                        :
                        savingModalView ?
                            <ModalBody style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <Spinner size="lg" color="blue" />
                                <div>Saving...</div>
                            </ModalBody>
                            :
                            <ModalBody>
                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="fullname"
                                    >Nama Lengkap</Label>
                                    <Input type="text" name="fullname" id="fullname" value={name} onChange={(e) => setName(e.target.value)} style={{ width: '85%' }} disabled />
                                </FormGroup>


                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="email"
                                        disabled
                                    >Email</Label>
                                    <Input type="email" name="email" id="email" value={email} onChange={(e) => setEmail(e.target.value)} style={{ width: '85%' }} disabled />
                                </FormGroup>

                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="nik"
                                    >NIK(KTP)</Label>
                                    <Input type="number" name="nik" id="nik" value={nik} onChange={(e) => setNIK(e.target.value)} style={{ width: '85%' }} disabled />
                                </FormGroup>

                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="telephone"
                                    >No.Handphone</Label>
                                    <Input type="number" name="telephone" id="telephone" value={phone} onChange={(e) => setPhone(e.target.value)} style={{ width: '85%' }} disabled />
                                </FormGroup>

                                <FormGroup for="gender" style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label style={{ width: '23%', textAlign: 'end', paddingRight: '10px' }}>Jenis Kelamin</Label>
                                    <div style={{ width: '77%' }}>
                                        <Input
                                            type="select"
                                            name="gender"
                                            id="gender"
                                            value={gender}
                                            onChange={(e) => setGender(e.target.value)}
                                            disabled>
                                            <option value="">Pilih Jenis Kelamin</option>
                                            <option value="male">Pria</option>
                                            <option value="female">Wanita</option>
                                        </Input>
                                    </div>
                                </FormGroup>

                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label for="pob" style={{ width: '23%', textAlign: 'end', paddingRight: '10px' }}>Tempat Lahir</Label>
                                    <div style={{ width: '77%' }}>
                                        <Input type="text" id="pob" value={pob} onChange={(e) => setPob(e.target.value)} disabled />
                                    </div>
                                </FormGroup>

                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label for="dob" style={{ width: '23%', textAlign: 'end', paddingRight: '10px' }}>Tanggal Lahir</Label>
                                    <div style={{ width: '77%' }}>
                                        <Input type="date" name="dob" id="dob" value={dob} onChange={(e) => setDob(e.target.value)} disabled />
                                    </div>
                                </FormGroup>

                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="address"
                                    >Alamat</Label>
                                    <Input type="text" name="address" id="address" value={address} onChange={(e) => setAddress(e.target.value)} style={{ width: '85%' }} disabled />
                                </FormGroup>

                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '22%', textAlign: 'end', paddingRight: '10px' }}
                                        for="provinsi"
                                    >Provinsi</Label>
                                    <div style={{ width: '78%' }}>
                                        <AsyncSelect
                                            styles={{ width: '50%' }}
                                            loadOptions={getProvince}
                                            placeholder={provinceFocus ? "Pilih Provinsi" : province != "" ? province : "Ketik Provinsi"}
                                            onFocus={() => setProvinceFocus(true)}
                                            onBlur={() => setProvinceFocus(false)}
                                            defaultOptions={true}
                                            isDisabled={true}
                                            onChange={(e) => setterProvinceAndDistrict(e.value, e.label)}
                                        />
                                    </div>

                                </FormGroup>

                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '22%', textAlign: 'end', paddingRight: '10px' }}
                                        for="kabupaten"
                                    >Kabupaten</Label>
                                    <div style={{ width: '78%' }}>
                                        <Select
                                            options={listDistrict}
                                            placeholder={districtFocus ? "Pilih Kabupaten" : city != "" ? city : "Ketik Kabupaten"}
                                            onFocus={() => setDistrictFocus(true)}
                                            onBlur={() => setDistrictFocus(false)}
                                            defaultOptions={true}
                                            isDisabled={true}
                                            onChange={(e) => setterDistrict(e.value, e.label)}
                                        />
                                    </div>
                                </FormGroup>

                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label for="postal-code" style={{ width: '22%', textAlign: 'end', paddingRight: '10px' }}>Kode Pos</Label>
                                    <div style={{ width: '78%' }}>
                                        <Input type="text" id="postal-code" value={postalCode} onChange={(e) => setPostalCode(e.target.value)} disabled />
                                    </div>
                                </FormGroup>

                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label for="education" style={{ width: '22%', textAlign: 'end', paddingRight: '10px' }}>Pendidikan</Label>
                                    <div style={{ width: '78%' }}>
                                        <Input type="select" name="education" id="education" value={education} onChange={education} onChange={(e) => setEducation(e.target.value)} disabled>
                                            <option value="">Pilih Pendidikan</option>
                                            <option value="elementary-school">SD/MI/Sederajat</option>
                                            <option value="junior-high-school">SMP/Mts/Sederajat</option>
                                            <option value="senior-high-school">SMA/SMK/Sederajat</option>
                                            <option value="strata-1">S1</option>
                                            <option value="strata-2">S2</option>
                                            <option value="strata-3">S3</option>
                                        </Input>
                                    </div>
                                </FormGroup>

                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label for="work" style={{ width: '22%', textAlign: 'end', paddingRight: '10px' }}>Pekerjaan</Label>
                                    <div style={{ width: '78%' }}>
                                        <Input type="select" name="work" id="work" value={profession} onChange={(e) => setProfession(e.target.value)} disabled>
                                            <option value="">Pilih Pekerjaan</option>
                                            <option value="private-employees">Pegawai Swasta</option>
                                            <option value="goverment-employees">Pegawai Negeri</option>
                                            <option value="entrepreneur">Wiraswasta</option>
                                            <option value="student">Mahasiswa/Pelajar</option>
                                        </Input>
                                    </div>
                                </FormGroup>
                            </ModalBody>

                }
                <ModalFooter>
                    {/* <Button color="danger" onClick={toggleViewOpen}>Simpan</Button>{' '} */}
                    <Button color="warning" onClick={toggleViewClose}>Tutup</Button>
                </ModalFooter>
            </Modal>

        );
    }



    if (isLoading) {
        return (
            <SpinnerLoading />
        );
    }

    return (
        <div style={styles.body}>
            <div style={styles.searchAndSort}>
                <div style={{ width: '80%' }}>
                    <InputGroup>
                        <Input placeholder=" Masukkan Keyword" onChange={(e) => setKeywordSearch(e.target.value)} onKeyUp={(e) => e.key === "Enter" ? searchAsesor() : null} />
                        <InputGroupText>
                            <AiOutlineSearch onClick={searchAsesor} />
                        </InputGroupText>
                    </InputGroup>
                </div>
                <div style={{ marginLeft: '5px', marginRight: '5px' }} />
                <div style={{ width: '20%' }}>
                    <Input
                        id="exampleSelect"
                        name="select"
                        type="select"
                        onChange={(e) => sortAsesor(e.target.value)}
                    >
                        <option value="">Sortir Berdasarkan...</option>
                        <option value="name_asc">Nama(A-Z)</option>
                        <option value="name_desc">Nama(Z-A)</option>
                        <option value="email_asc">Email(A-Z)</option>
                        <option value="email_desc">Email(Z-A)</option>
                    </Input>
                </div>
            </div>
            <div>
            </div>
            <div style={styles.table}>
                <Table
                    bordered
                    // responsive
                    striped
                >
                    <thead>
                        <tr style={{ background: 'black', color: 'white' }}>
                            <th style={{ width: '5%' }}>No</th>
                            <th>Profile Asesor</th>
                            <th style={{ width: '20%', textAlign: 'center' }}>Aksi</th>
                        </tr>
                    </thead>
                    {
                        loadingSearch || loadingSort || loadingPage ?
                            <tbody style={{ display: 'flex', justifyContent: 'center', justifyContent: 'center', textAlign: 'center', flexDirection: 'column' }}>
                                Loading....
                            </tbody>
                            :
                            <tbody>
                                {
                                    listAsesor.map((item, index) => (
                                        <tr>
                                            <td>{index + indexPage}</td>
                                            <td>
                                                <Row>
                                                    <Col xs="3">Nama Asesor:</Col>
                                                    <Col>{item.name}</Col>
                                                </Row>
                                                <Row>
                                                    <Col xs="3">Email:</Col>
                                                    <Col>{item.email}</Col>
                                                </Row>
                                            </td>
                                            <td>
                                                <Row>
                                                    <Col xs="3">
                                                        <Button color="primary"
                                                            onClick={() => toggleUpdateOpen(item.id)}>
                                                            <AiFillEdit />
                                                        </Button></Col>
                                                    <Col xs="3"><Button color="danger" onClick={() => toggleDeleteOpen(item.id)}><AiFillDelete /></Button></Col>
                                                    <Col xs="3"><Button color="success" onClick={() => toggleViewOpen(item.id)}><AiFillFolderOpen /></Button></Col>
                                                </Row>
                                            </td>
                                        </tr>
                                    ))
                                }
                            </tbody>
                    }

                </Table>
            </div>
            <div style={styles.addAndPagination}>
                <div style={{ width: '50%', display: 'flex', justifyContent: 'flex-start' }}>
                    <Button onClick={toggleCreate}>
                        <AiOutlineUserAdd />   Tambah Asesor
                    </Button>
                </div>
                <div style={{ width: '50%', display: 'flex', justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                    <Pagination aria-label="Page navigation example">
                        <PaginationItem>
                            <PaginationLink
                                onClick={nextPageAsesorDec}
                                previous
                            />
                        </PaginationItem>
                        <div style={{ width: '100px', display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <div style={{ width: '60px' }}>
                                <Input type="number" style={{ textAlign: 'center' }} value={page} onChange={(e) => setPage(e.target.value)} onKeyUp={(e) => e.key === "Enter" ? nextPageAsesiEnter() : null} />
                            </div>
                            <div style={{ width: '40px' }}>
                                /{totalPage}
                            </div>
                        </div>
                        <PaginationItem>
                            <PaginationLink
                                onClick={nextPageAsesorInc}
                                next
                            />
                        </PaginationItem>
                    </Pagination>
                </div>
            </div>
            {showModalCreate()}
            {showModalUpdate()}
            {showModalDelete()}
            {showModalView()}
        </div>
    );
}

const styles = {
    body: {
        width: '100%',
        height: '95%',
        paddingRight: '2%',
        paddingLeft: '2%'
    },
    searchAndSort: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        marginTop: '15px'
    },
    table: {
        width: '100%',
        marginTop: '15px'
    },
    addAndPagination: {
        width: '100%',
        marginTop: '15px',
        display: 'flex',
        flexDirection: 'row',
    },
}