import React, { useEffect, useState } from 'react';
import SpinnerLoading from '../../components/SpinnerLoading';
import axios from 'axios';
import { Button, Col, Input, InputGroup, InputGroupText, Modal, ModalBody, ModalFooter, ModalHeader, Pagination, PaginationItem, PaginationLink, Row, Spinner, Table } from 'reactstrap';
import { AiFillEdit, AiFillDelete, AiFillFolderOpen, AiOutlineSearch, AiOutlineUserAdd, AiOutlineCheck, AiOutlineClose } from "react-icons/ai";
import Select from 'react-select';
import { APIURL } from '../../helper/LinkAPI';
import AsyncSelect from 'react-select/async';
import { convertDate, convertTime } from '../../utils/DateTimeConverter';
import ModalSpinner from '../../components/ModalSpinner';


export default function ProposeNewAsesmentPage() {
    const [next, setNext] = useState(1);
    const [isLoading, setIsLoading] = useState(false);
    const [asesi, setAsesi] = useState(null);
    const [document, setDocument] = useState(null);
    const [schedule, setSchedule] = useState(null);
    const [isLoadingContent, setIsLoadingContent] = useState(false);
    const [isLoadingProgress, setIsLoadingProgress] = useState(false);
    const [selectedFile, setSelectedFile] = useState(null);
    const [isFilePicked, setIsFilePicked] = useState(false);
    const [listAsesi, setListAsesi] = useState([]);
    const [id, setId] = useState(null);
    const [modalApprove, setModalApprove] = useState(false);
    const [modalDecline, setModalDecline] = useState(false);
    const [savingModalApprove, setSavingModalApprove] = useState(false);
    const [savingModalDecline, setSavingModalDecline] = useState(false);
    const [loadingContent, setLoadingContent] = useState(true);

    useEffect(() => {
        getAsesi();
    }, []);

    const getAsesi = async () => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + "/asesi-registrations/dropdown-list?track=1", {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });
            const data = result.data.data;
            const tempArrayAsesi = [];
            console.log("get Asesi: " + JSON.stringify(data));

            if (result.status == 200) {
                const asesi = data;
                setLoadingContent(false);
                if (asesi.length) {
                    asesi.forEach((element) => {
                        tempArrayAsesi.push({
                            label: element.name,
                            value: element.id
                        });
                    });
                } else {
                    tempArrayAsesi.push({
                        label: '',
                        value: ''
                    });
                }
                setListAsesi(tempArrayAsesi);
            } else {
                setListAsesi([]);
            }
            return true;

        } catch (error) {
            // console.log("error: " + error);
            setListAsesi([]);
            return false;
        }
    }

    const getScheduleAndDocument = async (id) => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        setId(id);
        setLoadingContent(true);
        try {
            const result = await axios.get(APIURL + `/assesment/document-apl/${id}`, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const dataSchedule = result.data.data?.created_at;
                const dataDocument = result.data?.data;
                setLoadingContent(false);
                if (status === "success") {
                    console.log("data schedule success: " + JSON.stringify(dataSchedule));
                    console.log("data document success: " + JSON.stringify(dataDocument));
                    setSchedule(dataSchedule);
                    setDocument(dataDocument);
                    setTimeout(() => {
                        setIsLoading(false)
                    }, 2000);
                }
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                // history.push('/asesi');
            } else {
                alert("Maaf data belum tersedia");
                // history.push('/asesi');
            }
        }
    }

    const changeHandler = (event, index, document, type) => {
        try {
            console.log("index: " + index);
            console.log(event.target.files[0]);
            setSelectedFile(event.target.files[0]);
            setIsFilePicked(true);
            // handleChange(index, true);
            setIsLoadingProgress(true);
            setTimeout(() => {
                if (document != null) {
                    if (type == "update") {
                        updateDocument(document, event.target.files[0]);
                    } else {
                        createDocument(document, event.target.files[0]);
                    }
                    console.log("document != null");
                } else {
                    console.log("document == null");
                }
            }, 1000);
        } catch (error) {
            console.log(null);
            setSelectedFile(null);
            setIsFilePicked(false);
            // handleChange(index, false);
        }
    };

    const updateDocument = async (document, files) => {
        console.log('res update: ' + JSON.stringify(document));
        const token = localStorage.getItem("token");
        const formData = new FormData();
        formData.append("status", "wait");
        formData.append("praassesment_method_document_id", document.praassesment_method_document_id);
        formData.append("asesi_registration_id", document.asesi_registration_id);
        formData.append("file", files);
        // setIsLoadingProgress(true);
        try {
            const result = await axios.post(APIURL + `/praassesment/answer/${document.id}?_method=PUT`, formData, {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "multipart/form-data",
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                // setIsLoadingProgress(false);
                if (status === "success") {
                    refreshDocument().then((value) => {
                        console.log("val: " + value);
                        if (value) {
                            alert("Update Success");
                            setIsLoadingProgress(false);
                        } else {
                            alert("Update Success, but not refreshed");
                            setIsLoadingProgress(false);
                        }
                    });
                }
            } else {
                alert("Update Bad");
                setIsLoadingProgress(false);

            }
        } catch (error) {
            alert("Update Error: " + error);
            setIsLoadingProgress(false);
        }

    }


    const createDocument = async (document, files) => {
        console.log('res doc: ' + JSON.stringify(document));
        console.log('res files: ' + JSON.stringify(files));
        const token = localStorage.getItem("token");
        const formData = new FormData();
        formData.append("status", "wait");
        formData.append("praassesment_method_document_id", document.id);
        formData.append("asesi_registration_id", id);
        formData.append("file", files);
        try {
            const result = await axios.post(APIURL + "/praassesment/answer", formData, {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "multipart/form-data",
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                setIsLoadingProgress(false);
                if (status === "success") {
                    refreshDocument().then((value) => {
                        console.log("val: " + value);
                        if (value) {
                            alert("Create Success");
                            setIsLoadingProgress(false);
                        } else {
                            alert("Create Success, but not refreshed");
                            setIsLoadingProgress(false);
                        }
                    });
                }
            } else {
                alert("Create Bad");
                setIsLoadingProgress(false);

            }
        } catch (error) {
            alert("Create Error: " + error);
            setIsLoadingProgress(false);
        }
    }

    const refreshDocument = async () => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + `/assesment-schedule/pra-assesment/${id}`, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const dataSchedule = result.data.data.created_at;
                const dataDocument = result.data.data;
                if (status === "success") {
                    console.log("data schedule success: " + JSON.stringify(dataSchedule));
                    console.log("data document success: " + JSON.stringify(dataDocument));
                    setDocument(dataDocument);
                } else {
                    console.log("data schedule unsuccess: " + JSON.stringify(dataSchedule));
                    console.log("data document unsuccess: " + JSON.stringify(dataDocument));
                }

                return true;
            } else {
                console.log("bad request");
                return false;
            }
        } catch (error) {
            alert("Maaf data belum tersedia");
            console.log(error);
            return false;
        }
    }

    const approveProcess = async () => {
        const token = localStorage.getItem("token");
        setSavingModalApprove(true);
        try {
            const result = await axios.put(APIURL + `/assesment/propose-new-asesment/${id}`, {
                "track": 2
            }, {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json",
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                if (status === "success") {
                    alert("Approve Success");
                    getAsesi().then((value) => {
                        if (value) {
                            setSavingModalApprove(false);
                            toggleApproveClose();
                        } else {
                            setSavingModalApprove(false);
                            alert("Data tidak terefresh");
                        }
                    });
                }
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                setSavingModalApprove(false);
            } else {
                alert("Error: " + error.response.data?.data?.error);
                setSavingModalApprove(false);
            }
        }
    }


    const toggleApproveOpen = () => {
        if (id != null) {
            console.log("id: " + JSON.stringify(id));
            setId(id);
            setModalApprove(!modalApprove);
        } else {
            alert("Get data failed");
        }
    }

    const toggleApproveClose = () => {
        setModalApprove(!modalApprove);
    }

    const declineProcess = async () => {
        const token = localStorage.getItem("token");
        setSavingModalDecline(true);
        try {
            const result = await axios.put(APIURL + `/assesment/propose-new-asesment/${id}`, {
                "track": 10
            }, {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json",
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                if (status === "success") {
                    alert("Decline Success");
                    getAsesi().then((value) => {
                        if (value) {
                            setSavingModalDecline(false);
                            toggleDeclineClose();
                        } else {
                            setSavingModalDecline(false);
                            alert("Data tidak terefresh");
                        }
                    })
                }
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                setSavingModalDecline(false);
            } else {
                alert("Error: " + error.response.data?.data?.error);
                setSavingModalDecline(false);
            }
        }
    }

    const toggleDeclineOpen = () => {
        if (id != null) {
            console.log("id: " + JSON.stringify(id));
            setId(id);
            setModalDecline(!modalDecline);
        } else {
            alert("Get data failed");
        }
    }

    const toggleDeclineClose = () => {
        setModalDecline(!modalDecline);
    }


    const showModalApprove = () => {
        return (
            <Modal isOpen={modalApprove} toggle={toggleApproveClose} size="lg" style={{ maxWidth: '700px', width: '100%' }} backdrop="static">
                <ModalHeader toggle={toggleApproveClose}>Approve Calon Asesi</ModalHeader>
                {
                    savingModalApprove ?
                        <ModalBody style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                            <Spinner size="lg" color="blue" />
                            <div>Saving...</div>
                        </ModalBody>
                        :
                        <ModalBody>
                            Apakah Anda yakin ingin meng-approve calon asesi tersebut ?
                        </ModalBody>

                }
                <ModalFooter>
                    <Button color="danger" onClick={approveProcess}>Ya</Button>{' '}
                    <Button color="warning" onClick={toggleApproveClose}>Tidak</Button>
                </ModalFooter>
            </Modal>

        );
    }

    const showModalDecline = () => {
        return (
            <Modal isOpen={modalDecline} toggle={toggleDeclineClose} size="lg" style={{ maxWidth: '700px', width: '100%' }} backdrop="static">
                <ModalHeader toggle={toggleDeclineClose}>Decline Calon Asesi</ModalHeader>
                {
                    savingModalDecline ?
                        <ModalBody style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                            <Spinner size="lg" color="blue" />
                            <div>Saving...</div>
                        </ModalBody>
                        :
                        <ModalBody>
                            Apakah Anda yakin ingin meng-decline calon asesi tersebut ?
                        </ModalBody>

                }
                <ModalFooter>
                    <Button color="danger" onClick={declineProcess}>Ya</Button>{' '}
                    <Button color="warning" onClick={toggleDeclineClose}>Tidak</Button>
                </ModalFooter>
            </Modal>

        );
    }

    return (
        <div style={styles.body}>

            <div style={{ width: '100%', background: 'white' }}>
                <div style={{ width: '100%', background: 'rgb(233, 233, 233)', marginTop: '15px' }}>
                    <div style={{ width: '50%' }}>
                        <Table>
                            <tr>
                                <td style={{ width: '35%' }}>Nama Calon Asesi</td>
                                <td>:</td>
                                <td>
                                    <Select
                                        options={listAsesi}
                                        placeholder={"Nama Asesi"}
                                        defaultOptions={true}
                                        onChange={(e) => getScheduleAndDocument(e.value)}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td>Tanggal Pengajuan</td>
                                <td>:</td>
                                <td>
                                    {/* {schedule != null ? convertDate(schedule.date_schedule) : ""} */}
                                    {/* {schedule != null?schedule: null} */}
                                    {schedule != null ? schedule : ""}
                                </td>
                            </tr>
                            <tr>
                                <td>Lampiran</td>
                                <td>:</td>
                                <td></td>
                            </tr>
                        </Table>
                    </div>
                </div>
                {
                    isLoadingContent ?
                        <div style={styles.table}>
                            <Table
                                bordered
                                // responsive
                                striped
                            >
                                <thead>
                                    <tr style={{ background: '#B3936A', color: 'white' }}>
                                        <th style={{ width: '5%' }}>No</th>
                                        <th>Form</th>
                                        <th style={{ width: '20%', textAlign: 'center' }}>Aksi</th>
                                    </tr>
                                </thead>

                            </Table>
                        </div> :
                        <div style={styles.table}>
                            <Table
                                bordered
                                striped
                            >
                                <thead>
                                    <tr style={{ background: '#B3936A', color: 'white' }}>
                                        <th style={{ width: '5%' }}>No</th>
                                        <th>Form</th>
                                        <th style={{ width: '20%', textAlign: 'center' }}>Aksi</th>
                                    </tr>
                                </thead>
                                {
                                    loadingContent ?
                                        <tbody>
                                            <tr>Loading...</tr>
                                        </tbody>
                                        :
                                        document != null ?
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>
                                                        FR APL 01 dan FR APL 02
                                                    </td>
                                                    <td style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                                        <Row>
                                                            <Button color="success"
                                                                onClick={() => {
                                                                    document.convert_pdf != null ? window.open(`https://test.devinfolspbuma.online/public/api${document.convert_pdf}`, '_blank') : alert("Dokumen tidak tersedia")
                                                                }}
                                                            >
                                                                <AiFillFolderOpen />
                                                            </Button>
                                                        </Row>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>
                                                        KTP
                                                    </td>
                                                    <td style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                                        <Row>
                                                            <Button color="success"
                                                                onClick={() => {
                                                                    document.ktp != null ? window.open(`https://test.devinfolspbuma.online/public/storage${document.ktp}`, '_blank') : alert("Dokumen tidak tersedia")
                                                                }}
                                                            >
                                                                <AiFillFolderOpen />
                                                            </Button>
                                                        </Row>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>
                                                        Diploma
                                                    </td>
                                                    <td style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                                        <Row>
                                                            <Button color="success"
                                                                onClick={() => {
                                                                    document.diploma != null ? window.open(`https://test.devinfolspbuma.online/public/storage${document.diploma}`, '_blank') : alert("Dokumen tidak tersedia")
                                                                }}
                                                            >
                                                                <AiFillFolderOpen />
                                                            </Button>
                                                        </Row>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>4</td>
                                                    <td>
                                                        Training Certificate
                                                    </td>
                                                    <td style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                                        <Row>
                                                            <Button color="success"
                                                                onClick={() => {
                                                                    document.relevant_training_certificate != null ? window.open(`https://test.devinfolspbuma.online/public/storage${document.relevant_training_certificate}`, '_blank') : alert("Dokumen tidak tersedia")
                                                                }}
                                                            >
                                                                <AiFillFolderOpen />
                                                            </Button>
                                                        </Row>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>5</td>
                                                    <td>
                                                        Unit Operation Certificate
                                                    </td>
                                                    <td style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                                        <Row>
                                                            <Button color="success"
                                                                onClick={() => {
                                                                    document.unit_operation_certificate != null ? window.open(`https://test.devinfolspbuma.online/public/storage${document.unit_operation_certificate}`, '_blank') : alert("Dokumen tidak tersedia")
                                                                }}
                                                            >
                                                                <AiFillFolderOpen />
                                                            </Button>
                                                        </Row>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>6</td>
                                                    <td>
                                                        Versality Unit
                                                    </td>
                                                    <td style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                                        <Row>
                                                            <Button color="success"
                                                                onClick={() => {
                                                                    document.versality_unit != null ? window.open(`https://test.devinfolspbuma.online/public/storage${document.versality_unit}`, '_blank') : alert("Dokumen tidak tersedia")
                                                                }}
                                                            >
                                                                <AiFillFolderOpen />
                                                            </Button>
                                                        </Row>
                                                    </td>
                                                </tr>
                                            </tbody> :
                                            <tbody></tbody>
                                }
                            </Table>
                        </div>
                }
                <div style={styles.decision}>
                    <Button style={{ width: '20%' }} color="primary" onClick={toggleApproveOpen}><AiOutlineCheck />Approve</Button>
                    <div style={{ marginLeft: '5px', marginRight: '5px' }} />
                    <Button style={{ width: '20%' }} color="danger" onClick={toggleDeclineOpen}><AiOutlineClose />Decline</Button>
                </div>
            </div>
            <ModalSpinner statusOpen={isLoadingProgress} toggle={isLoadingProgress} />
            {showModalApprove()}
            {showModalDecline()}
        </div>
    );
}

const styles = {
    body: {
        width: '100%',
        height: '95%',
        paddingRight: '2%',
        paddingLeft: '2%'
    },
    searchAndSort: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        marginTop: '15px'
    },
    table: {
        width: '100%',
        // marginTop: '15px',
        background: 'rgb(233, 233, 233)'
    },
    addAndPagination: {
        width: '100%',
        marginTop: '15px',
        display: 'flex',
        flexDirection: 'row',
    },
    decision: {
        width: '100%',
        background: 'white',
        // marginTop: '15px',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
}