import React, { useEffect, useState } from 'react';
import SpinnerLoading from '../../components/SpinnerLoading';
import axios from 'axios';
import { Button, Col, Input, InputGroup, InputGroupText, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';
import { AiFillEdit, AiFillDelete, AiFillFolderOpen, AiOutlineSearch, AiOutlineUserAdd } from "react-icons/ai";
import { APIURL } from '../../helper/LinkAPI';

export default function ListScheme() {
    const [next, setNext] = useState(1);
    const [isLoading, setIsLoading] = useState(true);
    const [listScheme, setListScheme] = useState([]);
    const [page, setPage] = useState(1);

    // useEffect(() => {
    //     window.scroll({
    //         top: 0,
    //         left: 0,
    //         behavior: 'smooth'
    //     });
    // }, [next]);

    useEffect(() => {
        getListScheme();
    }, []);


    const getListScheme = async () => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + `/schematic-certification?page=${page}`, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                if (status === "success") {
                    console.log("data success: " + JSON.stringify(data[0].data));
                    setListScheme(data[0].data);
                    setTimeout(() => {
                        setIsLoading(false)
                    }, 1000);
                } else {
                    console.log("data unsuccess: " + JSON.stringify(data));
                }
            } else {
                console.log("bad request");
            }
        } catch (error) {
            console.log("error request");
        }
    }

    if (isLoading) {
        return (
            <SpinnerLoading />
        );
    }

    return (
        <div style={styles.body}>
            <div style={styles.searchAndSort}>
                <div style={{ width: '80%' }}>
                    <InputGroup>
                        <Input placeholder=" Masukkan Keyword" />
                        <InputGroupText>
                            <AiOutlineSearch />
                        </InputGroupText>
                    </InputGroup>
                </div>
                <div style={{ marginLeft: '5px', marginRight: '5px' }} />
                <div style={{ width: '20%' }}>
                    <Input
                        id="exampleSelect"
                        name="select"
                        type="select"
                    >
                        <option>Sortir Berdasarkan...</option>
                    </Input>
                </div>
            </div>
            <div>
            </div>
            <div style={styles.table}>
                <Table
                    bordered
                    // responsive
                    striped
                >
                    <thead>
                        <tr style={{ background: 'black', color: 'white' }}>
                            <th style={{ width: '5%' }}>No</th>
                            <th>Nama Skema</th>
                            <th style={{ width: '20%', textAlign: 'center' }}>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            listScheme.map((item, index) => (
                                <tr>
                                    <td>{index + 1}</td>
                                    <td>
                                            <Col xs="5">{item.name}</Col>
                                    </td>
                                    <td>
                                        <Row>
                                            <Col xs="3"><Button color="primary"><AiFillEdit /></Button></Col>
                                            <Col xs="3"><Button color="danger"><AiFillDelete /></Button></Col>
                                            <Col xs="3"><Button color="success"><AiFillFolderOpen /></Button></Col>
                                        </Row>
                                    </td>
                                </tr>
                            ))
                        }
                    </tbody>
                </Table>
            </div>
            <div style={styles.addAndPagination}>
                <div style={{ width: '50%', display: 'flex', justifyContent: 'flex-start' }}>
                    <Button>
                        <AiOutlineUserAdd />   Tambah Skema
                    </Button>
                </div>
                <div style={{ width: '50%', display: 'flex', justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                    <Pagination aria-label="Page navigation example">
                        <PaginationItem disabled>
                            <PaginationLink
                                first
                                href="#"
                            />
                        </PaginationItem>
                        <PaginationItem disabled>
                            <PaginationLink
                                href="#"
                                previous
                            />
                        </PaginationItem>
                        <PaginationItem active>
                            <PaginationLink href="#">
                                1
                            </PaginationLink>
                        </PaginationItem>
                        <PaginationItem>
                            <PaginationLink href="#">
                                2
                            </PaginationLink>
                        </PaginationItem>
                        <PaginationItem>
                            <PaginationLink href="#">
                                3
                            </PaginationLink>
                        </PaginationItem>
                        <PaginationItem>
                            <PaginationLink
                                href="#"
                                next
                            />
                        </PaginationItem>
                    </Pagination>
                </div>
            </div>
        </div>
    );
}

const styles = {
    body: {
        width: '100%',
        height: '95%',
        paddingRight: '2%',
        paddingLeft: '2%'
    },
    searchAndSort: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        marginTop: '15px'
    },
    table: {
        width: '100%',
        marginTop: '15px'
    },
    addAndPagination: {
        width: '100%',
        marginTop: '15px',
        display: 'flex',
        flexDirection: 'row',
    },
}