import React, { useEffect, useState } from 'react';
import SpinnerLoading from '../../components/SpinnerLoading';
import axios from 'axios';
import { Button, Col, Input, InputGroup, InputGroupText, Modal, ModalBody, ModalFooter, ModalHeader, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';
import { AiFillEdit, AiFillDelete, AiFillFolderOpen, AiOutlineSearch, AiOutlineUserAdd, AiOutlineDownload, AiOutlineUpload } from "react-icons/ai";
import { APIURL } from '../../helper/LinkAPI';
import { convertDate, convertTime } from '../../utils/DateTimeConverter';

export default function AssesmentDocumentPage() {
    const [next, setNext] = useState(1);
    const [isLoading, setIsLoading] = useState(true);
    const [listData, setListData] = useState([]);
    const [modalView, setModalView] = useState(false);
    const [listDocument, setListDocument] = useState([]);
    const [asesorName, setAsesorName] = useState(null);

    useEffect(() => {
        getAssesmentDocument();
    }, []);

    useEffect(() => {
        window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });
    }, [next]);


    const getAssesmentDocument = async () => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + "/assesment-schedule/list?search=", {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            })

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                if (status === "success") {
                    console.log("data success: " + JSON.stringify(data));
                    setListData(data.data);
                    setTimeout(() => {
                        setIsLoading(false)
                    }, 1000);

                } else {
                    console.log("data unsuccess: " + JSON.stringify(data));
                }
            } else {
                console.log("bad request");
            }
        } catch (error) {

        }
    }

    const getViewDocument = async (id) => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        console.log("id: " + id);
        // setIsLoading(true);
        try {
            const result = await axios.get(APIURL + `/assesment-schedule/assesment/${id}`, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const dataSchedule = result.data.data;
                const dataDocument = result.data.data.assesment_method.assesment_document;
                if (status === "success") {
                    console.log("data success: " + JSON.stringify(result.data.data));
                    setListDocument(dataDocument);
                    // getDocument(dataDocument);
                    // setTimeout(() => {
                    //     setIsLoading(false);
                    //     setNext(2);
                    // }, 1000);
                }
                return true;
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                return false;
                // setIsLoading(false);
            } else {
                alert("Error: " + JSON.stringify(error.response.data?.data?.error));
                return false;
                // setIsLoading(false);
            }
        }
    }

    const toggleView = (id, name) => {
        setAsesorName(name);
        getViewDocument(id).then((value) => {
            if (value) {
                setModalView(!modalView);
            } else {
                setModalView(false);
            }
        });
    }

    const showModalViewDoc = () => {
        return (
            <Modal isOpen={modalView} toggle={() => modalView ? setModalView(false) : setModalView(false)} size="lg" style={{ maxWidth: '700px', width: '100%' }} backdrop="static">
                <ModalHeader toggle={() => modalView ? setModalView(false) : setModalView(false)}>Daftar Dokumen({asesorName})</ModalHeader>
                <ModalBody style={{ height: '400px', overflowY: 'scroll' }}>
                    <Table>
                        {
                            listDocument.map((item, index) => (
                                <tr>
                                    <td style={{ width: '5%' }}>{index + 1}</td>
                                    <td style={{ color: 'blue' }}>{item.name}</td>
                                    <td>
                                        {
                                            item.document_answer != null ? 
                                            <Button color="success" onClick={() => { window.open(`https://test.devinfolspbuma.online/public${item.document_answer.file}`, '_blank') }}>
                                                <AiOutlineDownload /> Download
                                            </Button>
                                            :
                                            <Button color="secondary" onClick={() => alert("Maaf file belum bisa di download")}>
                                                <AiOutlineDownload /> Download
                                            </Button>
                                        }
                                    </td>
                                </tr>
                            ))
                        }
                    </Table>
                </ModalBody>
                <ModalFooter>
                    <Button color="warning" onClick={() => modalView ? setModalView(false) : setModalView(false)}>Tutup</Button>
                </ModalFooter>
            </Modal>

        );
    }


    if (isLoading) {
        return (
            <SpinnerLoading />
        );
    }

    return (
        <div style={styles.body}>
            <div style={styles.searchAndSort}>
                <div style={{ width: '80%' }}>
                    <InputGroup>
                        <Input placeholder=" Masukkan Keyword" />
                        <InputGroupText>
                            <AiOutlineSearch />
                        </InputGroupText>
                    </InputGroup>
                </div>
                <div style={{ marginLeft: '5px', marginRight: '5px' }} />
                <div style={{ width: '20%' }}>
                    <Input
                        id="exampleSelect"
                        name="select"
                        type="select"
                    >
                        <option>Sortir Berdasarkan...</option>
                    </Input>
                </div>
            </div>
            <div>
            </div>
            <div style={styles.table}>
                <Table
                    bordered
                    // responsive
                    striped
                >
                    <thead>
                        <tr style={{ background: 'black', color: 'white' }}>
                            <th style={{ width: '5%' }}>No</th>
                            <th>Tanggal Asesmen</th>
                            <th>Tanggal Submit</th>
                            <th>Nama Asesor</th>
                            <th style={{ width: '10%', textAlign: 'center' }}>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            listData.map((item, index) => (
                                <tr>
                                    <td>{index + 1}</td>
                                    <td>{convertDate(item.date_schedule)}</td>
                                    <td style={{ textAlign: 'center' }}>-</td>
                                    <td>{item.assesor.name}</td>
                                    <td>
                                        <Row>
                                            <Col xs="1"><Button color="success" onClick={() => toggleView(item.asesi_registration_id, item.assesor.name)}><AiFillFolderOpen /></Button></Col>
                                        </Row>
                                    </td>
                                </tr>
                            ))
                        }
                    </tbody>
                </Table>
            </div>
            <div style={styles.pagination}>
                <div style={{ width: '100%', display: 'flex', justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                    <Pagination aria-label="Page navigation example">
                        <PaginationItem disabled>
                            <PaginationLink
                                first
                                href="#"
                            />
                        </PaginationItem>
                        <PaginationItem disabled>
                            <PaginationLink
                                href="#"
                                previous
                            />
                        </PaginationItem>
                        <PaginationItem active>
                            <PaginationLink href="#">
                                1
                            </PaginationLink>
                        </PaginationItem>
                        <PaginationItem>
                            <PaginationLink href="#">
                                2
                            </PaginationLink>
                        </PaginationItem>
                        <PaginationItem>
                            <PaginationLink href="#">
                                3
                            </PaginationLink>
                        </PaginationItem>
                        <PaginationItem>
                            <PaginationLink
                                href="#"
                                next
                            />
                        </PaginationItem>
                    </Pagination>
                </div>
            </div>
            {showModalViewDoc()}
        </div>
    );
}

const styles = {
    body: {
        width: '100%',
        height: '95%',
        paddingRight: '2%',
        paddingLeft: '2%'
    },
    searchAndSort: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        marginTop: '15px'
    },
    table: {
        width: '100%',
        marginTop: '15px'
    },
    pagination: {
        width: '100%',
        marginTop: '15px',
        display: 'flex'
    },
}