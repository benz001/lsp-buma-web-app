import React, { useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { FaUser, FaUserTie, FaUserLock } from "react-icons/fa";
import axios from 'axios';


export default function WelcomePage() {
    useEffect(()=>{
        // alert("Not Connected with API");
        // getProvince();
    },[]);

    let history = useHistory();
    return (
        <div style={styles.container}>
            <div style={{ width: '500px', fontSize: '18px', background: 'rgb(47, 85, 151)', color: 'white', textAlign: 'center' }}>
                <b>Selamat Datang di Aplikasi Asesmen LSP BUMA</b>
            </div>
            <div style={styles.headContainer}>
                Masuk Sebagai:
            </div>
            <div style={styles.subContainer}>
                <div style={styles.sectionAccessionAndAssesor}>
                    <div onClick={()=> history.push('/login-asesi')} style={styles.accession}>
                        <div style={{ width: '100%', height: '85%', justifyContent: 'center', alignItems: 'center', display: 'flex' }}>
                            <FaUser size={100} color="#B0131E" />
                        </div>
                        <div style={{ width: '100%', height: '15%', justifyContent: 'center', display: 'flex', background: 'rgb(47, 85, 151)', color: 'white', borderBottomLeftRadius: '5px', borderBottomRightRadius: '5px' }}>
                            <Link to='/login-asesi' style={{ color: 'white' }}>Asesi</Link>
                        </div>
                    </div>
                    <div style={styles.separator} />
                    <div onClick={()=> history.push('/login-asesor')} style={styles.assesor}>
                        <div style={{ width: '100%', height: '85%', justifyContent: 'center', alignItems: 'center', display: 'flex' }}>
                            <FaUserTie size={100} color="#B0131E" />
                        </div>
                        <div style={{ width: '100%', height: '15%', justifyContent: 'center', display: 'flex', background: 'rgb(47, 85, 151)', color: 'white', borderBottomLeftRadius: '5px', borderBottomRightRadius: '5px' }}>
                            <Link to='/login-asesor' style={{ color: 'white' }}>Asesor</Link>
                        </div>
                    </div>
                </div>

                <div style={styles.sectionAdmin}>
                    <div onClick={()=> history.push('/login-admin')} style={styles.admin}>
                        <div style={{ width: '100%', height: '85%', justifyContent: 'center', alignItems: 'center', display: 'flex' }}>
                            <FaUserLock size={100} color="#B0131E" />
                        </div>
                        <div style={{ width: '100%', height: '15%', justifyContent: 'center', display: 'flex', background: 'rgb(47, 85, 151)', color: 'white', borderBottomLeftRadius: '5px', borderBottomRightRadius: '5px' }}>
                            <Link to='/login-admin' style={{ color: 'white' }}> Admin </Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

const styles = {
    container: {
        width: '100%',
        height: '100%',
        background: 'lightgrey',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headContainer: {
        width: '500px',
        height: '25px',
        textAlign: 'center',
        background: 'rgb(163, 194, 194)',
        // marginTop: '10px'
    },
    subContainer: {
        width: '500px',
        height: '380px',
        background: 'rgb(163, 194, 194)',
        margin: '0 auto',
        display: 'flex',
        flexDirection: 'column'
    },
    sectionAccessionAndAssesor: {
        width: '100%',
        height: '50%',
        // background: 'green', 
        display: 'flex',
        flexDirection: 'row',
        padding: '15px'
    },
    accession: {
        width: '45%',
        height: '100%',
        background: 'white',
        display: 'flex',
        flexDirection: 'column',
        borderRadius: '5px'
    },
    separator: {
        width: '10%',
        height: '100%',
        background: 'transparent'
    },
    assesor: {
        width: '45%',
        height: '100%',
        background: 'white',
        borderRadius: '5px'
    },
    sectionAdmin: {
        width: '100%',
        height: '50%',
        // background: 'blue', 
        display: 'flex',
        justifyContent: 'center',
        paddingTop: '15px',
        paddingBottom: '15px'
    },
    admin: {
        width: '45%',
        height: '100%',
        background: 'white',
        display: 'flex',
        flexDirection: 'column',
        borderRadius: '5px'
    }

}