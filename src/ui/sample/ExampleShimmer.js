import React from 'react';
import { Shimmer } from 'react-shimmer';

export default function Exampleshimmer(){
    return (
        <div style={{ background: 'grey', width: '100%', height: '100%' }}>
            <Shimmer width={800} height={100}/>
        </div>
    );
}