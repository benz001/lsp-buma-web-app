import axios from 'axios';
import React, { useEffect, useState } from 'react';
import AsyncSelect from 'react-select/async';


export default function ExampleReactSelect() {

    const [selectedOption, setSelectedOption] = useState({});
    const [normalSelectOption, setNormalSelectOption] = useState(null);

    useEffect(()=>{
        console.log(fetchData())
    },[])


    const fetchData = async () => {
        try {
            const result = await axios.get("https://my-json-server.typicode.com/typicode/demo/comments/");
            const tempArray = [];
            const data = result.data;

            if (data.length) {
                data.forEach((element) => {
                    tempArray.push({
                        label: element.body,
                        value: element.id
                    });
                });
            } else {

                tempArray.push({
                    label: data.body,
                    value: data.id
                });

            }
            return tempArray;
        } catch (error) {
            console.log(error, "catch the hoop");
            return [];
        }


    }

    const onSearchChange = (selectedOption) => {
        if (selectedOption) {
            setSelectedOption(selectedOption);
        }
    }

    const handleChange = (normalSelectOption) => {
        setNormalSelectOption(normalSelectOption);
    }

    return (
        <div style={{ marginLeft: '40%', width: '200px' }}>
            <div>Remote Data</div>
            <AsyncSelect
                // value={selectedOption}
                loadOptions={fetchData}
                placeholder="Admin Name"
                onChange={(e) => onSearchChange(e)}
                // defaultOptions={true}
            />
        </div>
    );
}