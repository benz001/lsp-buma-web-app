import React, { useEffect } from 'react';
import { useHistory } from 'react-router';
import logo from '../assets/image/ic_logo_lsp_buma.png';
import axios from 'axios';
import { Spinner } from 'reactstrap';

export default function SplashPage() {
    let history = useHistory();
    useEffect(() => {
        checkAuth();
    }, []);

    const checkAuth = async () => {
        let roles = localStorage.getItem('roles');
        let isLogin = localStorage.getItem('isLogin');
        console.log("isLogin: " + isLogin);
        if (isLogin) {
            checkRoles(roles)
        } else {
            history.push('/welcome');
        }
    }

    const checkRoles = (roles) => {
        if (roles !== null) {
            switch (roles) {
                case "asesi":
                    setTimeout(() => {
                        history.push('/asesi');
                    }, 3000);
                    break;
                case "asesor":
                    setTimeout(() => {
                        history.push('/asesor');
                    }, 3000);
                    break;
                case "admin":
                    setTimeout(() => {
                        history.push('/admin');
                    }, 3000);
                    break;
                default:
                    setTimeout(() => {
                        history.push('/welcome');
                    }, 3000);
                    break;
            }
        } else {
            history.push('/welcome');
        }
    }
    return (
        <div style={{ width: '100%', height: '100%', background: 'lightgrey', display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
            <img src={logo} width={200} height={150} />
            <div>Loading...</div>
            <Spinner color="primary" style={{ width: '1.5rem', height: '1.5rem' }} />
        </div>
    );
}