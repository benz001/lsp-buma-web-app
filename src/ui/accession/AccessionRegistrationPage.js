import React, { useContext, useEffect, useState } from "react";
import { AiOutlineArrowLeft, AiOutlineArrowRight } from "react-icons/ai";
import { Button, Form, FormGroup, Label, Input, Spinner } from "reactstrap";
import CertificationData from "./CertificationDataPage";
import Select from "react-select";
import AccessionPersonalData from "./AccessionPersonalData";
import EmploymentData from "./AccessionEmploymentData";
import { Shimmer } from "react-shimmer";
import SpinnerLoading from "../../components/SpinnerLoading";
import axios from "axios";
import { Context } from "../../provider/provider";
import { checkThisEmpty } from "../../helper/CheckEmpty";
import { APIURL } from "../../helper/LinkAPI";
import ModalSpinner from "../../components/ModalSpinner";
import SelfAssessment from "./SelfAssessment";
import { useHistory } from 'react-router';
import { LOCAL_URL, WEB_URL } from "../../helper/LinkWeb";
import AccessionPersonalDataMemo from "./AccessionPersonalData";
import EmploymentDataMemo from "./AccessionEmploymentData";
import CertificationDataMemo from "./CertificationDataPage";
import SelfAssessmentMemo from "./SelfAssessment";

export default function AccessionRegistrationPage() {
    const [next, setNext] = useState(1);
    const [isLoading, setIsLoading] = useState(false);
    const { state, dispatch } = useContext(Context);
    const [close, setClose] = useState(false);
    const [isRegister, setIsRegister] = useState(null);
    let history = useHistory();

    useEffect(() => {
        const registrationAsesiId = localStorage.getItem("registration_asesi_id");
        console.log("already register: "+state.alreadyRegister.status);
        if (registrationAsesiId != null) {
            setNext(4);
        } else {
            setNext(1);
        }
    }, [])


    useEffect(() => {
        window.scroll({
            top: 0,
            left: 0,
            behavior: "smooth"
        });
    }, [next]);

    const nextToPageTwo = () => {
        if (
            checkThisEmpty(state.dataPersonal.name) ||
            checkThisEmpty(state.dataPersonal.email) ||
            checkThisEmpty(state.dataPersonal.nik) ||
            checkThisEmpty(state.dataPersonal.phoneNumber) ||
            checkThisEmpty(state.dataPersonal.gender) ||
            checkThisEmpty(state.dataPersonal.pob) ||
            checkThisEmpty(state.dataPersonal.dob) ||
            checkThisEmpty(state.dataPersonal.address) ||
            checkThisEmpty(state.dataPersonal.province) ||
            checkThisEmpty(state.dataPersonal.district) ||
            checkThisEmpty(state.dataPersonal.postalCode) ||
            checkThisEmpty(state.dataPersonal.education) ||
            checkThisEmpty(state.dataPersonal.profession)
        ) {
            alert("Field input tidak boleh kosong");
        } else {
            console.log("dataPersonal: " + JSON.stringify(state.dataPersonal));
            setNext(2)
        }
    }

    const nextToPageThree = () => {
        if (
            checkThisEmpty(state.dataEmployment.companyName) ||
            checkThisEmpty(state.dataEmployment.companyAddress) ||
            checkThisEmpty(state.dataEmployment.companyJobTitle) ||
            checkThisEmpty(state.dataEmployment.companyPhoneNumber) ||
            checkThisEmpty(state.dataEmployment.companyFaxNumber) ||
            checkThisEmpty(state.dataEmployment.companyEmail) ||
            checkThisEmpty(state.dataEmployment.photo)
        ) {
            alert("Field input tidak boleh kosong");
        } else {
            console.log("dataEmployment: " + JSON.stringify(state.dataEmployment));
            setNext(3);
        }

    }

    const submit = () => {
        if (
            checkThisEmpty(state.dataCertification.schematicCertification) ||
            checkThisEmpty(state.dataCertification.placeCompetence) ||
            checkThisEmpty(state.dataCertification.idCard) ||
            checkThisEmpty(state.dataCertification.certification) ||
            checkThisEmpty(state.dataCertification.certificationTraining) ||
            checkThisEmpty(state.dataCertification.unitOperationCertificate) ||
            checkThisEmpty(state.dataCertification.versalityUnit)
        ) {
            alert("Field input tidak boleh kosong");
        } else {
            console.log("dataCertification: " + JSON.stringify(state.dataCertification));
            submitProcess();
        }
    }

    const submitProcess = async () => {
        setIsLoading(true);
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        const formData = new FormData();
        formData.append("place_competence", state.dataCertification.placeCompetence);
        formData.append("schematic_certification", state.dataCertification.schematicCertification);
        formData.append("name", state.dataPersonal.name);
        formData.append("email", state.dataPersonal.email);
        formData.append("nik", state.dataPersonal.nik);
        formData.append("phone_number", state.dataPersonal.phoneNumber);
        formData.append("contact_number", state.dataPersonal.phoneNumber);
        formData.append("gender", state.dataPersonal.gender);
        formData.append("place_of_birth", state.dataPersonal.pob);
        formData.append("date_of_birth", state.dataPersonal.dob);
        formData.append("nationality", "Indonesia");
        formData.append("address", state.dataPersonal.address);
        formData.append("province", state.dataPersonal.province);
        formData.append("city", state.dataPersonal.district);
        formData.append("postal_code", state.dataPersonal.postalCode);
        formData.append("education", state.dataPersonal.education);
        formData.append("profession", state.dataPersonal.profession);
        formData.append("company_name", state.dataEmployment.companyName);
        formData.append("company_address", state.dataEmployment.companyAddress);
        formData.append("company_postal_code", "omnis");
        formData.append("company_job_title", state.dataEmployment.companyJobTitle);
        formData.append("company_contact_number", state.dataEmployment.companyPhoneNumber);
        formData.append("company_fax_number", state.dataEmployment.companyFaxNumber);
        formData.append("company_email", state.dataEmployment.companyEmail);
        // ====================file
        formData.append("ktp", state.dataCertification.idCard);
        formData.append("diploma", state.dataCertification.certification);
        formData.append("relevant_training_certificate", state.dataCertification.certificationTraining);
        formData.append("unit_operation_certificate", state.dataCertification.unitOperationCertificate);
        formData.append("versality_unit", state.dataCertification.versalityUnit);
        formData.append("photo", state.dataEmployment.photo);

        try {
            const result = await axios.post(APIURL + "/asesi-registration/draft",
                formData
                , {
                    headers: {
                        "Authorization": `Bearer ${token}`,

                        // "Content-Type": "application/json",
                    }
                });

            if (result.status === 200) {
                const data = result.data;
                const registrationAsesiId = result.data.data.id;
                localStorage.setItem("registration_asesi_id", registrationAsesiId);
                console.log("data success: " + JSON.stringify(data));
                alert("Data berhasil di simpan");
                setIsLoading(false);
                setNext(4);
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                setIsLoading(false);
            } else {
                alert("Error: " + JSON.stringify(error.response.data?.data?.error));
                setIsLoading(false);
            }
        }
    }

    const submitProcessSA = async () => {
        const token = localStorage.getItem("token");
        const registrationAsesiId = localStorage.getItem("registration_asesi_id");
        try {
            const result = await axios.put(APIURL + `/assesment/propose-new-asesment/${registrationAsesiId}`, {
                "track": 1
            },{
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json",
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                if (status === "success") {
                    alert("Submit Success");
                    setIsLoading(false);
                    setNext(1);
                    localStorage.removeItem("registration_asesi_id");
                    history.push('/asesi');
                }
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                setIsLoading(false);
            } else {
                alert("Error: " +JSON.stringify(error.response.data?.data?.error));
                setIsLoading(false);
            }
        }
    }

    return (
        <div style={styles.body}>
            {
                state.alreadyRegister.status ? <div></div> : <div style={styles.header}>
                    <h5>Rincian Data Pemohon Sertifikasi</h5>
                </div>
            }
            {
                next === 1 ? <AccessionPersonalDataMemo /> : next === 2 ? <EmploymentDataMemo /> : next === 3 ? <CertificationDataMemo /> : <SelfAssessmentMemo />
            }

            <ModalSpinner statusOpen={isLoading} toggle={isLoading} />

            <div style={{
                width: "100%",
                display: "flex",
                flexDirection: "row",
                marginTop: "10px"
            }}>
                <div style={{
                    width: "50%",
                    display: "flex",
                    justifyContent: "flex-start",

                }}>
                    {
                        next === 3 ?
                            <Button color="danger" onClick={() => setNext(2)}>
                                <AiOutlineArrowLeft /> Kembali
                            </Button> :
                            next === 2 ?
                                <Button color="danger" onClick={() => setNext(1)}>
                                    <AiOutlineArrowLeft /> Kembali
                                </Button>
                                : null

                    }

                </div>

                {state.alreadyRegister.status ? <div></div> :
                    <div style={{
                        width: "50%",
                        display: "flex",
                        justifyContent: "flex-end",
                    }}>
                        {
                            next === 1 ?
                                <Button color="primary" onClick={nextToPageTwo}>
                                    Selanjutnya <AiOutlineArrowRight />
                                </Button> :
                                next === 2 ?
                                    <Button color="primary" onClick={nextToPageThree}>
                                        Selanjutnya <AiOutlineArrowRight />
                                    </Button>
                                    : next === 3 ? <Button color="success" onClick={submit}>
                                        Simpan
                                    </Button> : <Button color="success" onClick={submitProcessSA}>
                                        Submit
                                    </Button>
                        }
                    </div>}
            </div>
        </div>
    );
}

const styles = {
    body: {
        width: "100%",
        height: "95%",
        // overflow: "scroll",
        // overflowX: "hidden",
        paddingRight: "2%",
        paddingLeft: "2%"
    },
    header: {
        width: "100%",
        backgroundColor: "#B3936A",
        color: "white"
    },
    formInput: {
        backgroundColor: "lightgrey",
        paddingRight: "1%",
        paddingLeft: "1%",
    }
}
