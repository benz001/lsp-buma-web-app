import React, { useContext, useState } from "react";
import { Form, FormGroup, Input, Label } from "reactstrap";
import Select from 'react-select';
import { changeDataEmployment } from '../../provider/actions';
import { Context } from "../../provider/provider";

 function EmploymentData() {
    const [companyName, setCompanyName] = useState("");
    const [companyAddress, setCompanyAddress] = useState("");
    const [companyJobTitle, setCompanyJobTitle] = useState("");
    const [companyPhoneNumber, setCompanyPhoneNumber] = useState("");
    const [companyFaxNumber, setCompanyFaxNumber] = useState("");
    const [companyEmail, setCompanyEmail] = useState("");
    // const [photo, setPhoto] = useState(localStorage.getItem('photo'));
    const [selectedFilePhoto, setSelectedFilePhoto] = useState(null);
    const [isFilePhoto, setIsFilePickedPhoto] = useState(false);
    const { state, dispatch } = useContext(Context);

    const saveCompanyName = (valueCompanyName) => {
        if (valueCompanyName !== "") {
            setCompanyName(valueCompanyName);
            dispatch(changeDataEmployment(
                valueCompanyName,
                companyAddress,
                companyJobTitle,
                companyPhoneNumber,
                companyFaxNumber,
                companyEmail,
                selectedFilePhoto
            ));
        } else {
            setCompanyName("");
            dispatch(changeDataEmployment(
                companyName,
                companyAddress,
                companyJobTitle,
                companyPhoneNumber,
                companyFaxNumber,
                companyEmail,
                selectedFilePhoto
            ));
        }
    }

    const saveCompanyAddress = (valueCompanyAddress) => {
        if (valueCompanyAddress !== "") {
            setCompanyAddress(valueCompanyAddress);
            dispatch(changeDataEmployment(
                companyName,
                valueCompanyAddress,
                companyJobTitle,
                companyPhoneNumber,
                companyFaxNumber,
                companyEmail,
                selectedFilePhoto
            ));
        } else {
            setCompanyAddress("");
            dispatch(changeDataEmployment(
                companyName,
                companyAddress,
                companyJobTitle,
                companyPhoneNumber,
                companyFaxNumber,
                companyEmail,
                selectedFilePhoto
            ));
        }
    }

    const saveCompanyJobTitle = (valueCompanyJobTitle) => {
        if (valueCompanyJobTitle !== "") {
            setCompanyJobTitle(valueCompanyJobTitle);
            dispatch(changeDataEmployment(
                companyName,
                companyAddress,
                valueCompanyJobTitle,
                companyPhoneNumber,
                companyFaxNumber,
                companyEmail,
                selectedFilePhoto
            ));

        } else {
            setCompanyJobTitle("");
            dispatch(changeDataEmployment(
                companyName,
                companyAddress,
                companyJobTitle,
                companyPhoneNumber,
                companyFaxNumber,
                companyEmail,
                selectedFilePhoto
            ));
        }
    }

    const saveCompanyPhoneNumber = (valueCompanyPhoneNumber) => {
        if (valueCompanyPhoneNumber !== "") {
            setCompanyPhoneNumber(valueCompanyPhoneNumber);
            dispatch(changeDataEmployment(
                companyName,
                companyAddress,
                companyJobTitle,
                valueCompanyPhoneNumber,
                companyFaxNumber,
                companyEmail,
                selectedFilePhoto
            ));
        } else {
            setCompanyPhoneNumber("");
            dispatch(changeDataEmployment(
                companyName,
                companyAddress,
                companyJobTitle,
                companyPhoneNumber,
                companyFaxNumber,
                companyEmail,
                selectedFilePhoto
            ));
        }
    }

    const saveCompanyFaxNumber = (valueCompanyFaxNumber) => {
        if (valueCompanyFaxNumber !== "") {
            setCompanyFaxNumber(valueCompanyFaxNumber);
            dispatch(changeDataEmployment(
                companyName,
                companyAddress,
                companyJobTitle,
                companyPhoneNumber,
                valueCompanyFaxNumber,
                companyEmail,
                selectedFilePhoto
            ));
        } else {
            setCompanyFaxNumber("");
            dispatch(changeDataEmployment(
                companyName,
                companyAddress,
                companyJobTitle,
                companyPhoneNumber,
                companyFaxNumber,
                companyEmail,
                selectedFilePhoto
            ));
        }
    }

    const saveCompanyEmail = (valueCompanyEmail) => {
        if (valueCompanyEmail !== "") {
            setCompanyEmail(valueCompanyEmail);
            dispatch(changeDataEmployment(
                companyName,
                companyAddress,
                companyJobTitle,
                companyPhoneNumber,
                companyFaxNumber,
                valueCompanyEmail,
                selectedFilePhoto
            ));
        } else {
            setCompanyEmail("");
            dispatch(changeDataEmployment(
                companyName,
                companyAddress,
                companyJobTitle,
                companyPhoneNumber,
                companyFaxNumber,
                companyEmail,
                selectedFilePhoto
            ));
        }
    }

    const savePhoto = (event) => {
        const e = event.target.files[0];

        try {
            const name = e.name;
            if (typeof name === "string") {
                setSelectedFilePhoto(event.target.files[0]);
                setIsFilePickedPhoto(true);
                dispatch(changeDataEmployment(
                    companyName,
                    companyAddress,
                    companyJobTitle,
                    companyPhoneNumber,
                    companyFaxNumber,
                    companyEmail,
                    event.target.files[0]
                ));
                setTimeout(() => {
                    console.log(event.target.files[0]);
                }, 1000);
            } else {
                setSelectedFilePhoto(null);
                dispatch(changeDataEmployment(
                    companyName,
                    companyAddress,
                    companyJobTitle,
                    companyPhoneNumber,
                    companyFaxNumber,
                    companyEmail,
                    selectedFilePhoto
                ));
                setTimeout(() => {
                    console.log(event.target.files[0]);
                }, 1000);
            }

        } catch (error) {
            setSelectedFilePhoto(selectedFilePhoto);
            setTimeout(() => {
                console.log(selectedFilePhoto);
            }, 1000);
            dispatch(changeDataEmployment(
                companyName,
                companyAddress,
                companyJobTitle,
                companyPhoneNumber,
                companyFaxNumber,
                companyEmail,
                selectedFilePhoto
            ));
        }

    };

    return (
        <div style={styles.formInput}>
            <div style={styles.title}>
                <h5>Data Pekerjaan Sekarang</h5>
            </div>
            <div style={styles.input}>
                <Form>
                    <FormGroup>
                        <Label for="company-name"><i>Nama Perusahaan/Lembaga Tempat Bekerja</i></Label>
                        <Input type="text" id="company-name" value={state.dataEmployment.companyName} onChange={(e) => saveCompanyName(e.target.value)} />
                    </FormGroup>

                    <FormGroup>
                        <Label for="company-address"><i>Alamat Tempat Pekerjaan</i></Label>
                        <Input type="text" id="company-address" value={state.dataEmployment.companyAddress} onChange={(e) => saveCompanyAddress(e.target.value)} />
                    </FormGroup>

                    <FormGroup>
                        <Label for="company-position"><i>Jabatan Pekerjaan</i></Label>
                        <Input type="text" id="company-position" value={state.dataEmployment.companyJobTitle} onChange={(e) => saveCompanyJobTitle(e.target.value)} />
                    </FormGroup>

                    <FormGroup>
                        <Label for="company-telephone"><i>No Telp Tempat Pekerjaan</i></Label>
                        <Input type="number" id="company-telephone" value={state.dataEmployment.companyPhoneNumber} onChange={(e) => saveCompanyPhoneNumber(e.target.value)} />
                    </FormGroup>

                    <FormGroup>
                        <Label for="company-fax"><i>No Fax Tempat Pekerjaan</i></Label>
                        <Input type="number" id="company-fax" value={state.dataEmployment.companyFaxNumber} onChange={(e) => saveCompanyFaxNumber(e.target.value)} />
                    </FormGroup>

                    <FormGroup>
                        <Label for="company-email"><i>Email Tempat Pekerjaan</i></Label>
                        <Input type="email" id="company-email" value={state.dataEmployment.companyEmail} onChange={(e) => saveCompanyEmail(e.target.value)} />
                    </FormGroup>

                    <FormGroup>
                        <Label for="employee-photo"><i>Photo</i></Label>
                        <Input type="file" id="employee-photo" onChange={savePhoto} accept="image/*" />
                    </FormGroup>
                </Form>
            </div>
        </div>
    );
}
const styles = {
    formInput: {
        backgroundColor: '#E9E9E9',
        paddingRight: '1%',
        paddingLeft: '1%',
        paddingBottom: '1%',
    }
}

const EmploymentDataMemo = React.memo(EmploymentData);

export default EmploymentDataMemo;