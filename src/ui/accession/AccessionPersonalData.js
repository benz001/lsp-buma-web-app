import React, { useContext, useEffect, useState } from "react";
import { Form, FormGroup, Input, Label } from "reactstrap";
import AsyncSelect from 'react-select/async';
import Select from 'react-select';
import axios from "axios";
import { APIURL } from '../../helper/LinkAPI';
import { Context } from "../../provider/provider";
import { CHANGE_DATA_PERSONAL, changeDataPersonal, changeStatus } from "../../provider/actions";
import SpinnerLoading from "../../components/SpinnerLoading";

function AccessionPersonalData(props) {
    const [listDistrict, setListDistrict] = useState([]);
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [nik, setNIK] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [gender, setGender] = useState("");
    const [pob, setPob] = useState("");
    const [dob, setDob] = useState("");
    const [address, setAddress] = useState("");
    const [province, setProvince] = useState("");
    const [district, setDistrict] = useState("");
    const [postalCode, setPostalCode] = useState("");
    const [education, setEducation] = useState("");
    const [profession, setProfession] = useState("");
    const [isLoading, setIsLoading] = useState(true);
    const [alreadyRegister, setAlreadyRegister] = useState(false);
    const { state, dispatch } = useContext(Context);

    useEffect(() => {
        getProfile();
    }, []);

    const getProfile = async () => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + "/user/me", {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                if (status === "success") {
                    console.log("data success: " + JSON.stringify(data));
                    // setDataUser(data);
                    setTimeout(() => {
                        setIsLoading(false)
                    }, 1000);
                    dispatch(changeDataPersonal(
                        data.user.name,
                        data.user.email,
                        data.user_profile.nik,
                        data.user_profile.phone_number,
                        data.user_profile.gender,
                        data.user_profile.place_of_birth,
                        data.user_profile.date_of_birth,
                        data.user_profile.address,
                        data.user_profile.province,
                        data.user_profile.city,
                        data.user_profile.postal_code,
                        data.user_profile.education,
                        data.user_profile.profession
                    ));
                    if (data.asesi != null) {
                        if (parseInt(data.asesi.track) > 0) {
                            setAlreadyRegister(true);
                               dispatch(changeStatus(true));
                        } else {
                            setAlreadyRegister(false);
                            dispatch(changeStatus(false));
                        }
                    } else {
                        setAlreadyRegister(false);
                        dispatch(changeStatus(false));
                    }
                }
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
            } else {
                alert("Error: " + JSON.stringify(error.response.data?.data?.error));
            }
        }
    }

    const getProvince = async () => {
        try {
            const result = await axios.get("https://ibnux.github.io/data-indonesia/provinsi.json");
            const data = result.data;
            const province = data;
            const tempArrayProvince = [];
            console.log("get Province");
            if (province.length) {
                province.forEach((element) => {
                    tempArrayProvince.push({
                        label: element.nama,
                        value: `${element.id}`
                    });
                });
            } else {
                tempArrayProvince.push({
                    label: province.nama,
                    value: `${province.id}`
                });
            }

            return tempArrayProvince;
        } catch (error) {
            // console.log("error: " + error);
            return [];
        }
    }

    const setterProvinceAndDistrict = (value, label) => {
        getDistrict(value);
        saveProvince(label);

    }

    const getDistrict = async (value) => {
        // console.log(selectDistrict !== "");
        try {
            const result = await axios.get(`https://ibnux.github.io/data-indonesia/kabupaten/${value}.json`);
            const data = result.data;
            const district = data;
            const tempArrayDistrict = [];

            console.log("getDistrict");

            if (district.length) {
                district.forEach((element) => {
                    tempArrayDistrict.push({
                        label: element.nama,
                        value: `${element.id}`
                    });
                });
            } else {
                tempArrayDistrict.push({
                    label: district.nama,
                    value: `${district.id}`
                });
            }

            setListDistrict(tempArrayDistrict);
        } catch (error) {
            // console.log("error: " + error);
            setListDistrict([]);
        }
    }

    const saveName = (valueName) => {
        if (valueName !== "") {
            setName(valueName);
            dispatch(changeDataPersonal(
                valueName,
                email,
                nik,
                phoneNumber,
                gender,
                pob,
                dob,
                address,
                province,
                district,
                postalCode,
                education,
                profession
            ));

        } else {
            setName("");
            dispatch(changeDataPersonal(
                name,
                email,
                nik,
                phoneNumber,
                gender,
                pob,
                dob,
                address,
                province,
                district,
                postalCode,
                education,
                profession
            ));
        }
    }

    const saveEmail = (valueEmail) => {
        if (valueEmail !== "") {
            setEmail(valueEmail);
            dispatch(changeDataPersonal(
                name,
                valueEmail,
                nik,
                phoneNumber,
                gender,
                pob,
                dob,
                address,
                province,
                district,
                postalCode,
                education,
                profession
            ));

        } else {
            setEmail("");
            dispatch(changeDataPersonal(
                name,
                email,
                nik,
                phoneNumber,
                gender,
                pob,
                dob,
                address,
                province,
                district,
                postalCode,
                education,
                profession
            ));
        }
    }

    const saveNIK = (valueNIK) => {
        if (valueNIK !== "") {
            setNIK(valueNIK);
            dispatch(changeDataPersonal(
                name,
                email,
                valueNIK,
                phoneNumber,
                gender,
                pob,
                dob,
                address,
                province,
                district,
                postalCode,
                education,
                profession
            ));
        } else {
            setNIK("");
            dispatch(changeDataPersonal(
                name,
                email,
                nik,
                phoneNumber,
                gender,
                pob,
                dob,
                address,
                province,
                district,
                postalCode,
                education,
                profession
            ));
        }
    }

    const savePhoneNumber = (valuePhoneNumber) => {
        if (valuePhoneNumber !== "") {
            setPhoneNumber(valuePhoneNumber);
            dispatch(changeDataPersonal(
                name,
                email,
                nik,
                valuePhoneNumber,
                gender,
                pob,
                dob,
                address,
                province,
                district,
                postalCode,
                education,
                profession
            ));
        } else {
            setPhoneNumber("");
            dispatch(changeDataPersonal(
                name,
                email,
                nik,
                phoneNumber,
                gender,
                pob,
                dob,
                address,
                province,
                district,
                postalCode,
                education,
                profession
            ));
        }
    }

    const saveGender = (valueGender) => {
        if (valueGender !== "") {
            setGender(valueGender);
            dispatch(changeDataPersonal(
                name,
                email,
                nik,
                phoneNumber,
                valueGender,
                pob,
                dob,
                address,
                province,
                district,
                postalCode,
                education,
                profession
            ));
        } else {
            setGender("");
            dispatch(changeDataPersonal(
                name,
                email,
                nik,
                phoneNumber,
                gender,
                pob,
                dob,
                address,
                province,
                district,
                postalCode,
                education,
                profession
            ));
        }
    }

    const savePob = (valuePob) => {
        if (valuePob !== "") {
            setPob(valuePob);
            dispatch(changeDataPersonal(
                name,
                email,
                nik,
                phoneNumber,
                gender,
                valuePob,
                dob,
                address,
                province,
                district,
                postalCode,
                education,
                profession
            ));
        } else {
            setPob("");
            dispatch(changeDataPersonal(
                name,
                email,
                nik,
                phoneNumber,
                gender,
                pob,
                dob,
                address,
                province,
                district,
                postalCode,
                education,
                profession
            ));
        }
    }

    const saveDob = (valueDob) => {
        if (valueDob !== "") {
            setDob(valueDob);
            dispatch(changeDataPersonal(
                name,
                email,
                nik,
                phoneNumber,
                gender,
                pob,
                valueDob,
                address,
                province,
                district,
                postalCode,
                education,
                profession
            ));
        } else {
            setDob("");
            dispatch(changeDataPersonal(
                name,
                email,
                nik,
                phoneNumber,
                gender,
                pob,
                dob,
                address,
                province,
                district,
                postalCode,
                education,
                profession
            ));
        }
    }

    const saveAddress = (valueAddress) => {
        if (valueAddress !== "") {
            setAddress(valueAddress);
            dispatch(changeDataPersonal(
                name,
                email,
                nik,
                phoneNumber,
                gender,
                pob,
                dob,
                valueAddress,
                province,
                district,
                postalCode,
                education,
                profession
            ));
        } else {
            setAddress("");
            dispatch(changeDataPersonal(
                name,
                email,
                nik,
                phoneNumber,
                gender,
                pob,
                dob,
                address,
                province,
                district,
                postalCode,
                education,
                profession
            ));
        }
    }

    const saveProvince = (valueProvince) => {
        if (valueProvince !== "") {
            setProvince(valueProvince);
            dispatch(changeDataPersonal(
                name,
                email,
                nik,
                phoneNumber,
                gender,
                pob,
                dob,
                address,
                valueProvince,
                district,
                postalCode,
                education,
                profession
            ));
        } else {
            setProvince("");
            dispatch(changeDataPersonal(
                name,
                email,
                nik,
                phoneNumber,
                gender,
                pob,
                dob,
                address,
                province,
                district,
                postalCode,
                education,
                profession
            ));
        }
    }

    const saveDistrict = (valueDistrict) => {
        if (valueDistrict !== "") {
            setDistrict(valueDistrict); dispatch(changeDataPersonal(
                name,
                email,
                nik,
                phoneNumber,
                gender,
                pob,
                dob,
                address,
                province,
                valueDistrict,
                postalCode,
                education,
                profession
            ));
        } else {
            setDistrict("");
            dispatch(changeDataPersonal(
                name,
                email,
                nik,
                phoneNumber,
                gender,
                pob,
                dob,
                address,
                province,
                district,
                postalCode,
                education,
                profession
            ));
        }
    }

    const savePostalCode = (valuePostalCode) => {
        if (valuePostalCode !== "") {
            setPostalCode(valuePostalCode);
            dispatch(changeDataPersonal(
                name,
                email,
                nik,
                phoneNumber,
                gender,
                pob,
                dob,
                address,
                province,
                district,
                valuePostalCode,
                education,
                profession
            ));
        } else {
            setPostalCode("");
            dispatch(changeDataPersonal(
                name,
                email,
                nik,
                phoneNumber,
                gender,
                pob,
                dob,
                address,
                province,
                district,
                postalCode,
                education,
                profession
            ));
        }
    }

    const saveEducation = (valueEducation) => {
        if (valueEducation !== "") {
            setEducation(valueEducation);
            dispatch(changeDataPersonal(
                name,
                email,
                nik,
                phoneNumber,
                gender,
                pob,
                dob,
                address,
                province,
                district,
                postalCode,
                valueEducation,
                profession
            ));
        } else {
            setEducation("");
            dispatch(changeDataPersonal(
                name,
                email,
                nik,
                phoneNumber,
                gender,
                pob,
                dob,
                address,
                province,
                district,
                postalCode,
                education,
                profession
            ));
        }
    }

    const saveProfession = (valueProfession) => {
        if (valueProfession !== "") {
            setProfession(valueProfession);
            dispatch(changeDataPersonal(
                name,
                email,
                nik,
                phoneNumber,
                gender,
                pob,
                dob,
                address,
                province,
                district,
                postalCode,
                education,
                valueProfession
            ));
        } else {
            setProfession("");
            dispatch(changeDataPersonal(
                name,
                email,
                nik,
                phoneNumber,
                gender,
                pob,
                dob,
                address,
                province,
                district,
                postalCode,
                education,
                profession
            ));
        }
    }


    if (isLoading) {
        return (
            <SpinnerLoading />
        );
    }

    return (
        alreadyRegister ?
            <div style={{ width: '100%', height: '100%', justifyContent: 'center', display: 'flex', fontWeight: 'bold' }}>
                Anda Sudah Melakukan Pandaftaran Asesi
            </div>
            :
            <div style={styles.formInputPersonal}>
                <div style={styles.title}>
                    <h5>Data Pribadi</h5>
                </div>
                <div style={styles.input}>
                    <div>
                        <Label for="edit"><i style={{ color: 'red' }}>*Untuk mengedit data pribadi, silahkan menuju halaman profile</i></Label>
                    </div>
                    <Form>
                        <FormGroup>
                            <Label for="name"><i>Nama</i></Label>
                            <Input type="text" id="name" value={state.dataPersonal.name} onChange={(e) => saveName(e.target.value)} disabled style={{ background: 'white' }} />
                        </FormGroup>

                        <FormGroup>
                            <Label for="email"><i>Email</i></Label>
                            <Input type="email" id="email" value={state.dataPersonal.email} onChange={(e) => saveEmail(e.target.value)} disabled style={{ background: 'white' }} />
                        </FormGroup>

                        <FormGroup>
                            <Label for="nik"><i>NIK (Nomor Induk Penduduk)</i></Label>
                            <Input type="number" id="nik" value={state.dataPersonal.nik} onChange={(e) => saveNIK(e.target.value)} disabled style={{ background: 'white' }} />
                        </FormGroup>

                        {/* <FormGroup>
                        <Label for="telephone"><i>No. Telp</i></Label>
                        <Input type="number" id="telephone" />
                    </FormGroup> */}

                        <FormGroup>
                            <Label for="handphone"><i>No. Handphone</i></Label>
                            {/* <Input type="number" id="handphone" /> */}
                            <Input
                                type="number"
                                name="handphone"
                                // pattern="[0-9]{3} [0-9]{3} [0-9]{4}"
                                // maxlength="12"
                                // title="Ten digits code"
                                // onKeyPress={false}
                                value={state.dataPersonal.phoneNumber}
                                onChange={(e) => savePhoneNumber(e.target.value)}
                                disabled
                                style={{ background: 'white' }}
                            />
                        </FormGroup>

                        <FormGroup for="gender">
                            <Label><i>Jenis Kelamin</i></Label>
                            <Input
                                type="select"
                                name="gender"
                                id="gender"
                                value={state.dataPersonal.gender}
                                onChange={(e) => saveGender(e.target.value)}
                                disabled
                                style={{ background: 'white' }}>
                                <option value="">Pilih Jenis Kelamin</option>
                                <option value="male">Pria</option>
                                <option value="female">Wanita</option>
                            </Input>
                        </FormGroup>

                        <FormGroup>
                            <Label for="pob"><i>Tempat Lahir</i></Label>
                            <Input type="text" id="pob" value={state.dataPersonal.pob} onChange={(e) => savePob(e.target.value)} disabled style={{ background: 'white' }} />
                        </FormGroup>

                        <FormGroup>
                            <Label for="dob"><i>Tanggal Lahir</i></Label>
                            <Input type="date" name="dob" id="dob" value={state.dataPersonal.dob} onChange={(e) => saveDob(e.target.value)} disabled style={{ background: 'white' }} />
                        </FormGroup>

                        <FormGroup>
                            <Label for="address"><i>Alamat</i></Label>
                            <Input type="text" name="address" id="address" value={state.dataPersonal.address} onChange={(e) => saveAddress(e.target.value)} disabled style={{ background: 'white' }} />
                        </FormGroup>

                        <FormGroup>
                            <Label for="province"><i>Pilih Provinsi</i></Label>
                            <Input type="text" name="province" id="address" value={state.dataPersonal.province} disabled style={{ background: 'white' }} />
                        </FormGroup>

                        <FormGroup>
                            <Label for="city"><i>Kota/Kabupaten</i></Label>
                            <Input type="text" name="city" id="city" value={state.dataPersonal.district} disabled style={{ background: 'white' }} />

                        </FormGroup>

                        <FormGroup>
                            <Label for="postal-code"><i>Kode Pos</i></Label>
                            <Input type="number" id="postal-code" value={state.dataPersonal.postalCode} onChange={(e) => savePostalCode(e.target.value)} disabled style={{ background: 'white' }} />
                        </FormGroup>

                        <FormGroup>
                            <Label for="education"><i>Pendidikan</i></Label>
                            <Input type="select" name="education" id="education" value={state.dataPersonal.education} onChange={state.dataPersonal.education} onChange={(e) => saveEducation(e.target.value)} disabled style={{ background: 'white' }}>
                                <option value="">Pilih Pendidikan</option>
                                <option value="elementary-school">SD/MI/Sederajat</option>
                                <option value="junior-high-school">SMP/Mts/Sederajat</option>
                                <option value="senior-high-school">SMA/SMK/Sederajat</option>
                                <option value="strata-1">S1</option>
                                <option value="strata-2">S2</option>
                                <option value="strata-3">S3</option>
                            </Input>
                        </FormGroup>

                        <FormGroup>
                            <Label for="work"><i>Pekerjaan</i></Label>
                            <Input type="select" name="work" id="work" value={state.dataPersonal.profession} onChange={(e) => saveProfession(e.target.value)} disabled style={{ background: 'white' }}>
                                <option value="">Pilih Pekerjaan</option>
                                <option value="private-employees">Pegawai Swasta</option>
                                <option value="goverment-employees">Pegawai Negeri</option>
                                <option value="entrepreneur">Wiraswasta</option>
                                <option value="student">Mahasiswa/Pelajar</option>
                            </Input>
                        </FormGroup>
                    </Form>
                </div>
            </div>
    );
}

const styles = {
    formInputPersonal: {
        // height: '100%',
        backgroundColor: '#E9E9E9',
        paddingRight: '1%',
        paddingLeft: '1%',
        paddingBottom: '1%'
    }
}

const AccessionPersonalDataMemo = React.memo(AccessionPersonalData);

export default AccessionPersonalDataMemo;