import React, { useContext, useEffect, useState } from 'react';
import { Button, Form, FormGroup, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader, Table } from 'reactstrap';
import upload from '../../assets/image/upload.png';
import checklist from '../../assets/image/checklist.png';
import stoped from '../../assets/image/stop.png';
import { useFilePicker } from 'use-file-picker';
import { AiOutlineCheck } from 'react-icons/ai';
import axios from 'axios';
import { APIURL } from '../../helper/LinkAPI';
import Select from 'react-select';
import { changeDataCertification, changeIdDataSchema, changeIdPlaceCompetence } from '../../provider/actions';
import { Context } from "../../provider/provider";

function CertificationData() {
    const [modal, setModal] = useState(false);

    const [modals, setModals] = useState(false);

    const [isOpen, setIsOpen] = useState(false);

    const [selectedFileIdCard, setSelectedFileIdCard] = useState(null);
    const [isFilePickedIdCard, setIsFilePickedIdCard] = useState(false);

    const [selectedFileCertification, setSelectedFileCertification] = useState(null);
    const [isFilePickedCertification, setIsFilePickedCertification] = useState(false);

    const [selectedFileTrainingCertification, setSelectedFileTrainingCertification] = useState(null);
    const [isFilePickedTrainingCertification, setIsFilePickedTrainingCertification] = useState(false);

    const [selectedFileUnitOperationCertificate, setSelectedFileUnitOperationCertificate] = useState(null);
    const [isFilePickedUnitOperationCertificate, setIsFilePickedUnitOperationCertificate] = useState(false);

    const [selectedFileVersalityUnit, setSelectedFileVersalityUnit] = useState(null);
    const [isFilePickedVersalityUnit, setIsFilePickedVersalityUnit] = useState(false);

    const [listSchematicCertificationOption, setListSchematicCertificationOption] = useState([]);
    const [listSchematicCertification, setListSchematicCertification] = useState([]);
    const [listSchematicUnit, setListSchematicUnit] = useState([]);

    const [listPlaceCompetence, setlistPlaceCompetence] = useState([]);

    const [listSelfAssesmen, setListSelfAssesment] = useState([]);
    const [status, setStatus] = useState(null);

    const [schematicCertification, setSchematicCertification] = useState("");
    const [placeCompetence, setPlaceCompetence] = useState("");

    const { state, dispatch } = useContext(Context);
    // const [switchOptionSchematicCertification, setSwitchOptionSchematicCertification] = useState()


    useEffect(() => {
        getListSchematicCertification();
        getListPlaceCompetence();
    }, []);

    const getListSchematicCertification = async () => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        // let tempSchematicUnit = [];
        try {
            const result = await axios.get(APIURL + "/schematic-certification", {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                console.log("success: " + JSON.stringify(result.data.data[0].data));
                const data = result.data.data[0].data;
                console.log(data.length);
                const tempArrayData = [];
                setListSchematicCertification(data);
                if (data.length) {
                    data.forEach((element) => {
                        tempArrayData.push({
                            label: element.name,
                            value: `${element.id}`
                        });
                    });
                } else {
                    tempArrayData.push({
                        label: "",
                        value: ""
                    });
                }
                setListSchematicCertificationOption(tempArrayData);
            } else {
                console.log("bad: " + JSON.stringify(result.data));
            }
        } catch (error) {
            console.log("error " + error);
            // alert("Login Error");
        }
    }

    const getListPlaceCompetence = async () => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        // let tempSchematicUnit = [];
        try {
            const result = await axios.get(APIURL + "/place-competence", {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                console.log("success place competence: " + JSON.stringify(result.data.data));
                const data = result.data.data;
                console.log(data.length);
                const tempArrayData = [];
                setlistPlaceCompetence(data);
                if (data.length) {
                    data.forEach((element) => {
                        tempArrayData.push({
                            label: element.name,
                            value: `${element.id}`
                        });
                    });
                } else {
                    tempArrayData.push({
                        label: "",
                        value: ""
                    });
                }
                setlistPlaceCompetence(tempArrayData);
            } else {
                console.log("bad: " + JSON.stringify(result.data));
            }
        } catch (error) {
            console.log("error " + error);
            // alert("Login Error");
        }
    }

    const saveParamSchematicUnit = (id, label) => {
        var idDataSchema = parseInt(id);
        console.log(label);
        console.log("idDataSchema:" + idDataSchema);
        dispatch(changeIdDataSchema(idDataSchema));
        saveSchematicCertification(label);
    }

    const saveParamPlaceCompetence = (id, label) => {
        var idPlacaCompetence = parseInt(id);
        console.log(label);
        console.log("idPlaceCompetence:" + idPlacaCompetence);
        dispatch(changeIdPlaceCompetence(idPlacaCompetence));
        savePlaceCompetency(label);
    }

    const saveSchematicCertification = (valueSchematicCertification) => {
        if (valueSchematicCertification !== "") {
            setSchematicCertification(valueSchematicCertification);
            dispatch(changeDataCertification(
                valueSchematicCertification,
                placeCompetence,
                selectedFileIdCard,
                selectedFileCertification,
                selectedFileTrainingCertification,
                selectedFileUnitOperationCertificate,
                selectedFileVersalityUnit
            ));
        } else {
            setSchematicCertification("");
            dispatch(changeDataCertification(
                schematicCertification,
                placeCompetence,
                selectedFileIdCard,
                selectedFileCertification,
                selectedFileTrainingCertification,
                selectedFileUnitOperationCertificate,
                selectedFileVersalityUnit
            ));
        }
    }

    const savePlaceCompetency = (valuePlaceCompetency) => {
        if (valuePlaceCompetency !== "") {
            setPlaceCompetence(valuePlaceCompetency);
            dispatch(changeDataCertification(
                schematicCertification,
                valuePlaceCompetency,
                selectedFileIdCard,
                selectedFileCertification,
                selectedFileTrainingCertification,
                selectedFileUnitOperationCertificate,
                selectedFileVersalityUnit
            ));
        } else {
            setPlaceCompetence("");
            dispatch(changeDataCertification(
                schematicCertification,
                placeCompetence,
                selectedFileIdCard,
                selectedFileCertification,
                selectedFileTrainingCertification,
                selectedFileUnitOperationCertificate,
                selectedFileVersalityUnit
            ));
        }
    }

    const saveIdCard = (event) => {
        const e = event.target.files[0];
        try {
            const name = e.name;
            if (typeof name === "string") {
                setIsFilePickedIdCard(true);
                setSelectedFileIdCard(event.target.files[0]);
                setTimeout(() => {
                    console.log(event.target.files[0]);
                }, 1000);
                dispatch(changeDataCertification(
                    schematicCertification,
                    placeCompetence,
                    event.target.files[0],
                    selectedFileCertification,
                    selectedFileTrainingCertification,
                    selectedFileUnitOperationCertificate,
                    selectedFileVersalityUnit
                ));
            } else {
                setSelectedFileIdCard(null);
                dispatch(changeDataCertification(
                    schematicCertification,
                    placeCompetence,
                    selectedFileIdCard,
                    selectedFileCertification,
                    selectedFileTrainingCertification,
                    selectedFileUnitOperationCertificate,
                    selectedFileVersalityUnit
                ));
            }
        } catch (error) {
            setSelectedFileIdCard(selectedFileIdCard);
            setTimeout(() => {
                console.log(selectedFileIdCard);
            }, 1000);
            dispatch(changeDataCertification(
                schematicCertification,
                placeCompetence,
                selectedFileIdCard,
                selectedFileCertification,
                selectedFileTrainingCertification,
                selectedFileUnitOperationCertificate,
                selectedFileVersalityUnit
            ));
        }

    };

    const saveCertification = (event) => {
        const e = event.target.files[0];
        try {
            const name = e.name;
            if (typeof name === "string") {
                setIsFilePickedCertification(true);
                setSelectedFileCertification(event.target.files[0]);
                setTimeout(() => {
                    console.log(event.target.files[0]);
                }, 3000);
                dispatch(changeDataCertification(
                    schematicCertification,
                    placeCompetence,
                    selectedFileIdCard,
                    event.target.files[0],
                    selectedFileTrainingCertification,
                    selectedFileUnitOperationCertificate,
                    selectedFileVersalityUnit
                ));
            } else {
                setSelectedFileCertification(null);
                setTimeout(() => {
                    console.log(selectedFileCertification);
                }, 1000);
                dispatch(changeDataCertification(
                    schematicCertification,
                    placeCompetence,
                    selectedFileIdCard,
                    selectedFileCertification,
                    selectedFileTrainingCertification,
                    selectedFileUnitOperationCertificate,
                    selectedFileVersalityUnit
                ));
            }
        } catch (error) {
            setSelectedFileCertification(selectedFileCertification);
            dispatch(changeDataCertification(
                schematicCertification,
                placeCompetence,
                selectedFileIdCard,
                selectedFileCertification,
                selectedFileTrainingCertification,
                selectedFileUnitOperationCertificate,
                selectedFileVersalityUnit
            ));
        }
    };

    const saveTrainingCertification = (event) => {
        const e = event.target.files[0];
        try {
            const name = e.name;
            if (typeof name === "string") {
                setIsFilePickedTrainingCertification(true);
                setSelectedFileTrainingCertification(event.target.files[0]);
                setTimeout(() => {
                    console.log(event.target.files[0]);
                }, 1000);

                dispatch(changeDataCertification(
                    schematicCertification,
                    placeCompetence,
                    selectedFileIdCard,
                    selectedFileCertification,
                    event.target.files[0],
                    selectedFileUnitOperationCertificate,
                    selectedFileVersalityUnit
                ));
            } else {
                setSelectedFileTrainingCertification(null);
                dispatch(changeDataCertification(
                    schematicCertification,
                    placeCompetence,
                    selectedFileIdCard,
                    selectedFileCertification,
                    selectedFileTrainingCertification,
                    selectedFileUnitOperationCertificate,
                    selectedFileVersalityUnit
                ));
            }
        } catch (error) {
            setSelectedFileTrainingCertification(selectedFileTrainingCertification);
            setTimeout(() => {
                console.log(selectedFileTrainingCertification);
            }, 1000);
            dispatch(changeDataCertification(
                schematicCertification,
                placeCompetence,
                selectedFileIdCard,
                selectedFileCertification,
                selectedFileTrainingCertification,
                selectedFileUnitOperationCertificate,
                selectedFileVersalityUnit
            ));
        }
    };

    const saveUnitOperationCertificate = (event) => {
        const e = event.target.files[0];
        try {
            const name = e.name;
            if (typeof name === "string") {
                setIsFilePickedUnitOperationCertificate(true);
                setSelectedFileUnitOperationCertificate(event.target.files[0]);
                setTimeout(() => {
                    console.log(event.target.files[0]);
                }, 3000);
                dispatch(changeDataCertification(
                    schematicCertification,
                    placeCompetence,
                    selectedFileIdCard,
                    selectedFileCertification,
                    selectedFileUnitOperationCertificate,
                    event.target.files[0],
                    selectedFileVersalityUnit
                ));
            } else {
                setSelectedFileUnitOperationCertificate(null);
                setTimeout(() => {
                    console.log(selectedFileUnitOperationCertificate);
                }, 1000);
                dispatch(changeDataCertification(
                    schematicCertification,
                    placeCompetence,
                    selectedFileIdCard,
                    selectedFileCertification,
                    selectedFileTrainingCertification,
                    selectedFileUnitOperationCertificate,
                    selectedFileVersalityUnit
                ));
            }
        } catch (error) {
            setSelectedFileUnitOperationCertificate(selectedFileUnitOperationCertificate);
            setTimeout(() => {
                console.log(selectedFileUnitOperationCertificate);
            }, 1000);
            dispatch(changeDataCertification(
                schematicCertification,
                placeCompetence,
                selectedFileIdCard,
                selectedFileCertification,
                selectedFileTrainingCertification,
                selectedFileUnitOperationCertificate,
                selectedFileVersalityUnit
            ));
        }

    };

    const saveVersalityUnit = (event) => {
        const e = event.target.files[0];
        try {
            const name = e.name;
            if (typeof name === "string") {
                setIsFilePickedVersalityUnit(true);
                setSelectedFileVersalityUnit(event.target.files[0]);
                setTimeout(() => {
                    console.log(event.target.files[0]);
                }, 3000);
                dispatch(changeDataCertification(
                    schematicCertification,
                    placeCompetence,
                    selectedFileIdCard,
                    selectedFileCertification,
                    selectedFileTrainingCertification,
                    selectedFileUnitOperationCertificate,
                    event.target.files[0]
                ));
            } else {
                setSelectedFileVersalityUnit(null);
                dispatch(changeDataCertification(
                    schematicCertification,
                    placeCompetence,
                    selectedFileIdCard,
                    selectedFileCertification,
                    selectedFileTrainingCertification,
                    selectedFileUnitOperationCertificate,
                    selectedFileVersalityUnit
                ));
            }
        } catch (error) {
            setSelectedFileVersalityUnit(selectedFileVersalityUnit);
            setTimeout(() => {
                console.log(selectedFileVersalityUnit);
            }, 1000);
            dispatch(changeDataCertification(
                schematicCertification,
                placeCompetence,
                selectedFileIdCard,
                selectedFileCertification,
                selectedFileTrainingCertification,
                selectedFileUnitOperationCertificate,
                selectedFileVersalityUnit
            ));
        }

    };

    const toggleCollapse = () => { setIsOpen(!isOpen) };

    const toggle = () => {
        console.log('open');
        setModal(!modal)
    };

    const toggleTwo = () => {
        console.log(listSelfAssesmen);
        console.log('open');
        setModals(!modals);
        setStatus(null);
    };
    const schemeData = () => {
        const styles = {
            header: {
                width: '100%',
                backgroundColor: '#B3936A',
                color: 'white'
            },
            formInput: {
                backgroundColor: '#F4F3EF',
                paddingRight: '1%',
                paddingLeft: '1%',
                paddingBottom: '0.1%'

            },
            input: {
                backgroundColor: 'transparent',
            }
        }
        return (
            <div style={styles.formInput}>
                <div style={styles.title}>
                    <h5>Data Skema dan TUK</h5>
                </div>
                <div style={styles.input}>
                    <Form>
                        <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                            <Label
                                style={{ width: '30%' }}
                                for="certification-scheme"
                            >Skema Sertifikasi</Label>
                            <div style={{ width: '70%' }}>
                                <Select
                                    options={listSchematicCertificationOption}
                                    onChange={(e) => saveParamSchematicUnit(e.value, e.label)}
                                    placeholder="Pilih Skema Sertifikasi"
                                />
                            </div>
                        </FormGroup>

                        <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                            <Label
                                style={{ width: '30%' }}
                                for="place-test"
                            >Tempat Uji Kompetensi</Label>
                            <div style={{ width: '70%' }}>
                                <Select
                                    options={listPlaceCompetence}
                                    onChange={(e) => saveParamPlaceCompetence(e.value, e.label)}
                                    placeholder="Pilih Tempat Uji Kompetensi"
                                />
                            </div>
                        </FormGroup>

                        {/* <FormGroup>
                            <Label
                                for="description-certification">Deskripsi Sertifikasi</Label>
                            <Input
                                type="text"
                                id="description-certification" />
                        </FormGroup> */}
                    </Form>
                </div>
            </div>
        );
    }

    const registrationData = () => {
        const styles = {
            formInput: {
                backgroundColor: ' rgb(243, 243, 239)',
                paddingRight: '1%',
                paddingLeft: '1%',
                paddingBottom: '1%',
                marginTop: '10px'
            }
        }
        return (
            <div style={styles.formInput}>
                <div style={styles.title}>
                    <h5>Data Registrasi</h5>
                </div>
                <div style={styles.input}>
                    <Form>
                        <FormGroup for="certification-type" style={{ display: 'flex', flexDirection: 'row' }}>
                            <Label style={{ width: '30%' }}>Jenis Sertifikasi</Label>
                            <Input type="select" name="certification-type" id="certification-type" style={{ width: '70%' }}>
                                <option>Pilih Jenis Sertifikasi</option>
                                <option>Sertifikasi Baru</option>
                                <option>Sertifikasi Ulang</option>
                            </Input>
                        </FormGroup>
                    </Form>
                </div>
            </div>
        );
    }



    const attachment = () => {
        listSchematicUnit.map((item) => (
            console.log(item.code)
        ))
        return (
            <div style={styles.body}>
                <div style={styles.title}>
                    <h5>Lampiran-lampiran</h5>
                    <h6>Bukti kelengkapan persyaratan dasar pemohon</h6>
                </div>
                <div style={styles.table}>
                    <Table striped>
                        <thead style={{ background: 'black', color: 'white' }}>
                            <tr>
                                <th>NO.</th>
                                <th>NAMA LAMPIRAN</th>
                                <th>JUDUL</th>
                                <th>DOK</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row">1</td>
                                <td>Copy KTP</td>
                                {isFilePickedIdCard ? (
                                    <td>
                                        {typeof selectedFileIdCard == undefined ? "undefined" : "KTP"}
                                    </td>) : (<td></td>)}
                                <td>
                                    <div>
                                        <label for="file-input-id-card">
                                            {isFilePickedIdCard ?
                                                <AiOutlineCheck size={30} color="#B0131E" /> :
                                                <img src={upload} />}
                                        </label>
                                        <Input id="file-input-id-card" type="file" onChange={saveIdCard} accept="image/*" style={{ display: 'none' }} />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">2</td>
                                <td>Copy Ijazah Terakhir</td>
                                {isFilePickedCertification ? (
                                    <td>
                                        {typeof selectedFileCertification == undefined ? "undefined" : "Ijazah Terakhir"}
                                    </td>) : (<td></td>)}
                                <td> <div>
                                    <label for="file-input-certification">
                                        {isFilePickedCertification ?
                                            <AiOutlineCheck size={30} color="#B0131E" /> :
                                            <img src={upload} />}
                                    </label>
                                    <Input id="file-input-certification" type="file" onChange={saveCertification} accept="image/*" style={{ display: 'none' }} />
                                </div></td>
                            </tr>
                            <tr>
                                <td scope="row">3</td>
                                <td>Copy Sertifikasi Pelatihan Relevan</td>
                                {isFilePickedTrainingCertification ? (
                                    <td>
                                        {typeof selectedFileTrainingCertification == undefined ? "undefined" : "Sertifikasi Pelatihan Relevan"}
                                    </td>) : (<td></td>)}
                                <td> <div>
                                    <label for="file-input-training-certification">
                                        {isFilePickedTrainingCertification ?
                                            <AiOutlineCheck size={30} color="#B0131E" /> :
                                            <img src={upload} />}
                                    </label>
                                    <Input id="file-input-training-certification" type="file" onChange={saveTrainingCertification} accept="image/*" style={{ display: 'none' }} />
                                </div></td>
                            </tr>
                            <tr>
                                <td scope="row">4</td>
                                <td>Sertifikat Sertifikasi Pengoprasian Unit</td>
                                {isFilePickedUnitOperationCertificate ? (
                                    <td>
                                        {typeof selectedFileUnitOperationCertificate == undefined ? "undefined" : "Sertifikat Sertifikasi Pengoprasian Unit"}
                                    </td>) : (<td></td>)}
                                <td> <div>
                                    <label for="file-input-unit-operation-certificate">
                                        {isFilePickedUnitOperationCertificate ?
                                            <AiOutlineCheck size={30} color="#B0131E" /> :
                                            <img src={upload} />}
                                    </label>
                                    <Input id="file-input-unit-operation-certificate" type="file" onChange={saveUnitOperationCertificate} accept="image/*" style={{ display: 'none' }} />
                                </div></td>
                            </tr>
                            <tr>
                                <td scope="row">5</td>
                                <td>SIMPER sesuai Versality Unit</td>
                                {isFilePickedVersalityUnit ? (
                                    <td>
                                        {typeof selectedFileVersalityUnit == undefined ? "undefined" : "SIMPER sesuai Versality Unit"}
                                    </td>) : (<td></td>)}
                                <td> <div>
                                    <label for="file-input-versality-unit">
                                        {isFilePickedVersalityUnit ?
                                            <AiOutlineCheck size={30} color="#B0131E" /> :
                                            <img src={upload} />}
                                    </label>
                                    <Input id="file-input-versality-unit" type="file" onChange={saveVersalityUnit} accept="image/*" style={{ display: 'none' }} />
                                </div></td>
                            </tr>
                        </tbody>
                    </Table>
                </div>
                {modalSelfAssesment()}
            </div>
        );
    }


    const modalSelfAssesment = () => {
        return (
            <Modal isOpen={modals} toggle={toggleTwo} size="lg" style={{ maxWidth: '700px', width: '100%' }} backdrop="static">
                <ModalHeader toggle={toggleTwo}>Daftar Pertanyaan/Self Assesment</ModalHeader>
                <ModalBody >
                    <Table striped>
                        <thead style={{ background: 'black', color: 'white', width: '70%' }}>
                            <tr>
                                <th>NO</th>
                                <th>Pertanyaan</th>
                                <th width="20%">K</th>
                                <th width="20%">BK</th>
                                {/* <th>DOK BUKTI</th> */}
                            </tr>
                        </thead>
                        {listSelfAssesmen.length != 0 ?
                            <tbody>
                                {
                                    listSelfAssesmen.map((item, index) => (
                                        <tr>
                                            <td scope="row">{index + 1}</td>
                                            <td>{item.question}</td>
                                            <td width="20%">
                                                <Input type="checkbox" style={{ marginLeft: '5px' }} />
                                            </td>
                                            <td width="20%">
                                                <Input type="checkbox" style={{ marginLeft: '5px' }} />
                                            </td>
                                            {/* <td><img src={upload} onClick={toggle} height="30px" style={{ marginLeft: '15px' }} /></td> */}
                                        </tr>
                                    ))
                                }
                            </tbody>
                            :
                            <tbody>
                                <tr>

                                </tr>
                            </tbody>
                        }

                    </Table>
                </ModalBody>
                <ModalFooter>
                    <Button color="danger" onClick={toggleTwo}>Tutup</Button>{' '}
                    <Button color="warning" style={{ color: 'white' }} onClick={toggleTwo}>Belum Kompeten Semua</Button>{' '}
                    <Button color="info" onClick={toggleTwo}>Kompeten Semua</Button>
                </ModalFooter>
            </Modal>
        );
    }


    return (
        <div style={styles.body}>
            {schemeData()}
            {registrationData()}
            {attachment()}
        </div>
    );
}

const CertificationDataMemo = React.memo(CertificationData);

export default CertificationDataMemo;


const styles = {
    body: {
        width: '100%',
        // paddingRight: '2%',
        // paddingLeft: '2%'
    },
    header: {
        width: '100%',
        backgroundColor: 'brown',
        color: 'white'
    },
    formInput: {
        backgroundColor: 'lightgrey',
        paddingRight: '1%',
        paddingLeft: '1%',
    }
}