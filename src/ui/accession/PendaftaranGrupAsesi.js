import React, { useState } from 'react';
import { Form, FormGroup, Input, Label, Modal, ModalHeader, ModalBody, ModalFooter, Button, Spinner } from 'reactstrap';
import SpinnerLoading from '../../components/SpinnerLoading';

export default function SchemeDataGroup() {
    const [modal, setModal] = useState(false);

    const [modals, setModals] = useState(false);

    const [isOpen, setIsOpen] = useState(false);

    const toggleCollapse = () => { setIsOpen(!isOpen) };

    const [isLoading, setIsLoading] = useState(false);

    const [soon] = useState(true);

    const toggle = () => {
        console.log('open');
        setModal(!modal)
    };

    if (isLoading) {
        return (
            <SpinnerLoading/>
        );
    }

    if (soon) {
        return(
            <div style={{width: '100%', height: '100', display: 'flex', justifyContent:'center', alignItems: 'center', fontWeight: 'bold'}}>
                Dalam Proses Pengembangan
            </div>
        );
    }
    return (
        <div style={styles.formInput}>
            <div style={styles.title}>
                <h5>Data Pendaftaran Sertifikasi</h5>
            </div>
            <div style={{ background: 'rgb(243, 243, 239)', marginTop: '12px', paddingRight: '2%', paddingLeft: '2%', paddingBottom: '2%' }}>
                <div>
                    <h5>Data Skema TUK</h5>
                </div>
                <div style={styles.input}>
                    <Form>
                        <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                            <Label
                                style={{ width: '30%' }}
                                for="certification-scheme"
                            >Skema Sertifikasi</Label>
                            <Input type="select" name="certification-place" id="certification-place" style={{ width: '70%' }}>
                                <option>Pilih Skema Sertifikasi</option>
                                <option>Skema A</option>
                                <option>Skema B</option>
                            </Input>
                        </FormGroup>
                        <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                            <Label
                                style={{ width: '30%' }}
                                for="place-test"
                            >Tempat Uji Kompetensi</Label>
                            <Input type="select" name="certification-place" id="certification-place" style={{ width: '70%' }}>
                                <option>Pilih Tempat Uji Kompetensi</option>
                                <option>Tempat A</option>
                                <option>Tempat B</option>
                            </Input>
                        </FormGroup>
                    </Form>
                </div>
            </div>

            <div style={{ background: 'rgb(243, 243, 239)', marginTop: '12px', paddingRight: '2%', paddingLeft: '2%', paddingBottom: '2%' }}>
                <div>
                    <h5>Upload Data Asesi</h5>
                </div>
                <div style={styles.input}>
                    <Form>
                        <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                            <Label
                                style={{ width: '30%' }}
                                for="certification-scheme"
                            >Data Asesi</Label>
                            <Button
                                style={{ width: '70%' }}
                                onClick={toggle}
                            >
                                Upload File (Format Excel)
                            </Button>
                        </FormGroup>
                        <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                            <Label
                                style={{ width: '30%' }}
                                for="place-test"
                            >Tanggal Sertifikasi</Label>
                            <Input
                                style={{ width: '70%' }}
                                type="date"
                                id="certification-scheme"
                                value="Fakultas ABC" />
                        </FormGroup>
                    </Form>
                </div>
            </div>

            <div style={{ display: 'flex', marginTop: '12px', paddingRight: '2%', paddingLeft: '2%', paddingBottom: '2%', justifyContent:'flex-end', alignItems:'flex-end' }}>
                <Button onClick={()=>alert('Data Sukses di Simpan')} color='success'>Submit</Button>
            </div>
            <Modal isOpen={modal} toggle={toggle} backdrop="static">
                <ModalHeader toggle={toggle}>Upload Lampiran Persyaratan</ModalHeader>
                <ModalBody>
                    <Form>
                        <FormGroup style={{ width: '100%', height: '100%', display: 'flex', flexDirection: 'row' }}>
                            <Label for="name"><i>Judul Dokumen</i></Label>
                            <div style={{ width: '10%' }} />
                            <Input type="text" name="name" id="name" />
                        </FormGroup>

                        <FormGroup style={{ width: '100%', height: '100%', display: 'flex', flexDirection: 'row' }}>
                            <Label for="name"><i>Keterangan</i></Label>
                            <div style={{ width: '10%' }} />
                            <Input type="text" name="name" id="name" />
                        </FormGroup>

                        <FormGroup style={{ width: '100%', height: '100%', display: 'flex', flexDirection: 'row' }}>
                            <Label for="name"><i>Upload Dokumen</i></Label>
                            <div style={{ width: '10%' }} />
                            <Input type="file" name="name" id="name" />
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="danger" onClick={toggle}>Tutup</Button>{' '}
                    <Button color="primary" onClick={toggle}>Simpan</Button>
                </ModalFooter>
            </Modal>

        </div>
    );
}

const styles = {
    title: {
        backgroundColor: '#B3936A',
        color: 'white'
    },
    formInput: {
        // backgroundColor: 'lightgrey',
        width: '100%',
        height: '100%',
        paddingRight: '1%',
        paddingLeft: '1%',
    }
}