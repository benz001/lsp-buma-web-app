import React, { useContext, useEffect, useState } from 'react';
import { Button, Input, Modal, ModalBody, ModalFooter, ModalHeader, Table, Spinner } from 'reactstrap';
import axios from 'axios';
import { APIURL } from '../../helper/LinkAPI';
import { Context } from "../../provider/provider";
import upload from '../../assets/image/ic_dokumen.png';
import send from '../../assets/image/paper_plane.png';
import { AiFillCaretRight, AiOutlineUpload } from 'react-icons/ai';



function SelfAssessment() {
    const [modals, setModals] = useState(false);
    const [listSchematicUnit, setListSchematicUnit] = useState([]);
    const [listSelfAssesmen, setListSelfAssesment] = useState([]);
    const [listAnswer, setListAnswer] = useState([]);
    const [listDoc, setListDoc] = useState([]);
    const [modalLoading, setModalLoading] = useState(false);
    const [listIdAnswer, setListIdAnswer] = useState([]);
    const [selectedFile, setSelectedFile] = useState(null);
    const [isFilePicked, setIsFilePicked] = useState(false);
    const [bulkAnswer, setBulkAnswer] = useState([]);
    const [isBulk, setIsBulk] = useState(false);

    useEffect(() => {
        getListSchematicUnit();
    }, []);

    const getListSchematicUnit = async () => {
        const token = localStorage.getItem("token");
        const registrationAsesiId = localStorage.getItem("registration_asesi_id");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + "/schematic-certification/asesi-registration/" + registrationAsesiId, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                console.log("success: " + JSON.stringify(result.data.data[0].schematic_unit));
                const data = result.data.data[0].schematic_unit;
                setListSchematicUnit(data);
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
            } else {
                alert("Error: " + JSON.stringify(error.response.data?.data?.error));
            }
        }
    }



    const getListSelfAsesmentByUnitId = (id) => {
        var idSchematicUnit = parseInt(id);
        console.log("id schematic unit: " + idSchematicUnit);
        var newData = listSchematicUnit.filter(function (el) {
            return el.id == id; // Changed this so a home would match
        });
        setListSelfAssesment(newData[0].self_assesment);
        console.log("res: " + JSON.stringify(newData[0].self_assesment));
        setTimeout(() => {
            getStatus(newData[0].self_assesment);
        }, 1000);
        setTimeout(() => {
            setModals(true);
        }, 2000);
    }

    const getStatus = (data) => {
        console.log("get status: " + data.length);
        setListAnswer([]);
        setListIdAnswer([]);
        setListDoc([]);
        setIsBulk(false);
        setBulkAnswer([]);
        for (let index = 0; index < data.length; index++) {
            setListAnswer(oldArray => [...oldArray, data[index].answer.answer]);
            setListIdAnswer(oldArray => [...oldArray, data[index].answer.id]);
            setListDoc(oldArray => [...oldArray, data[index].answer.proof]);
            console.log("count: " + index + " answer id: " + JSON.stringify(data[index].answer.id));
            console.log("count: " + index + " answers: " + JSON.stringify(data[index].answer.answer));
            console.log("counts: " + index + " proof: " + JSON.stringify(data[index].answer.proof));
        }
    }

    const updateAnswerSelfAssessment = async (id, idAnswer, answer, data) => {
        console.log("answer: " + answer);
        if (answer == "true") {
            changeStatusAnswer(id, idAnswer, true, data)
        } else {
            changeStatusAnswer(id, idAnswer, false, data);
        }

    }

    const updateAnswerSelfAssessmentProccess = async (id, answer, proof) => {
        console.log(id);
        console.log(typeof answer);
        console.log(proof);
        const yourAnswer = answer.toString();
        console.log("your answer: "+yourAnswer);
        const token = localStorage.getItem("token");
        const formData = new FormData();
        let header;
        if (proof != null) {
            header = {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "multipart/form-data",
                // "Accept": "application/json",
            };
            formData.append("answer", yourAnswer);
            formData.append("proof", proof);
            console.log("with file");
        } else {
            header = {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json",
                "Accept": "application/json",
            };
            formData.append("answer", yourAnswer);
            console.log("with out file");
        }
        try {
            const result = await axios.post(APIURL + `/asesi-registration/self-assesment/answer/${id}/update?_method=PUT`,
                formData
                , {
                    headers: header
                });

            if (result.status === 200) {
                const data = result.data;
                console.log(JSON.stringify(data));
                alert("Data berhasil di update");
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
            } else {
                alert("Error: " + JSON.stringify(error.response.data?.data?.error));
            }
        }
    }

    const refreshListSchematicUnit = async () => {
        const token = localStorage.getItem("token");
        const registrationAsesiId = localStorage.getItem("registration_asesi_id");
        setModalLoading(true);
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + "/schematic-certification/asesi-registration/" + registrationAsesiId, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                console.log("success refresh: " + JSON.stringify(result.data.data[0].schematic_unit));
                const data = result.data.data[0].schematic_unit;
                setListSchematicUnit(data);
                alert("Data berhasil di simpan");
                setModalLoading(false);
                setModals(!modals);
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                setModalLoading(false);
            } else {
                alert("Error: " + JSON.stringify(error.response.data?.data?.error));
                setModalLoading(false);
            }
        }
    }

    const toggleTwo = () => {
        console.log('open');
        refreshListSchematicUnit();
    };


    const changeStatusAnswer = (id, idAnswer, answer, data) => {
        console.log(id);
        console.log(idAnswer);
        console.log("list answer: " + listAnswer.length);
        data.answer.answer = answer;
        listAnswer.splice(id, 1, answer);
        setListAnswer([...listAnswer]);

    }

    const changeStatusAnswerAllTrue = () => {
        for (let i = 0; i < listAnswer.length; i++) {
            listAnswer.splice(i, 1, true);
            setListAnswer([...listAnswer]);
        }
        setTimeout(() => {
            setBulkAnswer([]);
            for (let j = 0; j < listAnswer.length; j++) {
                console.log("list id answer: " + listIdAnswer[j] + " list answer: " + listAnswer[j]);
                setBulkAnswer(oldArray => [...oldArray, { "id": listIdAnswer[j], "answer": listAnswer[j] }]);
            }
            setIsBulk(true);
        }, 1000);
    }

    const changeStatusAnswerAllFalse = () => {
        for (let i = 0; i < listAnswer.length; i++) {
            listAnswer.splice(i, 1, false);
            setListAnswer([...listAnswer]);
        }
        setTimeout(() => {
            setBulkAnswer([]);
            for (let j = 0; j < listAnswer.length; j++) {
                console.log("list id answer: " + listIdAnswer[j] + " list answer: " + listAnswer[j]);
                setBulkAnswer(oldArray => [...oldArray, { "id": listIdAnswer[j], "answer": listAnswer[j] }]);
            }
            setIsBulk(true);
        }, 1000);
    }

    const bulkAnswerProcess = async() => {
        const token = localStorage.getItem("token");
        console.log("bulk answer: "+JSON.stringify(bulkAnswer));
        try {
            const result = await axios.post(APIURL + `/self-assesment/bulk-answer`,
                {
                    "answers": bulkAnswer
                }
                , {
                    headers:{
                        "Authorization": `Bearer ${token}`,
                        "Content-Type": "application/json",
                        "Accept": "application/json",
                    }
                });

            if (result.status === 200) {
                const data = result.data;
                console.log(JSON.stringify(data));
                alert("Data berhasil di update");
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
            } else {
                alert("Error: " + JSON.stringify(error.response.data?.data?.error));
            }
        }
    }

    const changeHandler = (index, event) => {
        try {
            console.log(event.target.files[0]);
            setSelectedFile(event.target.files[0]);
            setIsFilePicked(true);
            listDoc.splice(index, 1, event.target.files[0]);
            setListDoc([...listDoc]);
        } catch (error) {
            console.log(null);
            setSelectedFile(null);
            setIsFilePicked(false);
        }
    };

    const attachment = () => {
        return (
            <div style={styles.body}>
                <div style={styles.title}>
                    <h5>Apl 02</h5>
                </div>
                <div style={styles.table}>
                    <Table striped>
                        <thead style={{ background: 'black', color: 'white' }}>
                            <tr>
                                <th>NO</th>
                                <th>KODE</th>
                                <th>SKEMA UNIT</th>
                                <th style={{ display: 'flex', justifyContent: 'center' }}>APL 02</th>
                            </tr>
                        </thead>
                        {
                            listSchematicUnit.length != 0 ?
                                <tbody>
                                    {
                                        listSchematicUnit.map((item, index) => (
                                            <tr>
                                                <td scope="row">{index + 1} </td>
                                                <td>{item.code}</td>
                                                <td>{item.title}</td>
                                                <td style={{ display: 'flex', justifyContent: 'center' }}>
                                                    <Button color="primary"
                                                        onClick={() => getListSelfAsesmentByUnitId(item.id)}
                                                    >
                                                        SELF ASESMENT
                                                        {/* {
                                                            status === index ? "LOADING..." : "SELF ASESMENT"
                                                        } */}
                                                    </Button>
                                                </td>
                                            </tr>
                                        ))
                                    }
                                </tbody> :
                                <tbody>
                                    <tr>
                                        Loading...
                                    </tr>
                                </tbody>
                        }
                    </Table>
                </div>
                {modalSelfAssesment()}
            </div>
        );
    }



    const modalSelfAssesment = () => {
        if (modalLoading) {
            return (
                <Modal isOpen={modals} toggle={toggleTwo} size="lg" style={{ maxWidth: '700px', width: '100%' }} backdrop="static">
                    <ModalHeader toggle={toggleTwo}>Daftar Pertanyaan/Self Assesment</ModalHeader>
                    <ModalBody style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                        <Spinner size="lg" color="blue" />
                        <div>Saving...</div>
                    </ModalBody>
                </Modal>
            );
        }
        return (
            <Modal isOpen={modals} toggle={toggleTwo} size="lg" style={{ maxWidth: '700px', width: '100%' }} backdrop="static">
                <ModalHeader toggle={toggleTwo}>Daftar Pertanyaan/Self Assesment</ModalHeader>
                <ModalBody >
                    <Table striped>
                        <thead style={{ background: 'black', color: 'white', width: '70%' }}>
                            <tr>
                                <th>NO</th>
                                <th>Pertanyaan</th>
                                <th width="10%">K</th>
                                <th width="10%">BK</th>
                                <th width="10%">Dok</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                listSelfAssesmen.map((item, index) => (
                                    <tr>
                                        <td scope="row">{index + 1}</td>
                                        <td>{item.question}</td>
                                        <td width="20%">
                                            <div style={{ width: '100%', display: 'flex', justifyContent: 'flex-start', paddingLeft: '20%' }}>
                                                <Input
                                                    name={`checkbox-${index}`}
                                                    type="checkbox"
                                                    value={
                                                        "true"
                                                        // item.answer.answer != null ? item.answer : null
                                                    }
                                                    checked={listAnswer[index] ? true : false}
                                                    onChange={(e) => updateAnswerSelfAssessment(index, item.answer.id, e.currentTarget.value, item)}
                                                />
                                            </div>
                                        </td>
                                        <td width="20%">
                                            <div style={{ width: '100%', display: 'flex', justifyContent: 'flex-start', paddingLeft: '20%' }}>
                                                <Input
                                                    name={`checkbox-${index}`}
                                                    type="checkbox"
                                                    value={
                                                        "false"
                                                        // item.answer.answer != null ? item.answer : null
                                                    }
                                                    checked={listAnswer[index] == false ? true : false}
                                                    onChange={(e) => updateAnswerSelfAssessment(index, item.answer.id, e.currentTarget.value, item)}
                                                />
                                            </div>
                                        </td>
                                        <td>
                                            {/* <label id={`file-input-id-${index}`}>
                                                <img src={upload} />
                                            </label>
                                            <Input id={`file-input-id-${index}`} type="file" onChange={(e) => changeHandler(index, e)} accept="image/*" style={{display: 'none'}} /> */}
                                            <div>
                                                <label for={`file-input-${index}`} style={{
                                                    // backgroundColor: 'blue',
                                                    color: 'white',
                                                    // padding: '0.5rem',
                                                    // borderRadius: '0.3rem',
                                                    width: '60%',
                                                    // textAlign: 'center'
                                                }}>
                                                    <img src={upload} width={30} height={30} />
                                                    <Input id={`file-input-${index}`} type="file" onChange={(e) => changeHandler(index, e)} style={{ display: 'none' }} />
                                                </label>
                                            </div>
                                        </td>
                                        <td>
                                            <div>
                                                <label for={`button-input-${index}`} style={{
                                                    // backgroundColor: 'blue',
                                                    color: 'white',
                                                    // padding: '0.5rem',
                                                    // borderRadius: '0.3rem',
                                                    width: '60%',
                                                    // textAlign: 'center'
                                                }}>
                                                    <img src={send} width={20} height={20} />
                                                    <Input id={`button-input-${index}`} type="button" onClick={(e) => updateAnswerSelfAssessmentProccess(item.answer.id, listAnswer[index], listDoc[index])} style={{ display: 'none' }} />
                                                </label>
                                            </div>
                                            {/* <Button color="success" onClick={() => updateAnswerSelfAssessmentProccess(item.answer.id, listAnswer[index], listDoc[index])}><AiFillCaretRight size={20} /></Button> */}
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </Table>
                </ModalBody>
                <ModalFooter>
                    <Button color="danger" onClick={toggleTwo}>Tutup</Button>{' '}
                    <Button color="info" onClick={changeStatusAnswerAllTrue}>Kompeten Semua</Button>
                    <Button color="warning" style={{ color: 'white' }} onClick={changeStatusAnswerAllFalse}>Belum Kompeten Semua</Button>{' '}
                    {
                        isBulk?  <Button color="primary" style={{ color: 'white' }} onClick={bulkAnswerProcess}>Submit Semua</Button>: null
                    }
                </ModalFooter>
            </Modal>
        );
    }


    return (
        <div style={styles.body}>
            {attachment()}
        </div>
    );
}

const SelfAssessmentMemo = React.memo(SelfAssessment);

export default SelfAssessmentMemo;


const styles = {
    body: {
        width: '100%',
    },
    header: {
        width: '100%',
        backgroundColor: 'brown',
        color: 'white'
    },
    formInput: {
        backgroundColor: 'lightgrey',
        paddingRight: '1%',
        paddingLeft: '1%',
    }
}