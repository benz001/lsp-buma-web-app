import React, { useState } from 'react';
import { Form, FormGroup, Input, Label, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter, Collapse, Card, CardBody } from 'reactstrap';
import upload from '../assets/image/upload.png';
import checklist from '../assets/image/checklist.png';
import stoped from '../assets/image/stop.png';
export default function Attachment() {
    const [modal, setModal] = useState(false);

    const [modals, setModals] = useState(false);

    const [isOpen, setIsOpen] = useState(false);

    const toggleCollapse = () => { setIsOpen(!isOpen) };

    const toggle = () => {
        console.log('open');
        setModal(!modal)
    };

    const toggleTwo = () => {
        console.log('open');
        setModals(!modals)
    };
    return (
        <div style={styles.body}>
            <div style={styles.title}>
                <h5>Lampiran-lampiran</h5>
                <h6>Bukti kelengkapan persyaratan dasar pemohon</h6>
            </div>
            <div style={styles.table}>
                <Table striped>
                    <thead style={{ background: 'black', color: 'white' }}>
                        <tr>
                            <th>NO.</th>
                            <th>NAMA LAMPIRAN</th>
                            <th>JUDUL</th>
                            <th>DOK</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td scope="row">1</td>
                            <td>Copy KTP</td>
                            <td></td>
                            <td><img src={upload} onClick={toggle} height="30px" /></td>
                        </tr>
                        <tr>
                            <td scope="row">2</td>
                            <td>Copy Ijazah Terakhir</td>
                            <td></td>
                            <td><img src={upload} onClick={toggle} height="30px" /></td>
                        </tr>
                        <tr>
                            <td scope="row">3</td>
                            <td>Copy Sertifikasi Pelatihan Relevan</td>
                            <td></td>
                            <td><img src={upload} onClick={toggle} height="30px" /></td>
                        </tr>
                        <tr>
                            <td scope="row">4</td>
                            <td>Sertifikat Sertifikasi Pengoprasian Unit</td>
                            <td></td>
                            <td><img src={upload} onClick={toggle} height="30px" /></td>
                        </tr>
                        <tr>
                            <td scope="row">5</td>
                            <td>SIMPER sesuai Versality Unit</td>
                            <td></td>
                            <td><img src={upload} onClick={toggle} height="30px" /></td>
                        </tr>
                    </tbody>
                </Table>
            </div>
            <div style={styles.title}>
                <h5>Apl 02</h5>
            </div>
            <div style={styles.table}>
                <Table striped>
                    <thead style={{ background: 'black', color: 'white' }}>
                        <tr>
                            <th>NO</th>
                            <th>KODE</th>
                            <th>SKEMA UNIT</th>
                            <th>APL 02</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td scope="row">1</td>
                            <td>TIK.MM01.008.01</td>
                            <td>Menerapkan prinsip prinsip rancangan visual</td>
                            <td>
                                <Button color="primary" onClick={toggleTwo}>
                                    SELF ASSESSMENT
                                </Button>
                            </td>
                        </tr>
                    </tbody>
                </Table>
            </div>
            <Modal isOpen={modal} toggle={toggle} backdrop="static">
                <ModalHeader toggle={toggle}>Upload Lampiran Persyaratan</ModalHeader>
                <ModalBody>
                    <Form>
                        <FormGroup style={{ width: '100%', height: '100%', display: 'flex', flexDirection: 'row' }}>
                            <Label for="name"><i>Judul Dokumen</i></Label>
                            <div style={{ width: '10%' }} />
                            <Input type="text" name="name" id="name" />
                        </FormGroup>

                        <FormGroup style={{ width: '100%', height: '100%', display: 'flex', flexDirection: 'row' }}>
                            <Label for="name"><i>Keterangan</i></Label>
                            <div style={{ width: '10%' }} />
                            <Input type="text" name="name" id="name" />
                        </FormGroup>

                        <FormGroup style={{ width: '100%', height: '100%', display: 'flex', flexDirection: 'row' }}>
                            <Label for="name"><i>Upload Dokumen</i></Label>
                            <div style={{ width: '10%' }} />
                            <Input type="file" name="name" id="name" />
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="dangery" onClick={toggle}>Tutup</Button>{' '}
                    <Button color="info" onClick={toggle}>Simpan</Button>
                </ModalFooter>
            </Modal>
            <Modal isOpen={modals} toggle={toggleTwo} size="lg" style={{maxWidth: '700px', width: '100%'}} backdrop="static">
                <ModalHeader toggle={toggleTwo}>Daftar Pertanyaan/Self Assesment</ModalHeader>
                <ModalBody >
                    <Table striped>
                        <thead style={{ background: 'black', color: 'white', width: '70%' }}>
                            <tr>
                                <th>NO</th>
                                <th>Pertanyaan</th>
                                <th>K</th>
                                <th>TK</th>
                                <th>DOK BUKTI</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row">1</td>
                                <td>Pertanyaan 1</td>
                                <td><img src={checklist} onClick={toggle} height="30px" /></td>
                                <td><img src={stoped} onClick={toggle} height="30px" /></td>
                                <td><img src={upload} onClick={toggle} height="30px" /></td>
                            </tr>
                        </tbody>
                    </Table>
                </ModalBody>
                <ModalFooter>
                    <Button color="danger" onClick={toggleTwo}>Tutup</Button>{' '}
                    <Button color="warning" style={{color:'white'}} onClick={toggleTwo}>Tidak Kompeten Semua</Button>{' '}
                    <Button color="info" onClick={toggleTwo}>Kompeten Semua</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}

const styles = {
    body: {
        width: '100%',
        paddingRight: '2%',
        paddingLeft: '2%'
    },
    header: {
        width: '100%',

        color: 'black'
    },
    table: {
        // backgroundColor: 'lightgrey',
        paddingRight: '1%',
        paddingLeft: '1%',
    }
}