import React, { useContext, useEffect, useState } from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useHistory,
    useRouteMatch
} from "react-router-dom";
import PendaftaranGrupAsesi from "./PendaftaranGrupAsesi";
import logo from '../../assets/image/logo.PNG';
import profile from '../../assets/image/ic_profil.png';
import asesi from '../../assets/image/ic_asesi.png';
import group from '../../assets/image/group.png';
import pra_asesmen from '../../assets/image/ic_pra_asesmen.png';
import asesmen from '../../assets/image/ic_asesmen.png';
import AccessionRegistration from "./AccessionRegistrationPage";
import AccessionProfilePage from "./AccessionProfilePage";
import AccessionPraAssesment from "./AccessionPraAssesment";
import AccessionAssesment from "./AccessionAssesment";
import { AiOutlineImport } from "react-icons/ai";
import ModalAlert from "../../components/ModalAlert";
import { changeStatus } from "../../provider/actions";
// import { Context } from "../../provider/provider";




export default function SidebarBarNavigationAccession() {
    let { path, url } = useRouteMatch();
    // const { state, dispatch } = useContext(Context);

    useEffect(() => {
        checkRolesAuth();
    }, []);
    let history = useHistory();
    const [activeIndicator, setActiveIndicator] = useState(0);
    const [showModal, setShowModal] = useState(false);
    const [hideWelcome, setHideWelcome] = useState(false);

    const changeShowModal = () => {
        history.push("/");
    }

    const routes = [
        {
            path: `${url}`,
            exact: true,
            sidebar: () => <div style={{ width: '100%', height: '100%', display: 'flex', flexDirection: 'row', justifyContent: 'center' }}> <h3 style={{ marginTop: '25%' }}>Selamat Datang, Silahkan Pilih Menu yang Tersedia</h3></div>,
            main: () => <div style={{ width: '100%', height: '100%', display: 'flex', flexDirection: 'row', justifyContent: 'center' }}> <h3 style={{ marginTop: '25%' }}>Selamat Datang, Silahkan Pilih Menu yang Tersedia</h3></div>
        },
        {
            path: `${url}/profil-asesi`,
            sidebar: () => <AccessionProfilePage />,
            main: () => <AccessionProfilePage />
        },
        {
            path: `${url}/pendaftaran-asesi`,
            sidebar: () => <AccessionRegistration />,
            main: () => <AccessionRegistration />
        },
        {
            path: `${url}/pendaftaran-grup-asesi`,
            sidebar: () => <PendaftaranGrupAsesi />,
            main: () => <PendaftaranGrupAsesi />
        },
        {
            path: `${url}/pra-asesmen-asesi`,
            sidebar: () => <AccessionPraAssesment />,
            main: () => <AccessionPraAssesment />
        },
        {
            path: `${url}/asesmen-asesi`,
            sidebar: () => <AccessionAssesment />,
            main: () => <AccessionAssesment />
        }
    ];

    const checkRolesAuth = () => {
        let roles = localStorage.getItem("roles");
        if (roles != null) {
            if (roles === "asesi") {
                setShowModal(false);
            } else {
                setShowModal(true);
            }
        } else {
            setShowModal(true);
        }
    }




    const chooseMenu = (indicator, status) => {
        if (indicator != 6) {
            setActiveIndicator(indicator);
            setHideWelcome(status);
        } else {
            if (window.confirm('Apa Anda yakin ingin keluar ?')) {
                // Save it!
                // dispatch(changeStatus(false));
                localStorage.clear();
                history.push("/");
                console.log('Thing was saved to the database.');
            } else {
                // Do nothing!
                console.log('Thing was not saved to the database.');
            }
        }
    }
    return (
        <Router>
            <div style={{ width: '100%' }}>
                <div style={{ width: '100%', height: '15%', background: '#2F5597', display: 'flex', flexDirection: 'row' }}>
                    <img src={logo} style={{ paddingLeft: '1%', paddingTop: '1%', paddingBottom: '2%' }} height='85px' />
                    <div style={{ borderLeft: '3px solid white', height: '50px', marginTop: '12px', marginLeft: '5px' }} />
                    <div style={{ display: 'flex', flexDirection: 'column', paddingLeft: '1%', paddingTop: '1%', color: 'white' }}>
                        <h6>LSP BUMA</h6>
                        <h6>Lembaga Sertifikasi Profesi PT Bukit Makmur Mandiri Utama</h6>
                    </div>
                </div>
                <div style={{ width: '100%', height: '1350px', display: 'flex', flexDirection: 'row', position: 'relative' }}>
                    <div style={{ width: '20%', background: '#E2EBCF' }}>
                        <div style={{ height: '5px' }} />
                        <ul style={{ listStyleType: "none", padding: 0 }}>
                            <li style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                                <img src={profile} height="30px" />
                                <Link onClick={() => chooseMenu(1, true)} style={{ color: activeIndicator === 1 ? 'blue' : 'black', textDecoration: 'none' }} to={`${url}/profil-asesi`}>Profile</Link>
                            </li>
                            <hr style={{ borderTop: activeIndicator === 1 ? '1px solid red' : '1px solid white' }} />
                            <li style={{ display: 'flex', flexDirection: 'row', paddingTop: 'px' }}>
                                <img src={asesi} height="30px" />
                                <Link onClick={() => chooseMenu(2, true)} style={{ color: activeIndicator === 2 ? 'blue' : 'black', textDecoration: 'none' }} to={`${url}/pendaftaran-asesi`}>Pendaftaran Asesi</Link>
                            </li>
                            <hr style={{ borderTop: activeIndicator === 2 ? '1px solid red' : '1px solid white' }} />
                            <li style={{ display: 'flex', flexDirection: 'row' }}>
                                <img src={group} height="30px" />
                                <Link onClick={() => chooseMenu(3, true)} style={{ color: activeIndicator === 3 ? 'blue' : 'black', textDecoration: 'none' }} to={`${url}/pendaftaran-grup-asesi`}>Pendaftaran Grup Asesi</Link>
                            </li>
                            <hr style={{ borderTop: activeIndicator === 3 ? '1px solid red' : '1px solid white' }} />
                            <li style={{ display: 'flex', flexDirection: 'row' }}>
                                <img src={pra_asesmen} height="30px" />
                                <Link onClick={() => chooseMenu(4, true)} style={{ color: activeIndicator === 4 ? 'blue' : 'black', textDecoration: 'none' }} to={`${url}/pra-asesmen-asesi`}>Pra-Asesmen</Link>
                            </li>
                            <hr style={{ borderTop: activeIndicator === 4 ? '1px solid red' : '1px solid white' }} />
                            <li style={{ display: 'flex', flexDirection: 'row' }}>
                                <img src={asesmen} height="30px" />
                                <Link onClick={() => chooseMenu(5, true)} style={{ color: activeIndicator === 5 ? 'blue' : 'black', textDecoration: 'none' }} to={`${url}/asesmen-asesi`}>Asesmen</Link>
                            </li>
                            <hr style={{ borderTop: activeIndicator === 5 ? '1px solid red' : '1px solid white' }} />
                            <li style={{ display: 'flex', flexDirection: 'row' }}>
                                {/* <img src={person} height="30px" /> */}
                                <AiOutlineImport color="#B0131E" size={25} />
                                <Link onClick={() => chooseMenu(6, true)} style={{ color: activeIndicator === 6 ? 'blue' : 'black', textDecoration: 'none' }}>Logout</Link>
                            </li>
                        </ul>
                    </div>
                    <div style={{ width: '80%', height: '100%' }}>
                        <Switch>
                            {routes.map((route, index) => (
                                <Route
                                    key={index}
                                    path={route.path}
                                    exact={route.exact}
                                    children={<route.main />}
                                />
                            ))}
                        </Switch>
                    </div>
                </div>
                <ModalAlert statusOpen={showModal} toggle={changeShowModal} title={"Pesan"} content={"Maaf Anda tidak memiliki akses kehalaman ini"} close={changeShowModal} />
            </div>
        </Router>
    );
}

