import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Form, FormGroup, Input, Label } from 'reactstrap';


export default function RegisterAccessionPage() {
    return (
        <div style={styles.body}>
            <div style={styles.header}><b>REGISTRASI ANGGOTA BARU</b></div>
            <div style={styles.opening}>
                <b>Jika anda belum terdaftar sebagai anggota</b>
               
                <p>
                    Silahkan isi form di bawah ini untuk membuat user atau id member baru<br>
                    </br>
                    Atau jika sudah mempunyai id atau user anggota silahkan login di
                   <br/>
                    <Link to='/login'>
                        <Button style={{ color: 'black', background: '#CDF0F2' }}>
                            Halaman Login
                        </Button>
                    </Link>
                </p>
            </div>
           
            <div style={styles.formRegistration}>
                <Form>
                    <FormGroup>
                        <Label for="name"><i>Nama</i></Label>
                        <Input type="text" name="name" id="name" />
                    </FormGroup>
                   
                    <FormGroup>
                        <Label for="email"><i>Email</i></Label>
                        <Input type="email" name="email" id="email" />
                    </FormGroup>
                   
                    <FormGroup>
                        <Label for="password"><i>Password</i></Label>
                        <Input type="password" name="password" id="password" />
                    </FormGroup>
                   
                    <FormGroup>
                        <Label for="repassword"><i>Ulang Password</i></Label>
                        <Input type="password" name="repassword" id="repassword" />
                    </FormGroup>
                   
                    <Button style={{ color: 'black', background: '#CDF0F2' }}>DAFTAR ANGGOTA</Button>
                </Form>
            </div>
        </div>
    );
}

const styles = {
    body: {
        width: '100%',
        backgroundColor: '#F4F3EF',
        paddingRight: '1%',
        paddingLeft: '1%'
    },
    opening: {
        backgroundColor: 'white',
        paddingBottom: '2%',
        paddingRight: '1%',
        paddingLeft: '1%'
    },
    header: {
        fontSize: '20px',
        backgroundColor: '#B3936A',
        color: 'white',
        paddingRight: '1%',
        paddingLeft: '1%'
    },
    formRegistration: {
        width: '100%',
        backgroundColor: 'white',
        paddingRight: '1%',
        paddingLeft: '1%'
    }
}