import React, { useEffect, useState } from 'react';
import { Button, Input, Label, Spinner, Table } from 'reactstrap';
import { AiOutlineCheck, AiOutlineDownload, AiOutlineStop, AiOutlineUpload } from 'react-icons/ai';
import { useFilePicker } from 'use-file-picker';
import SpinnerLoading from '../../components/SpinnerLoading';
import { APIURL } from '../../helper/LinkAPI';
import { convertDate, convertTime } from '../../utils/DateTimeConverter';
import axios from 'axios';
import ModalSpinner from '../../components/ModalSpinner';
import { useHistory } from 'react-router';
import { LOCAL_URL, WEB_URL } from '../../helper/LinkWeb';


export default function AccessionPraAssesment() {
    const [openFileSelector, { filesContent, loading }] = useFilePicker({
        accept: '.docx',
    });
    let history = useHistory();
    const [registrationAsesiId, setRegistrationAsesiId] = useState(null);
    const [schedule, setSchedule] = useState();
    const [isLoading, setIsLoading] = useState(true);
    const [isLoadingProgress, setIsLoadingProgress] = useState(false);
    const [listDocument, setListDocument] = useState([]);
    const [listDone, setListDone] = useState([]);
    const [selectedFile, setSelectedFile] = useState(null);
    const [isFilePicked, setIsFilePicked] = useState(false);
    const [alreadyRegister, setAlreadyRegister] = useState(null);


    useEffect(() => {
        checkRegistrationAsesi();
    }, []);

    const checkRegistrationAsesi = async () => {
        const token = localStorage.getItem("token");
        setIsLoading(true);
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + "/user/me", {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                if (status === "success") {
                    console.log("data success: " + JSON.stringify(data));
                    const id = data.asesi.id;
                    console.log("data successor: " + JSON.stringify(id));
                    if (parseInt(data.asesi.track) === 2) {
                        alert("Data belum tersedia");
                        history.push('/asesi');
                    } else if (parseInt(data.asesi.track) === 5) {
                        setAlreadyRegister(false);
                        if (id != null) {
                            setRegistrationAsesiId(id);
                            setTimeout(() => {
                                console.log("asesi: " + registrationAsesiId);
                                getScheduleAndDocument(id);
                            }, 1000);
                        } else {
                            alert("Maaf Anda belum melakukan pendaftaran asesi");
                        }
                    } if (parseInt(data.asesi.track) > 2 && parseInt(data.asesi.track) <= 4) {
                        setAlreadyRegister(true);
                        setIsLoading(false);
                    }
                    // if (parseInt(data.asesi.track) > 2 && parseInt(data.asesi.track) >= 3) {
                    //     setAlreadyRegister(true);
                    //     setIsLoading(false);

                    // } else if (parseInt(data.asesi.track) > 4) {
                    //     setAlreadyRegister(false);
                    //     setIsLoading(false);
                    // } else {
                    //     setAlreadyRegister(false);
                    //     if (id != null) {
                    //         setRegistrationAsesiId(id);
                    //         setTimeout(() => {
                    //             console.log("asesi: " + registrationAsesiId);
                    //             getScheduleAndDocument(id);
                    //         }, 1000);
                    //     } else {
                    //         alert("Maaf Anda belum melakukan pendaftaran asesi");
                    //     }
                    // }
                }
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                history.push('/asesi');
            } else {
                alert("Maaf Anda belum melakukan pendaftaran asesi");
                history.push('/asesi');
            }
        }
    }

    const getDocument = () => {
        for (let index = 0; index < listDocument.length; index++) {
            setListDone(oldArray => [...oldArray, false]);
            console.log("count: " + index);
        }
    }


    const getScheduleAndDocument = async (id) => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + `/assesment-schedule/pra-assesment/${id}`, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const dataSchedule = result.data.data;
                const dataDocument = result.data.data.schematic_registration.praassesment_method[0].praassesment_document;
                if (status === "success") {
                    console.log("data schedule success: " + JSON.stringify(dataSchedule));
                    console.log("data document success: " + JSON.stringify(dataDocument));
                    setSchedule(dataSchedule);
                    setListDocument(dataDocument);
                    setTimeout(() => {
                        getDocument()
                    }, 1000);
                    setTimeout(() => {
                        setIsLoading(false)
                    }, 2000);
                }
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                history.push('/asesi');
            } else {
                alert("Maaf data belum tersedia");
                history.push('/asesi');
            }
        }
    }

    const changeHandler = (event, index, document, type) => {
        try {
            console.log(event.target.files[0]);
            setSelectedFile(event.target.files[0]);
            setIsFilePicked(true);
            handleChange(index, true);
            setIsLoadingProgress(true);
            setTimeout(() => {
                if (document != null) {
                    if (type == "update") {
                        updateDocument(document, event.target.files[0]);
                    } else {
                        createDocument(document, event.target.files[0]);
                    }
                    console.log("document != null");
                } else {
                    console.log("document == null");
                }
            }, 1000);
        } catch (error) {
            console.log(null);
            setSelectedFile(null);
            setIsFilePicked(false);
            handleChange(index, false);
        }
    };

    const updateDocument = async (document, files) => {
        console.log('res: ' + JSON.stringify(document));
        const token = localStorage.getItem("token");
        const formData = new FormData();
        formData.append("status", "wait");
        formData.append("praassesment_method_document_id", document.praassesment_method_document_id);
        formData.append("asesi_registration_id", document.asesi_registration_id);
        formData.append("file", files);
        try {
            const result = await axios.post(APIURL + `/praassesment/answer/${document.id}?_method=PUT`, formData, {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "multipart/form-data",
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                setIsLoadingProgress(false);
                if (status === "success") {
                    refreshDocument().then((value) => {
                        console.log("val: " + value);
                        if (value) {
                            alert("Dokumen berhasil di update");
                            setIsLoadingProgress(false);
                        } else {
                            alert("Dokumen gagal di update");
                            setIsLoadingProgress(false);
                        }
                    });
                }
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                setIsLoadingProgress(false);
            } else {
                alert("Error: " + JSON.stringify(error.response.data?.data?.error));
                setIsLoadingProgress(false);
            }
        }

    }


    const createDocument = async (document, files) => {
        console.log('res doc: ' + JSON.stringify(document));
        console.log('res files: ' + JSON.stringify(files));
        const token = localStorage.getItem("token");
        const formData = new FormData();
        formData.append("status", "wait");
        formData.append("praassesment_method_document_id", document.id);
        formData.append("asesi_registration_id", registrationAsesiId);
        formData.append("file", files);
        try {
            const result = await axios.post(APIURL + "/praassesment/answer", formData, {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "multipart/form-data",
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                setIsLoadingProgress(false);
                if (status === "success") {
                    refreshDocument().then((value) => {
                        console.log("val: " + value);
                        if (value) {
                            alert("Dokumen berhasil di update");
                            setIsLoadingProgress(false);
                        } else {
                            alert("Dokumen gagal di update");
                            setIsLoadingProgress(false);
                        }
                    });
                }
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                setIsLoadingProgress(false);
            } else {
                alert("Error: " + JSON.stringify(error.response.data?.data?.error));
                setIsLoadingProgress(false);
            }
        }
    }

    const handleChange = (index, e) => {
        listDone.splice(index, 1, e)
        console.log(listDone);
        // setListDone([...listDone]);
    }

    const refreshDocument = async () => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + `/assesment-schedule/pra-assesment/${registrationAsesiId}`, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const dataSchedule = result.data.data;
                const dataDocument = result.data.data.schematic_registration.praassesment_method[0].praassesment_document;
                if (status === "success") {
                    console.log("data schedule success: " + JSON.stringify(dataSchedule));
                    console.log("data document success: " + JSON.stringify(dataDocument));
                    setListDocument(dataDocument);
                }
                return true;
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);

            } else {
                alert("Error: " + JSON.stringify(error.response.data?.data?.error));
            }
            return false;
        }
    }



    if (isLoading) {
        return (
            <SpinnerLoading />
        );
    }

    return (
        alreadyRegister ?
            <div style={{ width: '100%', height: '100%', justifyContent: 'center', display: 'flex', fontWeight: 'bold' }}>
                Anda Sudah Melakukan Proses Pra-Asesmen
            </div> :
            <div style={styles.container}>
                <div style={styles.schedule}>
                    <div style={styles.titleSchedule}>
                        <Label style={{ fontWeight: 'bold', fontSize: '14px', display: 'flex', justifyContent: 'center', paddingTop: '5px' }}>Jadwal Pra-Asesmen</Label>
                    </div>
                    <div style={styles.contentShedule}>
                        <Table>
                            <tr>
                                <td style={{ width: '20%' }}>Hari/tanggal</td>
                                <td style={{ width: '5%' }}>:</td>
                                <td>{convertDate(schedule.date_schedule)}</td>
                            </tr>
                            <tr>
                                <td >Pukul</td>
                                <td >:</td>
                                <td>{convertTime(schedule.time_schedule)} WIB</td>
                            </tr>
                            <tr>
                                <td>Tempat</td>
                                <td>:</td>
                                <td>{schedule.place_schedule}</td>
                            </tr>
                            <tr>
                                <td>Link Meeting</td>
                                <td>:</td>
                                <td>
                                    <a href={`${schedule.link_schedule}`} target="_blank" style={{ color: 'blue' }}>
                                        {schedule.link_schedule}
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>Nama Asesor</td>
                                <td>:</td>
                                <td>{schedule.asesor_name}</td>
                            </tr>
                        </Table>
                    </div>

                </div>
                <div style={styles.formulir}>
                    <div style={styles.titleListFormulir}>
                        <Label style={{ fontWeight: 'bold', fontSize: '14px', display: 'flex', justifyContent: 'center', paddingTop: '5px' }}>Formulir</Label>
                    </div>
                    <div style={styles.contentListFormulir}>
                        <Table>
                            {

                                listDocument.map((item, index) => (
                                    <tr>
                                        <td style={{ width: '5%' }}>{index + 1}</td>
                                        <td style={{ color: 'blue', width: '35%' }}>{item.name}</td>
                                        <td>
                                            <Button color="success" onClick={() => {
                                                // memperbaiki logika untuk download file yang telah di upload
                                                // ketika dokumen telah di upload maka akan ambil file
                                                // jika tidak terdapat file maka mengecek is generate 
                                                // is generate = 0 maka akan download template dari file
                                                // is generate = 1 maka akan download file dari hasil generate apl 01 dan apl 02
                                                item.document_answer != null ?
                                                    window.open(`https://test.devinfolspbuma.online/public${item.document_answer.file}`, '_blank') : (
                                                        item.is_generate = 1 ?
                                                            window.open(`https://test.devinfolspbuma.online/public/api/convert-pdf/${registrationAsesiId}`, '_blank') :
                                                            window.open(`https://test.devinfolspbuma.online/public${item.file}`, '_blank')
                                                    )

                                            }}>
                                                <AiOutlineDownload /> Download
                                            </Button>
                                        </td>
                                        <td>
                                            {/* {item.is_generate == "0" ? */}
                                            <div>
                                                <label for={`file-input-${index}`} style={{
                                                    backgroundColor: 'blue',
                                                    color: 'white',
                                                    padding: '0.5rem',
                                                    borderRadius: '0.3rem',
                                                    width: '60%',
                                                    textAlign: 'center'
                                                }}>
                                                    <AiOutlineUpload color="white" />
                                                    Upload
                                                    <Input id={`file-input-${index}`} type="file" onChange={(e) => { item.document_answer != null ? changeHandler(e, index, item.document_answer, "update") : changeHandler(e, index, item, "create") }} accept=".xlsx,.xls,.doc, .docx,.ppt, .pptx,.txt,.pdf" style={{ display: 'none' }} />
                                                </label>
                                            </div>
                                            {/* : null */}
                                            {/* } */}
                                        </td>
                                        <td style={{ width: '10%' }}>
                                            {
                                                // item.is_generate == "0" ? 
                                                item.document_answer != null ? <AiOutlineCheck color={"green"} size={20} /> : <AiOutlineStop color={"red"} size={20} />
                                                //  : <AiOutlineCheck color={"green"} size={20} />
                                            }
                                        </td>
                                    </tr>))
                            }
                        </Table>
                    </div>
                    {/* <div style={{ width: '95%', background: 'transparent', display: 'flex', alignItems: 'flex-end', justifyContent: 'flex-end' }}>
                    <Button>Submit</Button>
                </div> */}
                </div>
                <ModalSpinner statusOpen={isLoadingProgress} toggle={isLoadingProgress} />
            </div>

    );
}

const styles = {
    container: {
        // background: 'grey',
        width: '100%',
        // height: '100%',
        paddingLeft: '5%'
    },
    schedule: {
        // background: 'red',
        width: '100%',
        height: '50%'
    },
    formulir: {
        // background: 'blue',
        width: '100%',
        height: '50%',
        marginTop: '25px'
    },
    titleSchedule: {
        width: '30%',
        height: '40px',
        background: '#E9E9E9',
        marginTop: '5px'
    },
    contentShedule: {
        width: '95%',
        height: '250px',
        background: '#E9E9E9',
        marginTop: '10px'
    },
    titleListFormulir: {
        width: '30%',
        height: '40px',
        background: '#E9E9E9',
        marginTop: '5px'
    },
    contentListFormulir: {
        width: '95%',
        // height: '200px',
        background: '#E9E9E9',
        marginTop: '10px'
    }
};