import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { AiFillEdit, AiFillFolderOpen, AiOutlineDownload, AiOutlineUpload, AiOutlineUserAdd } from 'react-icons/ai';
import { Button, Col, FormGroup, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader, Pagination, PaginationItem, PaginationLink, Row, Spinner, Table } from 'reactstrap';
import { APIURL } from '../../helper/LinkAPI';
import AsyncSelect from 'react-select/async';
import Select from 'react-select';
import { checkThisEmpty } from "../../helper/CheckEmpty";
import ModalSpinner from '../../components/ModalSpinner';
import { convertDate } from '../../utils/DateTimeConverter';
import SpinnerLoading from '../../components/SpinnerLoading';


export default function AssessorAssesment() {
    const [date, setDate] = useState(null);
    const [time, setTime] = useState(null);
    const [place, setPlace] = useState(null);
    const [placeCompetence, setPlaceCompetence] = useState(null);
    const [placeCompetenceId, setPlaceCompetenceId] = useState(null);
    const [link, setLink] = useState(null);
    const [asesi, setAsesi] = useState(null);
    const [asesor, setAsesor] = useState(null);
    const [method, setMethod] = useState(null);
    const [listAsesi, setListAsesi] = useState();
    const [listPlaceCompetence, setListPlaceCompetence] = useState([]);
    const [listMethod, setListMethod] = useState([]);
    const [listSchedule, setListSchedule] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [isLoadingProgress, setIsLoadingProgress] = useState(false);
    const [isLoadingContent, setIsLoadingContent] = useState(true);

    const [modalUpdate, setModalUpdate] = useState(false);
    const [loadingModalUpdate, setLoadingModalUpdate] = useState(true);
    const [savingModalUpdate, setSavingModalUpdate] = useState(false);
    const [placeCompetenceFocus, setPlaceCompetenceFocus] = useState(false);

    const [id, setId] = useState(null);
    const [userId, setUserId] = useState(null)
    const [page, setPage] = useState(1);
    const [totalPage, setTotalPage] = useState(null);
    const [loadingPage, setLoadingPage] = useState(false);
    const [indexPage, setIndexPage] = useState(1);


    // useEffect(() => {
    //     getProfile();
    // }, []);

    useEffect(() => {
        getPlaceCompetence().then((value) => {
            if (value) {
                getAsesi().then((values) => {
                    if (values) {
                        getAssesmentMethod().then((valueses) => {
                            if (valueses) {
                                getProfile().then((id)=>{
                                    setUserId(id);
                                    getListSchedule(id)
                                });
                            }
                        });
                    }
                });
            }
        });
    }, []);

    const getProfile = async () => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + "/user/me", {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                if (status === "success") {
                    console.log("data success: " + JSON.stringify(data));
                    console.log("track: " + data?.asesi);
                    setTimeout(() => {
                        setIsLoading(false)
                    }, 1000);
                    console.log("user id: " +data.user_profile.user_id)
                    // data.user_profile.user_id != null ? setUserId(data.user_profile.user_id) : setUserId("");
                }
                return data.user_profile.user_id;
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                setIsLoading(false);
            } else {
                alert("Error: " + JSON.stringify(error.response.data?.data?.error));
                setIsLoading(false);
            }

            return null;
        }
    }

    const getPlaceCompetence = async () => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);

        try {
            const result = await axios.get(APIURL + "/place-competence", {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            console.log("get place competence");
            if (result.status === 200) {
                const data = result.data;
                const placeCompetence = data.data;
                console.log("res: " + JSON.stringify(placeCompetence));
                const tempArrayPlaceCompetence = [];
                if (placeCompetence.length) {
                    placeCompetence.forEach((element) => {
                        tempArrayPlaceCompetence.push({
                            label: element.name,
                            value: `${element.id}`
                        });
                    });
                } else {
                    tempArrayPlaceCompetence.push({
                        label: "",
                        value: ""
                    });
                }
                setListPlaceCompetence(tempArrayPlaceCompetence);
            } else {
                setListPlaceCompetence([]);;
            }
            return true;
        } catch (error) {
            // console.log("error: " + error);
            setListPlaceCompetence([]);
            return false;
        }
    }

    const getAsesi = async () => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            // const result = await axios.get(APIURL + "/assesment-schedule/pra-assesment/list?track=3", {
            //     headers: {
            //         "Authorization": `Bearer ${token}`
            //     }
            // });
             const result = await axios.get(APIURL + "/assesment-schedule/pra-assesment/list?track=5", {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });
            const data = result.data.data;
            const tempArrayAsesi = [];
            console.log("get Asesi: " + JSON.stringify(data));

            if (result.status == 200) {
                const asesi = data;
                if (asesi.length) {
                    asesi.forEach((element) => {
                        tempArrayAsesi.push({
                            label: element.asesi_name,
                            value: element.asesi_registration_id
                        });
                    });
                } else {
                    tempArrayAsesi.push({
                        label: '',
                        value: ''
                    });
                }
                setListAsesi(tempArrayAsesi);
            } else {
                setListAsesi(tempArrayAsesi);
            }

            return true;
        } catch (error) {
            setListAsesi([]);
            return false;
        }
    }

    const getAssesmentMethod = async () => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);

        try {
            const result = await axios.get(APIURL + "/assesment/method", {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            console.log("get assesment method");
            if (result.status === 200) {
                const data = result.data;
                const assesmentMethod = data.data;
                console.log("res: " + JSON.stringify(assesmentMethod));
                const tempArrayAssesmentMethod = [];
                if (assesmentMethod.length) {
                    assesmentMethod.forEach((element) => {
                        tempArrayAssesmentMethod.push({
                            label: element.name,
                            value: `${element.id}`
                        });
                    });
                } else {
                    tempArrayAssesmentMethod.push({
                        label: "",
                        value: ""
                    });
                }

                setListMethod(tempArrayAssesmentMethod);
            } else {
                setListMethod([]);
            }
            return true;
        } catch (error) {
            // console.log("error: " + error);
            setListMethod([]);
            return false;
        }
    }

    const checkValidation = () => {
        if (
            checkThisEmpty(date) ||
            checkThisEmpty(time) ||
            checkThisEmpty(placeCompetence) ||
            checkThisEmpty(link) ||
            checkThisEmpty(asesi) ||
            checkThisEmpty(method)) {
            alert("Field input tidak boleh kosong");
        } else {
            createAssessmentsShedule();
        }
    }

    const clearField = () => {
        setDate(null);
        setTime(null);
        setPlaceCompetence("");
        setLink("");
        setAsesi("");
        setAsesor("");
        setPlaceCompetenceId(null);
    }


    const createAssessmentsShedule = async () => {
        const token = localStorage.getItem("token");
        // console.log("token: " + token);
        const body = {
            "date_schedule": date,
            "time_schedule": time,
            "place_schedule": placeCompetence,
            "link_schedule": link,
            "place_competence_id": parseInt(place),
            "asesi_registration_id": parseInt(asesi),
            "assesment_method_id": parseInt(method)
        }
        // console.log(body);
        setIsLoadingProgress(true);
        try {
            const result = await axios.post(APIURL + "/assesment-schedule/assesment", body, {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                console.log("result: " + JSON.stringify(result.data));
                if (status === "success") {
                    alert("Update Success");
                    setIsLoadingProgress(false);
                    refreshListSchedule().then((value) => {
                        if (value) {
                            setModalUpdate(false);
                        }
                    });
                }
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                setIsLoadingProgress(false);
            } else {
                alert("Error: " + JSON.stringify(error.response.data?.data?.error));
                setIsLoadingProgress(false);
            }
        }
    }

    const getListSchedule = async (userId) => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + `/assesment-schedule/assesment?assesor_id=${userId}`, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                const total = result.data.data;
                if (status === "success") {
                    var listResult = data.data;
                    console.log("data success list schedule: " + JSON.stringify(data));
                    var newArray = listResult.filter(function (el) {
                        return el.assesor.id == userId;
                    });
                    setListSchedule(listResult);
                    setTotalPage(total.last_page);
                    setTimeout(() => {
                        setIsLoadingContent(false)
                    }, 1000);
                }
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
            } else {
                alert("Error: " + JSON.stringify(error.response.data?.data?.error));
            }
        }
    }

    const getDetailUpdate = async (id) => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + `/assesment-schedule/assesment-id/${id}`, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                if (status === "success") {
                    console.log("data success: " + JSON.stringify(data));
                    data.date_schedule != null ? setDate(data.date_schedule) : setDate(null);
                    data.time_schedule != null ? setTime(data.time_schedule) : setTime(null);
                    data.place_schedule != null ? setPlaceCompetence(data.place_schedule) : setPlaceCompetence("");
                    data.place_competence_id != null ? setPlaceCompetenceId(data.place_competence_id) : setPlaceCompetenceId(null);
                    data.link_schedule != null ? setLink(data.link_schedule) : setLink("");
                    data.assesor.name != null? setAsesor(data.assesor.name): setAsesor("");
                    data.asesi_registration.name != null? setAsesi(data.asesi_registration.name): setAsesi("");

                }
                return true;
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                setSavingModalUpdate(false);
            } else {
                alert("Error: " + error.response.data?.data?.error);
                setSavingModalUpdate(false);
            }
            return false;
        }

    }

    const updateSchedule = async () => {
        if (checkThisEmpty(date) ||
            checkThisEmpty(time) ||
            checkThisEmpty(placeCompetence) ||
            checkThisEmpty(link)) {
            alert("Field input tidak boleh kosong");
        } else {
            const token = localStorage.getItem("token");
            console.log("token: " + token);
            setSavingModalUpdate(true);
            try {
                const result = await axios.put(APIURL + `/assesment-schedule/assesment/${id}/update`, {
                    "date_schedule": date,
                    "time_schedule": time,
                    "link_schedule": link,
                    "place_competence_id": parseInt(placeCompetenceId)
                }, {
                    headers: {
                        "Authorization": `Bearer ${token}`,
                        "Content-Type": "application/json"
                    }
                });

                if (result.status === 200) {
                    const meta = result.data.meta;
                    const status = meta.status;
                    const data = result.data.data;
                    if (status === "success") {
                        alert("Update Success");
                        setSavingModalUpdate(false);
                        setIsLoadingContent(true);
                        refreshListSchedule().then((value) => {
                            if (value) {
                                setModalUpdate(false);
                            }
                        });
                    }
                }
            } catch (error) {
                if (error.message === "Network Error") {
                    alert("Error: " + error.message);
                    setSavingModalUpdate(false);
                } else {
                    alert("Error: " + error.response.data.data.error);
                    setSavingModalUpdate(false);
                }
            }
        }
    }

    const refreshListSchedule = async () => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + `/assesment-schedule/assesment?assesor_id=${userId}`, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                const total = result.data.data;
                if (status === "success") {
                    console.log("data success list schedule: " + JSON.stringify(data));
                    var listResult = data.data;
                    console.log("data success list schedule: " + JSON.stringify(data));
                    var newArray = listResult.filter(function (el) {
                        return el.assesor.id == parseInt(userId);
                    });
                    setListSchedule(newArray);
                    // setTotalPage(total.last_page);
                    setTimeout(() => {
                        setIsLoadingContent(false)
                    }, 1000);
                }
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                // history.push('/admin');
            } else {
                alert("Error: " + JSON.stringify(error.response.data?.data?.error));
                // history.push('/admin');
            }
        }
    }

    const toggleUpdateOpen = (id) => {
        clearField();
        getDetailUpdate(id).then((value) => {
            if (value) {
                console.log("value: " + value);
                setId(id);
                setModalUpdate(!modalUpdate);
                if (loadingModalUpdate) {
                    setTimeout(() => {
                        setLoadingModalUpdate(false);
                    }, 1000);
                }
            } else {
                alert("Get data failed");
            }
        });
    }

    const toggleUpdateClose = () => {
        clearField();
        setId(null);
        setModalUpdate(!modalUpdate);
        if (loadingModalUpdate) {
            setTimeout(() => {
                setLoadingModalUpdate(false);
            }, 1000);
        }
    }


    const contentSchedule = () => {
        return (
            <div style={styles.schedule}>
                <div style={styles.titleSchedule}>
                    <Label style={{ fontWeight: 'bold', fontSize: '14px', display: 'flex', justifyContent: 'center', paddingTop: '5px' }}>Jadwal Asesmen</Label>
                </div>
                <div style={styles.contentShedule}>
                    <Table>
                        <tr>
                            <td style={{ width: '20%' }}>Hari/tanggal</td>
                            <td style={{ width: '5%' }}>:</td>
                            <td><Input type="date" onChange={(e) => setDate(e.target.value)} /></td>
                        </tr>
                        <tr>
                            <td >Pukul</td>
                            <td >:</td>
                            <td><Input type="time" onChange={(e) => setTime(e.target.value)} /></td>
                        </tr>
                        <tr>
                            <td>Tempat</td>
                            <td>:</td>
                            <td>
                                <Select
                                    options={listPlaceCompetence}
                                    placeholder={"Tempat"}
                                    defaultOptions={true}
                                    onChange={(e) => setPlaceCompetence(e.label)}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>Link Meeting</td>
                            <td>:</td>
                            <td><Input type="text" onChange={(e) => setLink(e.target.value)} /></td>
                        </tr>
                        <tr>
                            <td>Nama Asesi</td>
                            <td>:</td>
                            <td>
                                <Select
                                    options={listAsesi}
                                    placeholder={"Asesi"}
                                    defaultOptions={true}
                                    onChange={(e) => setAsesi(e.value)}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>Metode Asesmen</td>
                            <td>:</td>
                            <td>
                                <Select
                                    options={listMethod}
                                    placeholder={"Metode Asesmen"}
                                    defaultOptions={true}
                                    onChange={(e) => setMethod(e.value)}
                                />
                            </td>
                        </tr>
                    </Table>
                </div>
                <div style={{ width: '95%', background: 'transparent', display: 'flex', alignItems: 'flex-end', justifyContent: 'flex-end', marginTop: '10px' }}>
                    <Button onClick={checkValidation}>Submit</Button>
                </div>
            </div>
        );
    }

    const tableHistory = () => {
        const styles = {
            table: {
                width: '95%'
            }
        }
        return (
            <div style={styles.table}>
                {
                    isLoadingContent ?
                        <Table
                            bordered
                            striped>
                            <thead>
                                <tr style={{ background: '#B3936A', color: 'white' }}>
                                    <th style={{ width: '5%' }}>No</th>
                                    <th>Daftar Jadwal</th>
                                    <th style={{ width: '20%', textAlign: 'center' }}>Aksi</th>
                                </tr>
                            </thead>
                            <tr>
                                Loading...
                            </tr>
                        </Table>
                        :
                        <Table
                            bordered
                            striped>
                            <thead>
                                <tr style={{ background: '#B3936A', color: 'white' }}>
                                    <th style={{ width: '5%' }}>No</th>
                                    <th>Daftar Jadwal</th>
                                    <th style={{ width: '20%', textAlign: 'center' }}>Aksi</th>
                                </tr>
                            </thead>

                            <tbody>
                                {
                                    listSchedule.map((item, index) => (
                                        <tr>
                                            <td>{index + 1}</td>
                                            <td>
                                                <Row>
                                                    <Col xs="3">Tanggal:</Col>
                                                    <Col>{convertDate(item.date_schedule)}</Col>
                                                </Row>
                                                {/* <Row>
                                                    <Col xs="3">Nama Asesor:</Col>
                                                    <Col>{item.assesor.name}</Col>
                                                </Row> */}
                                                <Row>
                                                    <Col xs="3">Nama Asesi:</Col>
                                                    <Col>{item.asesi_registration.name}</Col>
                                                </Row>
                                            </td>
                                            <td style={{textAlign: 'center'}}>
                                                <Button color="primary" onClick={() => toggleUpdateOpen(item.id)} style={{margin: '0px auto'}}>
                                                    <AiFillEdit />
                                                </Button>
                                            </td>
                                            {/* <td style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}> */}
                                            {/* <Row> */}
                                            {/* <Col xs="3"> */}
                                            {/* <Button color="primary" onClick={() => toggleUpdateOpen(item.id)}>
                                                            <AiFillEdit />
                                                        </Button> */}
                                            {/* </Col> */}
                                            {/* <Col xs="1" />
                                                    <Col xs="3">
                                                        <Button color="success">
                                                            <AiFillFolderOpen />
                                                        </Button>
                                                    </Col> */}
                                            {/* </Row> */}
                                            {/* </td> */}
                                        </tr>
                                    ))
                                }
                            </tbody>
                        </Table>
                }
            </div>
        );
    }

    const showModalUpdate = () => {
        return (
            <Modal isOpen={modalUpdate} toggle={toggleUpdateClose} size="lg" style={{ maxWidth: '700px', width: '100%' }} backdrop="static">
                <ModalHeader toggle={toggleUpdateClose}>Update Jadwal</ModalHeader>
                {
                    loadingModalUpdate ?
                        <ModalBody style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                            <Spinner size="lg" color="blue" />
                            <div>Loading...</div>
                        </ModalBody>
                        :
                        savingModalUpdate ?
                            <ModalBody style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <Spinner size="lg" color="blue" />
                                <div>Saving...</div>
                            </ModalBody>
                            :
                            <ModalBody>
                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="date-schedule"
                                    >Hari/tanggal</Label>
                                    <Input type="date" name="date-schedule" id="date-schedule" value={date} onChange={(e) => setDate(e.target.value)} style={{ width: '85%' }} />
                                </FormGroup>
                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="time-schedule"
                                        disabled
                                    >Pukul</Label>
                                    <Input type="time" name="time-schedule" id="time-schedule" value={time} onChange={(e) => setTime(e.target.value)} style={{ width: '85%' }} />
                                </FormGroup>
                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '22%', textAlign: 'end', paddingRight: '10px' }}
                                        for="provinsi"
                                    >Tempat</Label>
                                    <div style={{ width: '78%' }}>
                                        <Select
                                            styles={{ width: '50%' }}
                                            options={listPlaceCompetence}
                                            placeholder={placeCompetenceFocus ? "Pilih Tempat" : placeCompetence != "" ? placeCompetence : "Ketik Tempat"}
                                            defaultOptions={true}
                                            onFocus={() => setPlaceCompetenceFocus(true)}
                                            onBlur={() => setPlaceCompetenceFocus(false)}
                                            onChange={(e) => setPlaceCompetenceId(e.value)}
                                        />
                                    </div>
                                </FormGroup>
                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="link"
                                        disabled
                                    >Link</Label>
                                    <Input type="text" name="link" id="asesi-name" value={link} onChange={(e) => setLink(e.target.value)} style={{ width: '85%' }} />
                                </FormGroup>
                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="asesi-name"
                                        disabled
                                    >Asesi</Label>
                                    <Input type="text" name="asesi-name" id="asesi-name" value={asesi} onChange={(e) => setAsesi(e.target.value)} style={{ width: '85%' }} disabled />
                                </FormGroup>
                                <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                    <Label
                                        style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                        for="asesor-name"
                                        disabled
                                    >Asesor</Label>
                                    <Input type="text" name="asesor-name" id="asesor-name" value={asesor} onChange={(e) => setAsesor(e.target.value)} style={{ width: '85%' }} disabled />
                                </FormGroup>
                            </ModalBody>

                }
                <ModalFooter>
                    <Button color="danger" onClick={updateSchedule}>Simpan</Button>{' '}
                    <Button color="warning" onClick={toggleUpdateClose}>Batal</Button>
                </ModalFooter>
            </Modal>

        );
    }
    if (isLoading) {
        return (
            <SpinnerLoading />
        );
    }

    return (
        <div style={styles.container}>
            {contentSchedule()}
            <br />
            {tableHistory()}
            <ModalSpinner statusOpen={isLoadingProgress} toggle={isLoadingProgress} />
            {showModalUpdate()}
        </div>
    );
}

const styles = {
    container: {
        // background: 'grey',
        width: '100%',
        // height: '100%',
        paddingLeft: '5%',
        display: 'flex',
        flexDirection: 'column',
        // overflow: 'scroll',
    },
    schedule: {
        // background: 'red',
        width: '100%',
        height: '50%'
    },
    formulir: {
        // background: 'blue',
        marginTop: '10px',
        width: '100%',
        height: '50%'
    },
    titleSchedule: {
        width: '30%',
        height: '40px',
        background: '#E9E9E9',
        marginTop: '5px'
    },
    contentShedule: {
        width: '95%',
        // height: '320px',
        background: '#E9E9E9',
        marginTop: '10px'
    },
    titleListFormulir: {
        width: '30%',
        height: '40px',
        background: '#E9E9E9',
        marginTop: '5px'
    },
    contentListFormulir: {
        width: '95%',
        // height: '180px',
        background: '#E9E9E9',
        marginTop: '10px'
    }
};