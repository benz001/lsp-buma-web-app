import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { AiOutlineCheck, AiOutlineDownload, AiOutlineStop, AiOutlineUpload } from 'react-icons/ai';
import { Button, Input, Label, Table } from 'reactstrap';
import { APIURL } from '../../helper/LinkAPI';
import AsyncSelect from 'react-select/async';
import Select from 'react-select';
import { convertDate, convertTime } from '../../utils/DateTimeConverter';
import ModalSpinner from '../../components/ModalSpinner';

export default function AssessorFormulirAssesment() {
    const [next, setNext] = useState(1);
    const [schedule, setSchedule] = useState(null);
    const [id, setId] = useState(null);
    const [name, setName] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [isLoadingProgress, setIsLoadingProgress] = useState(false);
    const [listDocument, setListDocument] = useState([]);
    const [listDone, setListDone] = useState([]);
    const [selectedFile, setSelectedFile] = useState(null);
    const [isFilePicked, setIsFilePicked] = useState(false);
    const [listAsesi, setListAsesi] = useState([]);

    useEffect(()=>{
        getAsesi();
    },[])

    const getAsesi = async () => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + "/assesment-schedule/pra-assesment/list", {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });
            const data = result.data.data;
            const tempArrayAsesi = [];
            console.log("get Asesi: " + JSON.stringify(data));

            if (result.status == 200) {
                const asesi = data;
                if (asesi.length) {
                    asesi.forEach((element) => {
                        tempArrayAsesi.push({
                            label: element.asesi_name,
                            value: element.asesi_registration_id
                        });
                    });
                } else {
                    tempArrayAsesi.push({
                        label: '',
                        value: ''
                    });
                }
                setListAsesi(tempArrayAsesi);
            } else {
                setListAsesi([]);
            }

        } catch (error) {
            // console.log("error: " + error);
            return [];
        }
    }

    const getScheduleAndDocument = async () => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        console.log("id: " + id);
        setIsLoading(true);
        try {
            const result = await axios.get(APIURL + `/assesment-schedule/assesment/${id}`, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const dataSchedule = result.data.data;
                const dataDocument = result.data.data.assesment_method.assesment_document;
                if (status === "success") {
                    console.log("data success: " + JSON.stringify(result.data.data));
                    setSchedule(dataSchedule);
                    setListDocument(dataDocument);
                    getDocument(dataDocument);
                    setTimeout(() => {
                        setIsLoading(false);
                        setNext(2);
                    }, 1000);
                }
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                setIsLoading(false);
            } else {
                alert("Error: " + JSON.stringify(error.response.data?.data?.error));
                setIsLoading(false);
            }
        }
    }

    const getDocument = (dataDocument) => {
        console.log("array");
        for (let index = 0; index < dataDocument.length; index++) {
            setListDone(oldArray => [...oldArray, false]);
            console.log("count: " + index);
        }
    }

    const handleChange = (index, e) => {
        console.log("array:" + listDone);
        listDone.splice(index, 1, e)
        setListDone([...listDone]);
        console.log(listDone);
    }

    const changeHandler = (event, index, document, type) => {
        try {
            console.log("index: " + index);
            console.log(event.target.files[0]);
            setSelectedFile(event.target.files[0]);
            setIsFilePicked(true);
            handleChange(index, true);
            setIsLoadingProgress(true);
            setTimeout(() => {
                if (document != null) {
                    if (type == "update") {
                        updateDocument(document, event.target.files[0]);
                    } else {
                        createDocument(document, event.target.files[0]);
                    }
                    console.log("document != null");
                } else {
                    console.log("document == null");
                }
            }, 1000);
        } catch (error) {
            console.log(null);
            setSelectedFile(null);
            setIsFilePicked(false);
            handleChange(index, false);
        }
    };

    const updateDocument = async (document, files) => {
        console.log('res update: ' + JSON.stringify(document));
        const token = localStorage.getItem("token");
        const formData = new FormData();
        formData.append("status", "wait");
        formData.append("assesment_method_document_id", document.assesment_method_document_id);
        formData.append("asesi_registration_id", document.asesi_registration_id);
        formData.append("file", files);
        try {
            const result = await axios.post(APIURL + `/assesment/answer/${document.id}?_method=PUT`, formData, {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "multipart/form-data",
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                // setIsLoadingProgress(false);
                if (status === "success") {
                    refreshDocument().then((value) => {
                        console.log("val: " + value);
                        if (value) {
                            alert("Update Success");
                            setIsLoadingProgress(false);
                        }
                    });
                }
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                setIsLoadingProgress(false);
            } else {
                alert("Error: " + JSON.stringify(error.response.data?.data?.error));
                setIsLoadingProgress(false);
            }
        }

    }


    const createDocument = async (document, files) => {
        console.log('res doc: ' + JSON.stringify(document));
        console.log('res files: ' + JSON.stringify(files));
        const token = localStorage.getItem("token");
        const formData = new FormData();
        formData.append("status", "wait");
        formData.append("assesment_method_document_id", document.id);
        formData.append("asesi_registration_id", id);
        formData.append("file", files);
        try {
            const result = await axios.post(APIURL + "/assesment/answer", formData, {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "multipart/form-data",
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                setIsLoadingProgress(false);
                if (status === "success") {
                    refreshDocument().then((value) => {
                        console.log("val: " + value);
                        if (value) {
                            alert("Create Success");
                            setIsLoadingProgress(false);
                        } else {
                            alert("Create Success, but not refreshed");
                            setIsLoadingProgress(false);
                        }
                    });
                }
            } else {
                alert("Create Bad");
                setIsLoadingProgress(false);

            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                setIsLoadingProgress(false);
            } else {
                alert("Error: " + JSON.stringify(error.response.data?.data?.error));
                setIsLoadingProgress(false);
            }
        }
    }

    const setIdAndName = (id, name) => {
        setId(id);
        setName(name);
    }

    const refreshDocument = async () => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + `/assesment-schedule/assesment/${id}`, {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const dataSchedule = result.data.data;
                const dataDocument = result.data.data.assesment_method.assesment_document;
                if (status === "success") {
                    console.log("data schedule success: " + JSON.stringify(dataSchedule));
                    console.log("data document success: " + JSON.stringify(dataDocument));
                    setListDocument(dataDocument);
                } else {
                    console.log("data schedule unsuccess: " + JSON.stringify(dataSchedule));
                    console.log("data document unsuccess: " + JSON.stringify(dataDocument));
                }

                return true;
            } else {
                console.log("bad request");
                return false;
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
            } else {
                alert("Error: " + JSON.stringify(error.response.data?.data?.error));
            }
            return false;
        }
    }

    const contentNameAsesi = () => {
        return (
            <div style={styles.schedule}>
                <div style={styles.titleSchedule}>
                    <Label style={{ fontWeight: 'bold', fontSize: '14px', display: 'flex', justifyContent: 'center', paddingTop: '5px' }}>Asesi</Label>
                </div>
                <div style={styles.contentShedule}>
                    <Table>
                        <tr>
                            <td>Nama Asesi</td>
                            <td>:</td>
                            <td>
                                <Select
                                    options={listAsesi}
                                    placeholder={"Nama Asesi"}
                                    defaultOptions={true}
                                    onChange={
                                        (e) => setIdAndName(e.value, e.label)
                                    }
                                    style={{ background: 'white' }}
                                />
                            </td>
                        </tr>
                    </Table>
                </div>
                <div style={{ width: '95%', background: 'transparent', display: 'flex', alignItems: 'flex-end', justifyContent: 'flex-end', marginTop: '10px' }}>
                    <Button onClick={getScheduleAndDocument}>Selanjutnya</Button>
                </div>
            </div>
        );
    }


    const contentFormulir = () => {
        return (
            <div style={styles.formulir}>
                <div style={styles.titleListFormulir}>
                    <Label style={{ fontWeight: 'bold', fontSize: '14px', display: 'flex', justifyContent: 'center', paddingTop: '5px' }}>Formulir({name})</Label>
                </div>
                <div style={styles.contentListFormulir}>
                    <Table>
                        {

                            listDocument.map((item, index) => (
                                <tr>
                                    <td style={{ width: '5%' }}>{index + 1}</td>
                                    <td style={{ color: 'blue', width: '35%' }}>{item.name}</td>
                                    <td>
                                        <Button color="success" onClick={() => { item.document_answer != null ? window.open(`https://test.devinfolspbuma.online/public${item.document_answer.file}`, '_blank') : window.open(`https://test.devinfolspbuma.online/public${item.file}`, '_blank') }}>
                                            <AiOutlineDownload /> Download
                                        </Button>
                                    </td>
                                    <td>
                                        <div>
                                            <label for={`file-input-${index}`} style={{
                                                backgroundColor: 'blue',
                                                color: 'white',
                                                padding: '0.5rem',
                                                borderRadius: '0.3rem',
                                                width: '60%',
                                                textAlign: 'center'
                                            }}>
                                                <AiOutlineUpload color="white" />
                                                Upload
                                                <Input id={`file-input-${index}`} type="file" onChange={(e) => { item.document_answer != null ? changeHandler(e, index, item.document_answer, "update") : changeHandler(e, index, item, "create") }} accept=".xlsx,.xls,.doc, .docx,.ppt, .pptx,.txt,.pdf" style={{ display: 'none' }} />
                                            </label>
                                        </div>
                                    </td>
                                    <td style={{ width: '10%' }}>
                                        {
                                            listDone[index] ? <AiOutlineCheck color={"green"} size={20} /> : <AiOutlineStop color={"red"} size={20} />
                                        }
                                    </td>
                                </tr>))
                        }
                    </Table>
                </div>
                <div style={{ width: '95%', background: 'transparent' }}>
                    <Button style={{ float: 'left' }} onClick={() => setNext(1)}>Kembali</Button>
                </div>
            </div>
        );
    }
    return (
        <div style={styles.container}>
            {
                next === 1 ? contentNameAsesi() : contentFormulir()
            }
            <ModalSpinner statusOpen={isLoading} toggle={isLoading} />
            <ModalSpinner statusOpen={isLoadingProgress} toggle={isLoadingProgress} />
        </div>
    );
}

const styles = {
    container: {
        width: '100%',
        height: '100%',
        paddingLeft: '5%',

    },
    schedule: {
        width: '100%',
        height: '50%'
    },
    formulir: {
        // background: 'blue',
        width: '100%',
        height: '50%'
    },
    titleSchedule: {
        width: '30%',
        height: '40px',
        background: '#E9E9E9',
        marginTop: '5px'
    },
    contentShedule: {
        width: '95%',
        // height: '250px',
        background: '#E9E9E9',
        marginTop: '10px'
    },
    titleListFormulir: {
        width: '30%',
        height: '40px',
        background: '#E9E9E9',
        marginTop: '5px'
    },
    contentListFormulir: {
        width: '95%',
        // height: '200px',
        background: '#E9E9E9',
        marginTop: '10px'
    }
};