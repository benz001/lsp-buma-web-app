import React from 'react';
import { Form, FormGroup, Input, Label, Table } from 'reactstrap';

export default function FeedbackAndAssessmentNotesPage() {
    return (
        <div style={styles.container}>
            <div style={styles.formInput}>
                <div style={styles.title}>
                    <h5>Umpan Balik dan Catatan Asesmen</h5>
                </div>
                <div style={styles.input}>
                    <Form>
                        <FormGroup>
                            <Label for="assessor-name"><i>Nama Asesor</i></Label>
                            <Input type="text" name="assessor-name" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="accession-name"><i>Nama Asesi</i></Label>
                            <Input type="text" name="accession-name" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="date"></Label>
                            <Input type="date" name="date" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="time">Waktu</Label>
                            <Input type="time" name="time" />
                        </FormGroup>
                        <Label style={{ fontWeight: 'bold' }}>
                            <i>Umpan balik dari Asesi (diisi oleh Asesi setelah pengambilan keputusan):</i>
                        </Label>
                        <Table bordered style={{border:'1px solid black'}}>
                            {/* <th>
                                <td>Komponen</td>
                                <td>Hasil</td>
                                <td>Catatan/Komentar Asesi</td>
                            </th>
                            <tr>
                                <td>Saya mendapatkan penjelasan yang cukup memadai mengenai proses asesmen/uji kompetensi </td>
                                <td>
                                    <Label>
                                        <Input type="checkbox" />
                                    </Label>
                                </td>
                                <td>
                                    <Label>
                                        <Input type="checkbox" />
                                    </Label>
                                </td>
                                <td>Catatan/Komentar Asesi</td>
                            </tr> */}
                            <tr>
                                <th style={{width: '5%'}}>No</th>
                                <th style={{width: '40%'}}>Komponen</th>
                                <th style={{width: '5%'}}>Hasil</th>
                                <th style={{width: '5%'}}>Hasil</th>
                                <th>Catatan/Komentar Asesi</th>
                            </tr>
                            {/* <tr>
                                <td>Ya</td>
                                <td>Tidak</td>
                                <td>Ya</td>
                                <td>Tidak</td>
                            </tr> */}
                            <tr>
                                <td>1</td>
                                <td>Saya mendapatkan penjelasan yang cukup memadai mengenai proses asesmen/uji kompetensi </td>
                                <td>
                                    <Input type="checkbox" />
                                </td>
                                <td><Input type="checkbox" /></td>
                                <td><Input type="textarea" /></td>
                            </tr>
                        </Table>
                    </Form>
                </div>
            </div>
        </div>
    );
}

const styles = {
    container: {
        width: '100%',
        height: '100%',
        // backgroundColor: 'lightgrey',
    },
    formInput: {
        // backgroundColor: '#E9E9E9',
        paddingRight: '1%',
        paddingLeft: '1%',
        paddingBottom: '1%',
    }
};