import React from 'react';
import { Form, FormGroup, Input, Label } from 'reactstrap';

export default function CompetencyAssessmentRecordPage() {
    return (
        <div style={styles.container}>
            <div style={styles.formInput}>
                <div style={styles.title}>
                    <h5>Formulir Rekaman Asesmen Kompetensi</h5>
                </div>
                <div style={styles.input}>
                    <Form>
                        <FormGroup>
                            <Label for="accession-name"><i>Nama Asesi</i></Label>
                            <Input type="text" name="accession-name"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="assessor-name"><i>Nama Asesor</i></Label>
                            <Input type="text" name="accession-name"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="certification-scheme"><i>Skema Sertifikasi</i></Label>
                            <Input type="select" name="certification-scheme">
                                <option>Pilih Skema Sertifikasi</option>
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label for="certification-scheme"><i>Unit Kompetensi</i></Label>
                            <Input type="textarea" name="certification-scheme" style={{height: '200px', resize: 'none'}}/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="start-date"><i>Tanggal mulainya asesmen</i></Label>
                            <Input type="date" name="start-date"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="end-date"><i>Tanggal berakhirnya asesmen</i></Label>
                            <Input type="date" name="start-date"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="evidence-collected"><i>Bukti yang diperoleh</i></Label>
                            <br />
                            <div style={{paddingLeft: '20px'}}>
                                <Label>
                                    <Input type="checkbox" />{' '}
                                    Unit Kompetensi
                                </Label>
                                <br />
                                <Label>
                                    <Input type="checkbox" />{' '}
                                   Observasi Demonstrasi
                                </Label>
                                <br />
                                <Label>
                                    <Input type="checkbox" />{' '}
                                    Pernyataan Pihak Ketiga Pertanyaan Wawancara
                                </Label>
                                <br />
                                <Label>
                                    <Input type="checkbox" />{' '}
                                    Pertanyaan lisan 
                                </Label>
                                <br />
                                <Label>
                                    <Input type="checkbox" />{' '}
                                    Proyek kerja
                                </Label>
                                <br />
                                <Label>
                                    <Input type="checkbox" />{' '}
                                    Lainnya
                                </Label>
                            </div>
                        </FormGroup>
                        <FormGroup>
                            <Label for="recommended-assessment-results"><i>Rekomendasi hasil asesmen</i></Label>
                            <Input type="select" name="recommended-assessment-results">
                                <option>Pilih Rekomendasi Hasil Asesmen</option>
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label for="follow-up"><i>Tindak lanjut yang dibutuhkan </i></Label>
                            <Input type="textarea" name="follow-up" style={{height: '200px', resize: 'none'}}/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="follow-up"><i>Komentar/ Observasi oleh asesor  </i></Label>
                            <Input type="textarea" name="comment" style={{height: '200px', resize: 'none'}}/>
                        </FormGroup>
                    </Form>
                </div>
            </div>
        </div>
    );
}

const styles = {
    container: {
        width: '100%',
        height: '100%',
        backgroundColor: 'lightgrey',
    },
    formInput: {
        backgroundColor: '#E9E9E9',
        paddingRight: '1%',
        paddingLeft: '1%',
        paddingBottom: '1%',
    }
};