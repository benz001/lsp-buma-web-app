import React from 'react';
import { Form, FormGroup, Input, Label } from 'reactstrap';

export default function AssessmentApprovalConfidentialityPage() {
    return (
        <div style={styles.container}>
            <div style={styles.formInput}>
                <div style={styles.title}>
                    <h5>Persetujuan Asesmen dan Kerahasiaan</h5>
                </div>
                <div style={styles.input}>
                    <Form>
                        <Label style={{ fontWeight: 'bold' }}>
                            <i>Skema Sertifikasi</i>
                        </Label>
                        <FormGroup>
                            <Label for="title"><i>Judul</i></Label>
                            <Input type="text" id="title" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="number"><i>Nomor</i></Label>
                            <Input type="text" id="number" />
                        </FormGroup>
                    </Form>
                    <hr />
                    <Form>
                        <FormGroup>
                            <Label for="tuk"><i>TUK</i></Label>
                            <Input type="select" name="tuk">
                                <option>Pilih TUK</option>
                                <option>Sewaktu</option>
                                <option>Tempat Kerja</option>
                                <option>Mandiri</option>
                            </Input>
                        </FormGroup>
                        <br />
                        <FormGroup>
                            <Label for="assessor-name"><i>Nama Asesor</i></Label>
                            <Input type="text" name="assessor-name" />
                        </FormGroup>
                        <br />
                        <FormGroup>
                            <Label for="accession-name"><i>Nama Asesi</i></Label>
                            <Input type="text" name="accession-name" />
                        </FormGroup>
                        <br />
                        <FormGroup>
                            <Label for="evidence-collected"><i>Bukti yang akan dikumpulkan</i></Label>
                            <br />
                            <div style={{paddingLeft: '20px'}}>
                                <Label>
                                    <Input type="checkbox" />{' '}
                                    TL: Verifikasi Portofolio
                                </Label>
                                <br />
                                <Label>
                                    <Input type="checkbox" />{' '}
                                    L: Observasi Langsung
                                </Label>
                                <br />
                                <Label>
                                    <Input type="checkbox" />{' '}
                                    TL: Verifikasi Portofolio
                                </Label>
                                <br />
                                <Label>
                                    <Input type="checkbox" />{' '}
                                    T: Hasil Tes Tulis
                                </Label>
                                <br />
                                <Label>
                                    <Input type="checkbox" />{' '}
                                    T: Hasil Tes Lisan
                                </Label>
                                <br />
                                <Label>
                                    <Input type="checkbox" />{' '}
                                    T: Hasil Tes Wawancara
                                </Label>
                            </div>
                        </FormGroup>
                        <FormGroup>
                            <Label style={{fontWeight: 'bold'}}>
                                <i>Pelaksanaan asesmen disepakati pada</i>
                            </Label>
                        </FormGroup>
                        <FormGroup>
                            <Label for="date">Hari/Tanggal</Label>
                            <Input type="date" name="date"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="time">Waktu</Label>
                            <Input type="time" name="time"/>
                        </FormGroup>
                    </Form>
                </div>
            </div>
        </div>
    );
}

const styles = {
    container: {
        width: '100%',
        height: '100%',
        backgroundColor: 'lightgrey',
    },
    formInput: {
        backgroundColor: '#E9E9E9',
        paddingRight: '1%',
        paddingLeft: '1%',
        paddingBottom: '1%',
    }
};