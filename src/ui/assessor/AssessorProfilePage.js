import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Button, Form, FormGroup, Input, Label, Spinner, Table } from 'reactstrap';
import SpinnerLoading from '../../components/SpinnerLoading';
import { APIURL } from '../../helper/LinkAPI';
import AsyncSelect from 'react-select/async';
import Select from 'react-select';
import ModalSpinner from '../../components/ModalSpinner';

export default function AssessorProfilePage() {

    const [chooseMenu, setChooseMenu] = useState('formProfile');

    const [isLoading, setIsLoading] = useState(true);

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [nik, setNIK] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [gender, setGender] = useState("");
    const [pob, setPob] = useState("");
    const [dob, setDob] = useState("");
    const [address, setAddress] = useState("");
    const [province, setProvince] = useState("");
    const [city, setCity] = useState("");
    const [provinceFocus, setProvinceFocus] = useState(false);
    const [districtFocus, setDistrictFocus] = useState(false);
    const [listProvince, setListProvince] = useState([]);
    const [listDistrict, setListDistrict] = useState([]);
    const [postalCode, setPostalCode] = useState("");
    const [education, setEducation] = useState("");
    const [profession, setProfession] = useState("");
    const [photo, setPhoto] = useState(null);
    const [selectedFilePhoto, setSelectedFilePhoto] = useState(null);
    const [isFilePickedPhoto, setIsFilePickedPhoto] = useState(false);

    const [imgSrc, setImgSrc] = useState(null);
    const [status, setStatus] = useState(null);

    const [isLoadingProgress, setIsLoadingProgress] = useState(false);



    useEffect(() => {
        getProfile().then((v)=>{
            getProvince();
        });
    }, []);


    const checkStatus = (status) => {
        // console.log(typeof status);
        switch (status) {
            case "0":
                console.log("Dalam Proses");
                return "Dalam Proses";
            case "1":
                console.log("Validasi Pendaftaran");
                return "Validasi Pendaftaran";
            case "2":
                console.log("Pra Asesmen");
                return "Pra Asesmen";
            case "3":
                console.log("Asesmen");
                return "Asesmen";
            case "4":
                console.log("Selesai");
                return "Selesai";
            default:
                console.log("Tidak di Ketahui");
                return "Tidak di Ketahui";
        }
    }

    const getProfile = async () => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);
        try {
            const result = await axios.get(APIURL + "/user/me", {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                if (status === "success") {
                    console.log("data success: " + JSON.stringify(data));
                    console.log("track: " + data?.asesi);
                    setTimeout(() => {
                        setIsLoading(false)
                    }, 1000);
                    data.user.name != null ? setName(data.user.name) : setName("");
                    data.user.email != null ? setEmail(data.user.email) : setEmail("");
                    data.user_profile.nik != null ? setNIK(data.user_profile.nik) : setNIK("");
                    data.user_profile.phone_number != null ? setPhoneNumber(data.user_profile.phone_number) : setPhoneNumber("");
                    data.user_profile.gender != null ? setGender(data.user_profile.gender) : setGender("");
                    data.user_profile.place_of_birth != null ? setPob(data.user_profile.place_of_birth) : setPob("");
                    data.user_profile.date_of_birth != null ? setDob(data.user_profile.date_of_birth) : setDob("");
                    data.user_profile.address != null ? setAddress(data.user_profile.address) : setAddress("");
                    data.user_profile.province != null ? setProvince(data.user_profile.province) : setProvince("");
                    data.user_profile.city != null ? setCity(data.user_profile.city) : setCity("");
                    data.user_profile.postal_code != null ? setPostalCode(data.user_profile.postal_code) : setPostalCode("");
                    data.user_profile.education != null ? setEducation(data.user_profile.education) : setEducation("");
                    data.user_profile.profession != null ? setProfession(data.user_profile.profession) : setProfession("");
                    data.user_profile.photo != null ? setPhoto(data.user_profile.photo) : setPhoto(null);

                    if (data.asesi != null) {
                        setStatus(checkStatus(data.asesi.track));
                    } else {
                        setStatus("-");
                    }

                }
                return true;
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                setIsLoading(false);
            } else {
                alert("Error: " + JSON.stringify(error.response.data?.data?.error));
                setIsLoading(false);
            }

            return false;
        }
    }

    const updateProfile = async () => {
        const token = localStorage.getItem("token");
        console.log("token: " + token);

        console.log(selectedFilePhoto != null);

        const formData = new FormData();
        formData.append("nik", nik);
        formData.append("phone_number", phoneNumber);
        formData.append("gender", gender);
        formData.append("place_of_birth", pob);
        formData.append("date_of_birth", dob);
        formData.append("address", address);
        formData.append("province", province);
        formData.append("city", city);
        formData.append("postal_code", postalCode);
        formData.append("education", education);
        formData.append("profession", profession);
        formData.append("photo", selectedFilePhoto);

        const header = selectedFilePhoto != null ? {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "multipart/form-data"
        } : {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "application/json"
        };

        const data = selectedFilePhoto != null ? formData : {
            "nik": nik,
            "phone_number": phoneNumber,
            "gender": gender,
            "place_of_birth": pob,
            "date_of_birth": dob,
            "address": address,
            "province": province,
            "city": city,
            "postal_code": postalCode,
            "education": education,
            "profession": profession
        }

        console.log("header: " + JSON.stringify(header));
        console.log("data: " + JSON.stringify(data));
        setIsLoadingProgress(true);
        try {
            const result = await axios.post(APIURL + "/user/update-profile?_method=put", data, {
                headers: header
            });

            if (result.status === 200) {
                const meta = result.data.meta;
                const status = meta.status;
                const data = result.data.data;
                if (status === "success") {
                    alert("Profil berhasil di update");
                    setIsLoadingProgress(false);
                }
            }
        } catch (error) {
            if (error.message === "Network Error") {
                alert("Error: " + error.message);
                setIsLoadingProgress(false);
            } else {
                alert("Error: " + JSON.stringify(error.response.data.data.error));
                setIsLoadingProgress(false);
            }
        }

    }

    const getProvince = async () => {
        try {
            const result = await axios.get("https://ibnux.github.io/data-indonesia/provinsi.json");
            const data = result.data;
            const province = data;
            const tempArrayProvince = [];
            console.log("get Province");
            if (province.length) {
                province.forEach((element) => {
                    tempArrayProvince.push({
                        label: element.nama,
                        value: `${element.id}`
                    });
                });
            } else {
                tempArrayProvince.push({
                    label: province.nama,
                    value: `${province.id}`
                });
            }
            setListProvince(tempArrayProvince);
        } catch (error) {
            // console.log("error: " + error);
            setListProvince([]);
        }
    }

    const getDistrict = async (value) => {
        // console.log(selectDistrict !== "");
        try {
            const result = await axios.get(`https://ibnux.github.io/data-indonesia/kabupaten/${value}.json`);
            const data = result.data;
            const district = data;
            const tempArrayDistrict = [];

            console.log("getDistrict");

            if (district.length) {
                district.forEach((element) => {
                    tempArrayDistrict.push({
                        label: element.nama,
                        value: `${element.id}`
                    });
                });
            } else {
                tempArrayDistrict.push({
                    label: district.nama,
                    value: `${district.id}`
                });
            }

            setListDistrict(tempArrayDistrict);
        } catch (error) {
            // console.log("error: " + error);
            setListDistrict([]);
        }


    }

    const setterProvinceAndDistrict = (value, label) => {
        getDistrict(value);
        console.log(label);
        setProvince(label);

    }

    const setterDistrict = (value, label) => {
        console.log(label);
        setCity(label);

    }

    const savePhoto = (event) => {
        const e = event.target.files[0];
        let reader = new FileReader();
        let url = reader.readAsDataURL(e);
        try {
            console.log(event.target.files[0]);
            setSelectedFilePhoto(event.target.files[0]);
            // setImgSrc(URL.createObjectURL(event.target.files[0]));
            reader.onloadend = function () {
                setImgSrc(reader.result);
            }
        } catch (error) {
            console.log(null);
            setSelectedFilePhoto(null);
            setImgSrc(null);
        }

    };


    const formProfile = () => {
        return (
            <div style={styles.formEdit}>
                <div style={{ width: '100%', display: 'flex', flexDirection: 'column' }}>
                    {
                        photo != null ?
                            <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                <Label
                                    style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                    for="photo"
                                ></Label>
                                <div style={{ width: '150px', height: '150px', backgroundColor: '#E9E9E9' }} >

                                    {
                                        imgSrc != null ? <img src={imgSrc} width="150" height="150" /> : <img src={photo} width="150" height="150" />
                                    }

                                </div>
                            </FormGroup> :
                            <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                                <Label
                                    style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                    for="photo"
                                ></Label>
                                <div style={{ width: '150px', height: '150px', backgroundColor: '#E9E9E9' }} >
                                    {
                                        imgSrc != null ? <img src={imgSrc} width="150" height="150" /> : null
                                    }
                                </div>
                            </FormGroup>
                    }

                    <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                        <Label
                            style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                            for="fullname"
                        ></Label>
                        <input type="file" name="image" id="image" accept="image/*" onChange={savePhoto} />
                    </FormGroup>
                </div>
                <Form style={{ marginTop: '10px' }}>
                    <div style={{ display: 'flex', flexDirection: 'column' }}>
                        <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                            <Label
                                style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                for="fullname"
                            >Nama Lengkap</Label>
                            <Input type="text" name="fullname" id="fullname" value={name} onChange={(e) => setName(e.target.value)} style={{ width: '85%' }} disabled />
                        </FormGroup>


                        <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                            <Label
                                style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                for="email"
                                disabled
                            >Email</Label>
                            <Input type="email" name="email" id="email" value={email} onChange={(e) => setEmail(e.target.value)} style={{ width: '85%' }} disabled />
                        </FormGroup>

                        <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                            <Label
                                style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                for="nik"
                            >NIK(KTP)</Label>
                            <Input type="number" name="nik" id="nik" value={nik} onChange={(e) => setNIK(e.target.value)} style={{ width: '85%' }} />
                        </FormGroup>


                        <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                            <Label
                                style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                for="telephone"
                            >No.Handphone</Label>
                            <Input type="number" name="telephone" id="telephone" value={phoneNumber} onChange={(e) => setPhoneNumber(e.target.value)} style={{ width: '85%' }} />
                        </FormGroup>

                        <FormGroup for="gender" style={{ display: 'flex', flexDirection: 'row' }}>
                            <Label style={{ width: '23%', textAlign: 'end', paddingRight: '10px' }}>Jenis Kelamin</Label>
                            <div style={{ width: '77%' }}>
                                <Input
                                    type="select"
                                    name="gender"
                                    id="gender"
                                    value={gender}
                                    onChange={(e) => setGender(e.target.value)}>
                                    <option value="">Pilih Jenis Kelamin</option>
                                    <option value="male">Pria</option>
                                    <option value="female">Wanita</option>
                                </Input>
                            </div>
                        </FormGroup>

                        <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                            <Label for="pob" style={{ width: '23%', textAlign: 'end', paddingRight: '10px' }}>Tempat Lahir</Label>
                            <div style={{ width: '77%' }}>
                                <Input type="text" id="pob" value={pob} onChange={(e) => setPob(e.target.value)} />
                            </div>
                        </FormGroup>

                        <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                            <Label for="dob" style={{ width: '23%', textAlign: 'end', paddingRight: '10px' }}>Tanggal Lahir</Label>
                            <div style={{ width: '77%' }}>
                                <Input type="date" name="dob" id="dob" value={dob} onChange={(e) => setDob(e.target.value)} />
                            </div>
                        </FormGroup>

                        <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                            <Label
                                style={{ width: '25%', textAlign: 'end', paddingRight: '10px' }}
                                for="address"
                            >Alamat</Label>
                            <Input type="text" name="address" id="address" value={address} onChange={(e) => setAddress(e.target.value)} style={{ width: '85%' }} />
                        </FormGroup>

                        <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                            <Label
                                style={{ width: '22%', textAlign: 'end', paddingRight: '10px' }}
                                for="provinsi"
                            >Provinsi</Label>
                            <div style={{ width: '78%' }}>
                                <Select
                                    styles={{ width: '50%' }}
                                    options={listProvince}
                                    placeholder={provinceFocus ? "Pilih Provinsi" : province != "" ? province : "Ketik Provinsi"}
                                    onFocus={() => setProvinceFocus(true)}
                                    onBlur={() => setProvinceFocus(false)}
                                    defaultOptions={true}
                                    onChange={(e) => setterProvinceAndDistrict(e.value, e.label)}
                                />
                            </div>

                        </FormGroup>

                        <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                            <Label
                                style={{ width: '22%', textAlign: 'end', paddingRight: '10px' }}
                                for="kabupaten"
                            >Kabupaten</Label>
                            <div style={{ width: '78%' }}>
                                <Select
                                    options={listDistrict}
                                    placeholder={districtFocus ? "Pilih Kabupaten" : city != "" ? city : "Ketik Kabupaten"}
                                    onFocus={() => setDistrictFocus(true)}
                                    onBlur={() => setDistrictFocus(false)}
                                    defaultOptions={true}
                                    onChange={(e) => setterDistrict(e.value, e.label)}
                                />
                            </div>
                        </FormGroup>

                        <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                            <Label for="postal-code" style={{ width: '22%', textAlign: 'end', paddingRight: '10px' }}>Kode Pos</Label>
                            <div style={{ width: '78%' }}>
                                <Input type="text" id="postal-code" value={postalCode} onChange={(e) => setPostalCode(e.target.value)} />
                            </div>
                        </FormGroup>

                        <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                            <Label for="education" style={{ width: '22%', textAlign: 'end', paddingRight: '10px' }}>Pendidikan</Label>
                            <div style={{ width: '78%' }}>
                                <Input type="select" name="education" id="education" value={education} onChange={education} onChange={(e) => setEducation(e.target.value)}>
                                    <option value="">Pilih Pendidikan</option>
                                    <option value="elementary-school">SD/MI/Sederajat</option>
                                    <option value="junior-high-school">SMP/Mts/Sederajat</option>
                                    <option value="senior-high-school">SMA/SMK/Sederajat</option>
                                    <option value="strata-1">S1</option>
                                    <option value="strata-2">S2</option>
                                    <option value="strata-3">S3</option>
                                </Input>
                            </div>
                        </FormGroup>

                        <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                            <Label for="work" style={{ width: '22%', textAlign: 'end', paddingRight: '10px' }}>Pekerjaan</Label>
                            <div style={{ width: '78%' }}>
                                <Input type="select" name="work" id="work" value={profession} onChange={(e) => setProfession(e.target.value)}>
                                    <option value="">Pilih Pekerjaan</option>
                                    <option value="private-employees">Pegawai Swasta</option>
                                    <option value="goverment-employees">Pegawai Negeri</option>
                                    <option value="entrepreneur">Wiraswasta</option>
                                    <option value="student">Mahasiswa/Pelajar</option>
                                </Input>
                            </div>
                        </FormGroup>

                        <FormGroup style={{ display: 'flex', flexDirection: 'row' }}>
                            <Label
                                style={{ width: '22%', textAlign: 'end', paddingRight: '10px' }}
                                for="simpan"
                            ></Label>
                            <Button color="success" onClick={updateProfile} style={{ width: '15%' }}>
                                Simpan
                            </Button>
                        </FormGroup>
                    </div>
                </Form>
            </div>
        );
    }

    const assessmentHistory = () => {
        return (
            <div style={styles.schedule}>
                <div style={styles.titleSchedule}>
                    <Label style={{ fontWeight: 'bold', fontSize: '14px', display: 'flex', justifyContent: 'center', paddingTop: '5px' }}>Asesmen</Label>
                </div>
                <div style={styles.contentShedule}>
                    <Table>
                        <tr>
                            <td style={{ width: '20%' }}>Hari/tanggal</td>
                            <td style={{ width: '5%' }}>:</td>
                            <td>Senin, 16 Mei 2021</td>
                        </tr>
                        <tr>
                            <td>Jenis Asesmen</td>
                            <td>:</td>
                            <td>Asesmen LSP BUMA</td>
                        </tr>
                        <tr>
                            <td >Nama Skema</td>
                            <td >:</td>
                            <td>Operator Alat Berat Unit Buildozer </td>
                        </tr>
                        <tr>
                            <td>TUK</td>
                            <td>:</td>
                            <td>BUMA Jobsite Adaro</td>
                        </tr>
                        <tr>
                            <td>Metode Asesmen</td>
                            <td>:</td>
                            <td>Observasi Langsung</td>
                        </tr>
                        <tr>
                            <td>Hasil Asesmen</td>
                            <td>:</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <td>Cetak Sertifikat</td>
                            <td>:</td>
                            <td>-</td>
                        </tr>
                    </Table>
                </div>
            </div>
        );
    }

    if (isLoading) {
        return (
            <SpinnerLoading />
        );
    }
    return (
        <div style={styles.container}>
            <div style={styles.editProfile}>
                <ul style={styles.editProfileUl}>
                    <li>
                        {
                            photo != null ? <div style={{ width: '120px', height: '120px', backgroundColor: '#E9E9E9' }} >
                                {
                                    imgSrc != null ? <img src={imgSrc} width="120" height="120" /> : <img src={photo} width="120" height="120" />
                                }
                            </div> :
                                <div style={{ width: '120px', height: '120px', backgroundColor: '#E9E9E9' }} >
                                    {
                                        imgSrc != null ? <img src={imgSrc} width="120" height="120" /> : null
                                    }
                                </div>
                        }





                    </li>
                    <li>
                        <Link onClick={() => setChooseMenu('formProfile')}>
                            Edit Profile
                        </Link>
                    </li>
                    <li>
                        <Link onClick={() => setChooseMenu('assessmentHistory')}>
                            Riwayat Asesmen
                        </Link>
                    </li>
                </ul>
            </div>
            {
                chooseMenu === 'formProfile' ? formProfile() : chooseMenu === 'assessmentHistory' ? assessmentHistory() : null
            }
            <ModalSpinner statusOpen={isLoadingProgress} toggle={isLoadingProgress} />

        </div>
    );
}


const styles = {
    container: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'row',
        // overflow: 'scroll',
    },
    editProfile: {
        width: '20%',
        height: '100%',
        borderRightStyle: 'solid',
        borderWidth: '1px',
        borderColor: '#E9E9E9'
    },
    editProfileUl: {
        listStyleType: 'none',
        marginTop: '15px',
        marginLeft: '15px',
        padding: '0px'
    },
    formEdit: {
        width: '80%',
        height: '100%',
        display: 'flex',
        alignItmes: 'center',
        flexDirection: 'column',
        paddingRight: '5%',
        paddingLeft: '5%',
        marginTop: '15px'
    },
    formPhotoAssessor: {
        width: '200px',
        height: '160px',
        // backgroundColor: '#E9E9E9', 
        display: 'flex',
        flexDirection: 'row',
    },
    labelPhotoAssessor: {
        width: '35%',
        height: '100%',
        fontSize: '12px',
        // backgroundColor: 'red' 
    },
    schedule: {
        width: '100%',
        height: '50%',
        marginLeft: '10px'
    },
    titleSchedule: {
        width: '30%',
        height: '40px',
        background: '#E9E9E9',
        marginTop: '5px'
    },
    contentShedule: {
        width: '95%',
        background: '#E9E9E9',
        marginTop: '10px'
    },
}