import React, { useEffect, useState } from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect,
    useHistory,
    useRouteMatch
} from "react-router-dom";
import logo from '../../assets/image/logo.PNG'
import profile from '../../assets/image/ic_profil.png';
import pra_asesmen from '../../assets/image/ic_pra_asesmen.png';
import asesmen from '../../assets/image/ic_asesmen.png';
import AssessorProfilePage from "./AssessorProfilePage";
import AssessorPraAssesment from "./AssessorPraAssesment";
import AssessorAssesment from "./AssessorAssessment";
import { AiFillCopy, AiOutlineImport } from "react-icons/ai";
import ModalAlert from "../../components/ModalAlert";
import AssessorFormulirAssessment from "./AssessorFormulirAssessment";


export default function SidebarBarNavigationAssessor() {
    let { path, url } = useRouteMatch();
    let history = useHistory();

    const routes = [
        {
            path: `${url}`,
            exact: true,
            sidebar: () => <div style={{ width: '100%', height: '100%', display: 'flex', justifyContent: 'center' }}> <h3 style={{ marginTop: '25%' }}>Selamat Datang, Silahkan Pilih Menu yang Tersedia</h3></div>,
            main: () => <div style={{ width: '100%', height: '100%', display: 'flex', justifyContent: 'center' }}> <h3 style={{ marginTop: '25%' }}>Selamat Datang, Silahkan Pilih Menu yang Tersedia</h3></div>
        },
        {
            path: `${url}/profile-asesor`,
            exact: true,
            sidebar: () => <AssessorProfilePage />,
            main: () => <AssessorProfilePage />
        },
        {
            path: `${url}/pra-asesmen-asesor`,
            sidebar: () => <AssessorPraAssesment />,
            main: () => <AssessorPraAssesment />
        },
        {
            path: `${url}/asesmen-asesor`,
            sidebar: () => <AssessorAssesment />,
            main: () => <AssessorAssesment />
        },
        {
            path: `${url}/asesmen-formulir`,
            sidebar: () => <AssessorFormulirAssessment />,
            main: () => <AssessorFormulirAssessment />
        }
    ];

    const [activeIndicator, setActiveIndicator] = useState(0);
    const [showModal, setShowModal] = useState(false);
    const [hideWelcome, setHideWelcome] = useState(false);

    useEffect(() => {
        checkRolesAuth();
    }, []);

    const changeShowModal = () => {
        history.push("/")
    }

    const checkRolesAuth = () => {
        let roles = localStorage.getItem("roles");
        if (roles != null) {
            if (roles === "asesor") {
                setShowModal(false);
            } else {
                setShowModal(true);
            }
        } else {
            setShowModal(true);
        }
    }


    const chooseMenu = (indicator, status) => {
        if (indicator != 5) {
            setActiveIndicator(indicator);
            setHideWelcome(status);
        } else {
            if (window.confirm('Apa Anda yakin ingin keluar ?')) {
                // Save it!
                localStorage.clear();
                history.push("/");
                console.log('Thing was saved to the database.');
            } else {
                // Do nothing!
                console.log('Thing was not saved to the database.');
            }
        }
    }

    return (
        <Router>
            <div style={{ width: '100%' }}>
                <div style={{ width: '100%', height: '15%', background: '#2F5597', display: 'flex', flexDirection: 'row' }}>
                    <img src={logo} style={{ paddingLeft: '1%', paddingTop: '1%', paddingBottom: '2%' }} height='85px' />
                    <div style={{ borderLeft: '3px solid white', height: '50px', marginTop: '12px', marginLeft: '5px' }} />
                    <div style={{ display: 'flex', flexDirection: 'column', paddingLeft: '1%', paddingTop: '1%', color: 'white' }}>
                        <h6>LSP BUMA</h6>
                        <h6>Lembaga Sertifikasi Profesi PT Bukit Makmur Mandiri Utama</h6>
                    </div>
                </div>
                <div style={{ width: '100%', height: '1350px', display: 'flex', flexDirection: 'row' }}>
                    <div style={{ width: '20%', background: '#E2EBCF' }}>
                        <div style={{ height: '5px' }} />
                        <ul style={{ listStyleType: "none", padding: 0 }}>
                            <li style={styles.sideBarContentList}>
                                <img src={profile} height="30px" />
                                <Link
                                    onClick={() => chooseMenu(1, true)}
                                    style={{ color: activeIndicator === 1 ? 'blue' : 'black', textDecoration: 'none' }}
                                    to={`${url}/profile-asesor`}
                                >Profile</Link>
                            </li>
                            <hr style={{ borderTop: activeIndicator === 1 ? '1px solid red' : '1px solid white' }} />
                            <li style={styles.sideBarContentList}>
                                <img src={pra_asesmen} height="30px" />
                                <Link
                                    onClick={() => chooseMenu(2, true)}
                                    style={{ color: activeIndicator === 2 ? 'blue' : 'black', textDecoration: 'none' }}
                                    to={`${url}/pra-asesmen-asesor`}
                                >Pra-Asesmen</Link>
                            </li>
                            <hr style={{ borderTop: activeIndicator === 2 ? '1px solid red' : '1px solid white' }} />
                            <li style={styles.sideBarContentList}>
                                <img src={asesmen} height="30px" />
                                <Link
                                    onClick={() => chooseMenu(3, true)}
                                    style={{ color: activeIndicator === 3 ? 'blue' : 'black', textDecoration: 'none' }}
                                    to={`${url}/asesmen-asesor`}
                                >Asesmen</Link>
                            </li>
                            <hr style={{ borderTop: activeIndicator === 3 ? '1px solid red' : '1px solid white' }} />
                            <li style={styles.sideBarContentList}>
                                {/* <img src={person} height="30px" /> */}
                                <AiFillCopy color="#B0131E" size={25} />
                                <Link  to={`${url}/asesmen-formulir`} onClick={() => chooseMenu(4, true)} style={{ color: activeIndicator === 4 ? 'blue' : 'black', textDecoration: 'none' }}>Formulir Asesmen</Link>
                            </li>
                            <hr style={{ borderTop: activeIndicator === 4 ? '1px solid red' : '1px solid white' }} />
                            <li style={styles.sideBarContentList}>
                                {/* <img src={person} height="30px" /> */}
                                <AiOutlineImport color="#B0131E" size={25} />
                                <Link onClick={() => chooseMenu(5, true)} style={{ color:'black', textDecoration: 'none' }}>Logout</Link>
                            </li>
                        </ul>
                    </div>
                    <div style={{ width: '80%', height: '100%' }}>
                        <Switch>
                            {routes.map((route, index) => (
                                <Route
                                    key={index}
                                    path={route.path}
                                    exact={route.exact}
                                    children={<route.main />}
                                />
                            ))}
                        </Switch>
                    </div>
                </div>
                <ModalAlert statusOpen={showModal} toggle={changeShowModal} title={"Pesan"} content={"Maaf Anda tidak memiliki akses kehalaman ini"} close={changeShowModal} />
            </div>
        </Router>
    );
}


const styles = {
    container: {
        width: '100%',
        height: '1000px'
    },
    header: {
        width: '100%',
        height: '10%',
        background: '#2F5597',
        display: 'flex',
        flexDirection: 'row'
    },
    headerImg: {
        paddingLeft: '1%',
        paddingTop: '1%',
        paddingBottom: '2%'
    },
    headerTitle: {
        display: 'flex',
        flexDirection: 'column',
        paddingLeft: '1%',
        paddingTop: '1%',
        color: 'white'
    },
    sidebar: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'row'
    },
    sidebarContent: {
        width: '20%',
        height: '100%',
        background: '#E2EBCF'
    },
    sideBarContentUl: {
        listStyleType: "none",
        padding: 0
    },
    sideBarContentList: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    }
}

