import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Button, Form, FormGroup, Input, InputGroup, InputGroupAddon, Modal, ModalBody, Spinner } from 'reactstrap';
import logo from '../../assets/image/logo.PNG'
import ModalSpinner from '../../components/ModalSpinner';
// import ModalSpinner from '../../components/ModalSpinner';
import axios from 'axios';
import { APIURL } from '../../helper/LinkAPI';
import { checkThisEmpty } from "../../helper/CheckEmpty";
import { FaBullseye } from 'react-icons/fa';

export default function LoginAssesorPage() {

    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    const [isLoading, setIsLoading] = useState(false);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [visiblePassword, setVisiblePassword] = useState(false);


    let history = useHistory();

    const loginProccess = async () => {
        if (checkThisEmpty(email) || checkThisEmpty(password)) {
            alert("Field input tidak boleh kosong");
        } else {
            setIsLoading(true);
            try {
                const result = await axios.post(APIURL + "/user/login", {
                    "email": email,
                    "password": password
                });

                if (result.status === 200) {
                    const meta = result.data.meta;
                    const status = meta.status;
                    const data = result.data.data;
                    const token = result.data.data.access_token;
                    if (status === "success") {
                        const role = data.role[0];
                        if (role === "asesor") {
                            console.log("Success");
                            localStorage.setItem('roles', 'asesor');
                            localStorage.setItem("isLogin", true);
                            localStorage.setItem("token", token);
                            setIsLoading(false);
                            history.push('/');
                        } else {
                            setIsLoading(false);
                            alert("Anda tidak memiliki otorisasi");
                        }
                    }
                }
            } catch (error) {
                if (error.message === "Network Error") {
                    alert("Error: " + error.message);
                } else {
                    alert("Error: " + JSON.stringify(error.response.data?.data?.error));
                }
                setIsLoading(false);
            }
        }
    }

    return (
        <div style={styles.body}>
            <div style={styles.mainContainer}>
                <div style={{ margin: '0 auto' }}><u><b>Halaman Login Asesor</b></u></div>
                <div style={styles.container}>
                    <div style={styles.image}>
                        <img src={logo} height="60px" />
                    </div>
                    <div style={styles.form}>
                        <Form>
                            <FormGroup>
                                <Input
                                    type="text"
                                    name="Email"
                                    placeholder="Email"
                                    onChange={(event) => setEmail(event.target.value)} />
                            </FormGroup>

                            <FormGroup>
                                <InputGroup>
                                    <Input
                                        type={visiblePassword ? "text" : "password"}
                                        name="password"
                                        placeholder="Password"
                                        onChange={(event) => setPassword(event.target.value)} />
                                    <InputGroupAddon addonType="prepend"><Button onClick={() => visiblePassword ? setVisiblePassword(false) : setVisiblePassword(true)}><FaBullseye color={visiblePassword ? "red" : "white"} /></Button></InputGroupAddon>
                                </InputGroup>
                            </FormGroup>
                            <Button style={styles.btn} onClick={() => loginProccess()} block>
                                LOGIN
                            </Button>
                        </Form>
                        <br />
                        <div style={{ textAlign: 'start' }}>
                            Jika lupa password silahkan klik link <Link style={{ textDecoration: 'none', color: 'red' }}>lupa password</Link>
                            <br />
                            Sudah register tapi token belum terkirim? <Link style={{ textDecoration: 'none', color: 'red' }}>kirim ulang token</Link>
                        </div>
                    </div>
                </div>
            </div>
            <ModalSpinner statusOpen={isLoading} toggle={isLoading} />
        </div>
    );
}

const styles = {
    body: {
        width: '100%',
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        background: 'lightgrey',
    },
    mainContainer: {
        width: '50%',
        height: '350px',
        margin: '0 auto',
        display: 'flex',
        background: '#A3C2C2',
        flexDirection: 'column',
        paddingTop: '2%'

    },
    container: {
        width: '90%',
        height: '300px',
        margin: '0 auto',
        display: 'flex',
        background: '#A3C2C2',
        flexDirection: 'row',
        paddingTop: '2%'

    },
    image: {
        width: '30%',
        display: 'flex',
        justifyContent: 'center'
    },
    form: {
        width: '70%',
        flex: 'display',
        paddingRight: '5%',
    },
    btn: {
        background: '#006666',
    }
}