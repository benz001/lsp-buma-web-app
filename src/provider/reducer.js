export const initialState = {
    dataPersonal: {
        "name": "",
        "email": "",
        "nik": "",
        "phoneNumber": "",
        "gender": "",
        "pob": "",
        "dob": "",
        "address": "",
        "province": "",
        "district": "",
        "postalCode": "",
        "education": "",
        "profession": ""

    },
    dataEmployment: {
        "companyName": "",
        "companyAddress": "",
        "companyJobTitle": "",
        "companyPhoneNumber": "",
        "companyFaxNumber": "",
        "companyEmail": "",
        "photo": {}
    },
    dataEmployment: {
        "companyName": "",
        "companyAddress": "",
        "companyJobTitle": "",
        "companyPhoneNumber": "",
        "companyFaxNumber": "",
        "companyEmail": "",
        "photo": null
    },
    dataCertification: {
        "schematicCertification": "",
        "placeCompetence": "",
        "idCard": null,
        "certification": null,
        "certificationTraining": null,
        "unitOperationCertificate": null,
        "versalityUnit": null,
    },
    dataSchema: {
        "id": 0
    },
    alreadyRegister: {
        "status": false
    }
}

export const reducer = (state, action) => {
    switch (action.type) {
        case "change_data_personal":
            return {
                ...state, dataPersonal: {
                    "name": action.name,
                    "email": action.email,
                    "nik": action.nik,
                    "phoneNumber": action.phoneNumber,
                    "gender": action.gender,
                    "pob": action.pob,
                    "dob": action.dob,
                    "address": action.address,
                    "province": action.province,
                    "district": action.district,
                    "postalCode": action.postalCode,
                    "education": action.education,
                    "profession": action.profession
                }
            }
        case "change_data_employment":
            return {
                ...state, dataEmployment: {
                    "companyName": action.companyName,
                    "companyAddress": action.companyAddress,
                    "companyJobTitle": action.companyJobTitle,
                    "companyPhoneNumber": action.companyPhoneNumber,
                    "companyFaxNumber": action.companyFaxNumber,
                    "companyEmail": action.companyEmail,
                    "photo": action.photo
                }
            }
        case "change_data_certification":
            return {
                ...state, dataCertification: {
                    "schematicCertification": action.schematicCertification,
                    "placeCompetence": action.placeCompetence,
                    "idCard": action.idCard,
                    "certification": action.certification,
                    "certificationTraining": action.certificationTraining,
                    "unitOperationCertificate": action.unitOperationCertificate,
                    "versalityUnit": action.versalityUnit
                }
            }
        case "change_id_data_schema":
            return { ...state, dataSchema: { "id": action.payload } }
        case "change_id_place_competence":
            return { ...state, placeCompetence: { "id": action.payload } }
        case "change_status":
            return { ...state, alreadyRegister: { "status": action.payload } }
        default:
            return state;
    }
}