import { useReducer, createContext } from 'react';
import { initialState, reducer } from './reducer';

const Context = createContext(initialState);


function Provider(props) {
    const [state, dispatch] = useReducer(reducer, initialState);
    const value = { state, dispatch };

    return (
        <Context.Provider value={value}>
            {props.children}
        </Context.Provider>
    );

}

export {Context, Provider};