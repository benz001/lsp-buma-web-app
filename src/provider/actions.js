export const CHANGE_DATA_PERSONAL = "change_data_personal";
export const CHANGE_DATA_EMPLOYMENT = "change_data_employment";

export function changeDataPersonal(
    name,
    email,
    nik,
    phoneNumber,
    gender,
    pob,
    dob,
    address,
    province,
    district,
    postalCode,
    education,
    profession
) {
    return {
        type: "change_data_personal",
        name: name,
        email: email,
        nik: nik,
        phoneNumber: phoneNumber,
        gender: gender,
        pob: pob,
        dob: dob,
        address: address,
        province: province,
        district: district,
        postalCode: postalCode,
        education: education,
        profession: profession

    }
}

export function changeDataEmployment(
    companyName,
    companyAddress,
    companyJobTitle,
    companyPhoneNumber,
    companyFaxNumber,
    companyEmail,
    photo
) {
    return {
        type: "change_data_employment",
        companyName: companyName,
        companyAddress: companyAddress,
        companyJobTitle: companyJobTitle,
        companyPhoneNumber: companyPhoneNumber,
        companyFaxNumber: companyFaxNumber,
        companyEmail: companyEmail,
        photo: photo
    }
}

export function changeDataCertification(
    schematicCertification,
    placeCompetence,
    idCard,
    certification,
    certificationTraining,
    unitOperationCertificate,
    versalityUnit
) {
    return {
        type: "change_data_certification",
        schematicCertification: schematicCertification,
        placeCompetence: placeCompetence,
        idCard: idCard,
        certification: certification,
        certificationTraining: certificationTraining,
        unitOperationCertificate: unitOperationCertificate,
        versalityUnit: versalityUnit
    }
}

export function changeIdDataSchema(
    payload
) {
    return {
        type: "change_id_data_schema",
        payload: payload,
    }
}

export function changeIdPlaceCompetence(
    payload
) {
    return {
        type: "change_id_place_competence",
        payload: payload,
    }
    
}

export function changeStatus(
    payload
) {
    return {
        type: "change_status",
        payload: payload,
    }
    
}