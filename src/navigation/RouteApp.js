import { useEffect, useState } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";

import RegisterPage from '../ui/accession/RegisterAccessionPage';
import LoginAccessionPage from '../ui/accession/LoginAccessionPage';
import SideBarNavigationAccession from '../ui/accession/SideBarNavigationAccession';
import SidebarBarNavigationAssessor from "../ui/assessor/SideBarNavigationAssessor";
import SidebarBarNavigationAccession from "../ui/accession/SideBarNavigationAccession";
import WelcomePage from "../ui/WelcomePage";
import LoginAssesorPage from "../ui/assessor/LoginAssesorPage";
import SplashPage from "../ui/SplashPage";
import LoginAdminPage from "../ui/admin/LoginAdminPage";
import SidebarBarNavigationAdmin from "../ui/admin/SideBarNavigationAdmin";

const PrivateRoute = ({ children, ...rest }) => {
  let isLogin = localStorage.getItem("isLogin");
  return (
    <Route
      {...rest}
      render={({ location }) =>
        isLogin ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/welcome",
              state: { from: location }
            }}
          />
        )
      }
    />
  );
}

// export const NotLoginRoute = () => {
//   return (
//     <Router>
//       <Switch>
//         <Route exact path="/">
//           <SplashPage />
//         </Route>
//         <Route exact path="/welcome">
//           <WelcomePage />
//         </Route>
//         <Route exact path="/login-asesi">
//           <LoginAccessionPage />
//         </Route>
//         <Route exact path="/login-asesor">
//           <LoginAssesorPage />
//         </Route>
//       </Switch>
//     </Router>
//   );
// }


export const AlreadyLoginRoute = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <SplashPage />
        </Route>
        <Route exact path="/welcome">
          <WelcomePage />
        </Route>
        <Route exact path="/login-asesi">
          <LoginAccessionPage />
        </Route>
        <Route exact path="/login-asesor">
          <LoginAssesorPage />
        </Route>
        <PrivateRoute path="/asesi">
          <SidebarBarNavigationAccession />
        </PrivateRoute>
        <PrivateRoute path='/asesor'>
          <SidebarBarNavigationAssessor />
        </PrivateRoute>
        <Route path='/login-admin'>
          <LoginAdminPage />
        </Route>
        <Route path='/admin'>
          <SidebarBarNavigationAdmin />
        </Route>
      </Switch>
    </Router>
  );
}

